package in.org.cyberspace.taxi55;

import android.*;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.org.cyberspace.taxi55.model.ContactNumber;
import in.org.cyberspace.taxi55.services.ContactNumberService;

public class EmergencyContactActivity extends AppCompatActivity {


    private static final int PERMISSION_REQUEST_CODE = 1;
    EditText name_of_person, mobile_number;
    Button add_contact;

    String passId = "";

    Toolbar mtoolbar;
    AppBarLayout appBarLayout;
    ImageView add_from_contact_list;
    private List<String> mobilelist;
    HashMap<String, String> contactsname;
    String mobile;
    ProgressDialog pd;
    List<ContactNumber> ad;
    ImageView back_arrow;
    RecyclerView contact_list_recycler;
    private static final int RESULT_PICK_CONTACT = 132;
    Dialog a;

    public void pickContact(View v)
    {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        try {
            startActivityForResult(contactPickerIntent, RESULT_PICK_CONTACT);
        }catch (Exception ex1){
            ex1.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            // Check for the request code, we might be usign multiple startActivityForReslut
            switch (requestCode) {
                case RESULT_PICK_CONTACT:
                    contactPicked(data);
                    break;
            }
        } else {
            Log.e("MainActivity", "Failed to pick contact");
        }
    }


    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null ;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            // Set the value to the textviews
         /*   textView1.setText(name);
            textView2.setText(phoneNo);*/

            name_of_person.setText(name);
            mobile_number.setText(phoneNo);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_contact);

        add_contact = (Button) findViewById(R.id.add_contact);
        name_of_person = (EditText) findViewById(R.id.name_of_person);
        mobile_number = (EditText) findViewById(R.id.mobile_number);
        add_from_contact_list = (ImageView) findViewById(R.id.add_from_contact_list);


        contactsname = new HashMap<String, String>();
        mobilelist = new ArrayList<String>();
        ad = new ArrayList<>();

        back_arrow = (ImageView) findViewById(R.id.back_arrow);
        a = new Dialog(EmergencyContactActivity.this);

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        add_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(name_of_person.getText().toString())) {

                    ShowAlert("Warning...", "Enter name of person.");

                } else if (TextUtils.isEmpty(mobile_number.getText().toString())) {
                    ShowAlert("Warning...", "Enter mobile number...");
                } else {
                    PrefManager prefManager = new PrefManager(EmergencyContactActivity.this);
                    String Did = prefManager.getUSER_Id();


                    addToContact(name_of_person.getText().toString(), mobile_number.getText().toString(), Did);


                }
            }
        });


        add_from_contact_list.setColorFilter(getResources().getColor(R.color.sky_blue));
        add_from_contact_list.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                pickContact(v);
    /* a.setContentView(R.layout.dialog_number);

                contact_list_recycler = (RecyclerView) a.findViewById(R.id.contact_list_recycler);
                Button contact_cancel = (Button) a.findViewById(R.id.contact_cancel);
                Button contact_ok = (Button) a.findViewById(R.id.contact_ok);
                contact_list_recycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

                if (checkPermission()) {
                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("cont_no", MODE_PRIVATE);
                    int cnt = sharedPreferences.getInt("count", 0);
                    if (cnt > 0) {
                        a.show();
                        DBHelper dbHandler = new DBHelper(getApplicationContext());
                        List<ContactNumber> contactNumberList = dbHandler.allContacts();
                        ContactAdapter contactAdapter = new ContactAdapter(getApplicationContext(), contactNumberList, a);
                        contact_list_recycler.setAdapter(contactAdapter);
                    } else {
                        final ProgressDialog loading = ProgressDialog.show(EmergencyContactActivity.this, "Loading...", "Please wait...", false, false);
                        newContact(loading, a);
                    }
                } else {
                    insertDummyContactWrapper();
                    requestPermission();
                    Snackbar.make(getView(), "Permission Not Granted, Now you can not access contact.", Snackbar.LENGTH_LONG).show();


                }


                contact_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        a.dismiss();
                    }
                });
                contact_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        a.dismiss();
                    }
                });
*/

            }
        });

    }

    public void ShowAlert(String title, String message) {
        final Dialog dialog = new Dialog(getApplicationContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dilogbox_message);
        dialog.show();

        TextView tit = (TextView) dialog.findViewById(R.id.dialog_title);
        tit.setText(title);
        TextView mes = (TextView) dialog.findViewById(R.id.dialog_message);
        mes.setText(message);
        Button ok = (Button) dialog.findViewById(R.id.dialog_submit);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                dialog.cancel();

            }
        });

    }

    public void addToContact(final String name, final String num, final String user_id) {
        final ProgressDialog pdlg = new ProgressDialog(EmergencyContactActivity.this);
        pdlg.setMessage("Please Wait...");
        pdlg.show();
        pdlg.setCancelable(false);
        String REQUEST_TAG = getPackageName() + ".volleyJsonArrayRequest";
        JSONObject jsonObject = new JSONObject();
        try {
            //  jsonObject.put("secret_key", ConnectionConfiguration.SECRET_KEY);
            jsonObject.put("user_id", user_id);
            jsonObject.put("mobile_no", num);
            jsonObject.put("name", name);
            //  jsonObject.put("Mobile", mobile);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = ConnectionConfiguration.URL+"emergency_contact";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Response", response.toString());
                pdlg.dismiss();
                try {
                    if (response.getString("status").equals("success")) {
                        startActivity(new Intent(EmergencyContactActivity.this, Emergency_Contact.class));
                        finish();
                    } else {
                        Toast.makeText(EmergencyContactActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("Response", error.toString());
                        pdlg.dismiss();
                        Toast.makeText(EmergencyContactActivity.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                    }
                });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, REQUEST_TAG);
    }


    // }

    public void newContact(final ProgressDialog loading, final Dialog a) {

        //   final ProgressDialog loading = null;


        new AsyncTask() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading.show();

            }

            @Override
            protected Object doInBackground(Object[] params) {
                readPhoneContacts();
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                //a.show();
                SharedPreferences sh = getApplicationContext().getSharedPreferences("cont_no", MODE_PRIVATE);
                SharedPreferences.Editor editor = sh.edit();

                editor.putInt("count", ad.size());
                editor.commit();

                DBHelper dbHandler = new DBHelper(getApplicationContext());

                // List<ContactNumber> contactNumberList = new ArrayList<ContactNumber>()

                dbHandler.addMultipleContact(ad);
                loading.dismiss();


                a.show();

                List<ContactNumber> contactNumberList = dbHandler.allContacts();
                ContactAdapter contactAdapter = new ContactAdapter(getApplicationContext(), ad, a);
                contact_list_recycler.setAdapter(contactAdapter);

            }
        }.execute();
    }


    public void readPhoneContacts() //This Context parameter is nothing but your Activity class's Context
    {
        Context cntx = getApplicationContext();
        Cursor cursor = cntx.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        Integer contactsCount = cursor.getCount(); // get how many contacts you have in your contacts list
        if (contactsCount > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //the below cursor will give you details for multiple contacts
                    Cursor pCursor = cntx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    // continue till this cursor reaches to all phone numbers which are associated with a contact in the contact list
                    while (pCursor.moveToNext()) {
                        int phoneType = pCursor.getInt(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        //String isStarred 		= pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
                        String phoneNo = pCursor.getString(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //you will get all phone numbers according to it's type as below switch case.
                        //Logs.e will print the phone number along with the name in DDMS. you can use these details where ever you want.
                        String m = ((((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).replace("*", "").replace("#", "")).trim().toString();
                        m = m.replaceAll("\\D+", "");
                        String numberOnly = phoneNo.replaceAll("[^0-9]", "");

                        if (m.length() > 10) {
                            m = m.substring(Math.max(m.length() - 10, 0));
                        }
                        if (m.equals(mobile)) {

                        } else {
                            contactsname.put(m, contactName);
                            mobilelist.add(m);

                            ad.add(new ContactNumber(contactName, m));

                        }


                    }
                    pCursor.close();
                }

            }
            cursor.close();
        /*    try {
                SharedPreferences sharedPreferences = getSharedPreferences("PHONECONTACT", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                int no_mobile = mobilelist.size();
                editor.putInt("phone_no", no_mobile);
                editor.commit();

            } catch (Exception e) {
                e.printStackTrace();
            }*/
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.READ_CONTACTS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {

            //   insertDummyContactWrapper();
            return false;

        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(EmergencyContactActivity.this, android.Manifest.permission.READ_CONTACTS)) {

            if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(EmergencyContactActivity.this, new String[]{android.Manifest.permission.READ_CONTACTS}, 110);

            }

            // Toast.makeText(getApplicationContext(), "Contact permission allows us to Read the Contact. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();

        }/* else {
            ActivityCompat.requestPermissions(getApplicationContext(), new String[]{Manifest.permission.READ_CONTACTS}, 110);
       }*/
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void insertDummyContactWrapper() {
        int hasWriteContactsPermission = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            hasWriteContactsPermission = getApplicationContext().checkSelfPermission(android.Manifest.permission.READ_CONTACTS);
        }
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            if (!shouldShowRequestPermissionRationale(android.Manifest.permission.READ_CONTACTS)) {
                showMessageOKCancel("You need to allow access to Contacts",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(new String[]{android.Manifest.permission.READ_CONTACTS},
                                        110);
                            }
                        });
                return;
            }
            requestPermissions(new String[]{android.Manifest.permission.READ_CONTACTS},
                    110);
            return;
        }
        // insertDummyContact();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(EmergencyContactActivity.this, R.style.myDialog)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 110:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Snackbar.make(add_contact, "Permission Granted, Now you can access contact.", Snackbar.LENGTH_LONG).show();

                    Intent service_intent = new Intent(getApplicationContext(), ContactNumberService.class);
                    getApplicationContext().startService(service_intent);

                } else {

                    Snackbar.make(add_contact, "Permission Denied, You cannot access contact.", Snackbar.LENGTH_LONG).show();

                }
                break;


            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {
        Context ctx;
        List<ContactNumber> contactNumberList;
        Dialog dialog;

        public ContactAdapter(Context ctx, List<ContactNumber> contactNumberList, Dialog dialog) {
            this.ctx = ctx;
            this.contactNumberList = contactNumberList;
            this.dialog = dialog;
        }

        @Override
        public ContactAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_emergency_contact, parent, false);

            ContactAdapter.ViewHolder viewHolder = new ContactAdapter.ViewHolder(itemLayoutView);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ContactAdapter.ViewHolder holder, int position) {

            holder.cont_number.setText(contactNumberList.get(position).getNumber());
            holder.cont_nm.setText(contactNumberList.get(position).getName());
            holder.cont_id.setVisibility(View.GONE);
            holder.cont_menu.setVisibility(View.GONE);
            holder.cont_share_txt.setVisibility(View.GONE);
            holder.view_b.setVisibility(View.GONE);
            holder.toggleButton.setVisibility(View.GONE);
            holder.back_view.setVisibility(View.VISIBLE);
            holder.lin_lay_emg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    name_of_person.setText(holder.cont_nm.getText().toString());
                    mobile_number.setText(holder.cont_number.getText().toString());
                    dialog.dismiss();
                }
            });

        }

        @Override
        public int getItemCount() {
            return contactNumberList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {


            TextView cont_number, cont_nm, cont_id;
            ImageView cont_menu;
            TextView cont_share_txt;
            ToggleButton toggleButton;
            LinearLayout lin_lay_emg;
            View back_view ,view_b;

            public ViewHolder(View itemView) {
                super(itemView);
                cont_id = (TextView) itemView.findViewById(R.id.cont_id);
                cont_number = (TextView) itemView.findViewById(R.id.cont_number);
                cont_nm = (TextView) itemView.findViewById(R.id.cont_nm);
                cont_menu = (ImageView) itemView.findViewById(R.id.cont_menu);
                cont_share_txt = (TextView) itemView.findViewById(R.id.cont_share_txt);
                toggleButton = (ToggleButton) itemView.findViewById(R.id.toggle_btn);
                lin_lay_emg = (LinearLayout) itemView.findViewById(R.id.lin_lay_emg);
                back_view = (View) itemView.findViewById(R.id.back_view);
                view_b =(View) itemView.findViewById(R.id.view_b);

            }
        }

    }

}
