package in.org.cyberspace.taxi55.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import in.org.cyberspace.taxi55.BuildConfig;
import in.org.cyberspace.taxi55.ChatActivity;
import in.org.cyberspace.taxi55.R;
import in.org.cyberspace.taxi55.db.NotiDbHelper;
import in.org.cyberspace.taxi55.model.NewMessage;
import in.org.cyberspace.taxi55.model.NotificationUtils;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NotificationReceiver extends FirebaseMessagingService {
    public static final String name = "Notification";
    String TAG = "NotificationReceiver";
    NotificationUtils notificationUtils;

    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e("message_received", remoteMessage.getData() + BuildConfig.FLAVOR);
        Log.e(this.TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Log.e(this.TAG, "Message data payload: " + remoteMessage.getData());
        }
        if (remoteMessage.getNotification() != null) {
            Log.e(this.TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        String rid = (String) remoteMessage.getData().get(NotiDbHelper.rid);
        String sid = (String) remoteMessage.getData().get(NotiDbHelper.sid);
        String rtime = (String) remoteMessage.getData().get(NotiDbHelper.rtime);
        String mmmmm = (String) remoteMessage.getData().get(NotiDbHelper.msg);
        String name1 = (String) remoteMessage.getData().get("name");
        new NotiDbHelper(getApplicationContext()).saveSingleMessage(new NewMessage(0, rid, sid, mmmmm, timeStamp + BuildConfig.FLAVOR, timeStamp + BuildConfig.FLAVOR, 233, 1, name1));
        SharedPreferences sh1 = getApplicationContext().getSharedPreferences("chat_rm", 0);
        Editor editor = sh1.edit();
        String _rid = sh1.getString(NotiDbHelper.rid, BuildConfig.FLAVOR);
        String _sid = sh1.getString(NotiDbHelper.sid, BuildConfig.FLAVOR);
        Log.e("SIDDD", sid);
        showNotificationMessage(getApplicationContext(), "Message from Driver", name1 + " : " + mmmmm, sid + BuildConfig.FLAVOR, rtime, new Intent(getApplicationContext(), ChatActivity.class));
        sendBroadcast(new Intent().setAction(name).addCategory("android.intent.category.DEFAULT").putExtra("data", "yes"));
    }

    private void sendNotification(String messageBody, String message) {
        int notify_no;
        Intent intent = new Intent(this, NotificationReceiver.class);
        intent.addFlags(603979776);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, 134217728);
        if (1 < 9) {
            notify_no = 1 + 1;
        } else {
            notify_no = 0;
        }
        ((NotificationManager) getSystemService("notification")).notify(notify_no + 2, new Builder(this).setSmallIcon(R.drawable.icon).setContentTitle(messageBody).setContentText(message).setAutoCancel(true).setSound(RingtoneManager.getDefaultUri(2)).setContentIntent(pendingIntent).build());
    }

    private void showNotificationMessage(Context context, String title, String message, String id, String timeStamp, Intent intent) {
        this.notificationUtils = new NotificationUtils(context);
        intent.setFlags(268468224);
        this.notificationUtils.showNotificationMessage(title, message, id, timeStamp, intent);
    }
}


/*
package in.org.cyberspace.taxi55.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;
import java.util.Date;

import in.org.cyberspace.taxi55.NewChatActivity;
import in.org.cyberspace.taxi55.R;
import in.org.cyberspace.taxi55.Splash;
import in.org.cyberspace.taxi55.db.NotiDbHelper;
import in.org.cyberspace.taxi55.model.NewMessage;
import in.org.cyberspace.taxi55.model.NotificationUtils;

*/
/**
 * Created by Ashvini on 2/26/2017.
 *//*


public class NotificationReceiver extends FirebaseMessagingService {
    // String TAG = "NotificationReceiver";
    NotificationUtils notificationUtils;

    String TAG = "NotificationReceiver";
    public static final String name = "Notification";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
      */
/*  Log.e("message_received", remoteMessage.getData() + "");


        Log.e(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }


    }*//*



        Log.e("message_received", remoteMessage.getData() + "");


        Log.e(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        String rid = remoteMessage.getData().get("rid");
        String sid = remoteMessage.getData().get("sid");
        String rtime = remoteMessage.getData().get("rtime");
        String mmmmm = remoteMessage.getData().get("message");
        String name1 = remoteMessage.getData().get("name");

        NotiDbHelper mHelper = new NotiDbHelper(getApplicationContext());
        NewMessage m = new NewMessage(0, rid, sid, mmmmm, timeStamp + "", timeStamp + "", 233, 1,name1);
        mHelper.saveSingleMessage(m);

      //  sendNotification("Message from Passenger : "+name1, mmmmm + "");
        SharedPreferences sh1 = getApplicationContext().getSharedPreferences("chat_rm", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sh1.edit();
      */
/*  editor.putString("rid",chatRoomId+"");
        editor.putString("sid",id+"");
        editor.putString("type",type+"");
        *//*

        String _rid = sh1.getString("rid", "");
        //Log.e("_rid", _rid);
        String _sid = sh1.getString("sid", "");

        Log.e("SIDDD",sid);


        Intent intentres = new Intent(getApplicationContext(), Splash.class);
       */
/* intentres.putExtra("rid", rid);
        intentres.putExtra("sid", sid);*//*

        showNotificationMessage(getApplicationContext(), "Message from Driver", name1+" : "+mmmmm, sid + "", rtime, intentres);


      */
/*  if ((_rid.equals(sid)) && (_sid.equals(rid)) && (NewChatActivity.activate1 == true)) {
            sendBroadcast(new Intent()
                    .setAction(name)
                    .addCategory(Intent.CATEGORY_DEFAULT)
                    .putExtra("data", "yes"));
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();


        } else if ((_rid.equals(rid)) && (NewChatActivity.activate1 == true)) {
            sendBroadcast(new Intent()
                    .setAction(name)
                    .addCategory(Intent.CATEGORY_DEFAULT)
                    .putExtra("data", "yes"));
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();


        }*//*



    }

    private void sendNotification(String messageBody, String message) {


        int notify_no = 1;

        Intent intent = new Intent(this, NotificationReceiver.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, notify_no */
/* Request code *//*
, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        if (notify_no < 9) {
            notify_no = notify_no + 1;
        } else {
            notify_no = 0;
        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle(messageBody)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notify_no + 2 */
/* ID of notification *//*
, notificationBuilder.build());
    }

    private void showNotificationMessage(Context context, String title, String message, String id, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, id, timeStamp, intent);
    }
}
*/
