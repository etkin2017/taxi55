package in.org.cyberspace.taxi55;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.OnTabSelectedListener;
import android.support.design.widget.TabLayout.Tab;
import android.support.design.widget.TabLayout.TabLayoutOnPageChangeListener;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RateCard extends AppCompatActivity {
    String[] D_CAB;
    String[] D_Initial;
    String[] D_Rest;
    String[] N_CAB;
    String[] N_Initial;
    String[] N_Rest;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_card);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fetchRates();
    }

    public void displayTabs() {
        Log.i("Frag CABS", this.D_CAB.length + BuildConfig.FLAVOR);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Day").setIcon(R.drawable.day));
        tabLayout.addTab(tabLayout.newTab().setText("Night").setIcon(R.drawable.night));
        tabLayout.setTabGravity(0);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), this.N_CAB, this.D_CAB, this.N_Initial, this.D_Initial, this.N_Rest, this.D_Rest));
        viewPager.addOnPageChangeListener(new TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new OnTabSelectedListener() {
            public void onTabSelected(Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            public void onTabUnselected(Tab tab) {
            }

            public void onTabReselected(Tab tab) {
            }
        });
    }

    public void fetchRates() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("transfertype", "Point to Point Transfer");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "load_card_rate", jsonObject, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                Log.i("Response", response.toString());
                progressDialog.dismiss();
                try {
                    int i;
                    JSONObject jday;
                    String initKM;
                    JSONArray day = response.getJSONArray("day");
                    JSONArray nigth = response.getJSONArray("night");
                    RateCard.this.D_CAB = new String[day.length()];
                    RateCard.this.D_Initial = new String[day.length()];
                    RateCard.this.D_Rest = new String[day.length()];
                    RateCard.this.N_CAB = new String[nigth.length()];
                    RateCard.this.N_Initial = new String[nigth.length()];
                    RateCard.this.N_Rest = new String[nigth.length()];
                    for (i = 0; i < day.length(); i++) {
                        jday = day.getJSONObject(i);
                        RateCard.this.D_CAB[i] = jday.getString("cartype");
                        initKM = jday.getString("intialkm");
                        RateCard.this.D_Initial[i] = "For First " + initKM + " KM - " + jday.getString("intailrate") + " / KM";
                        RateCard.this.D_Rest[i] = "For Rest KM - " + jday.getString("standardrate") + " / KM";
                    }
                    for (i = 0; i < nigth.length(); i++) {
                        jday = nigth.getJSONObject(i);
                        RateCard.this.N_CAB[i] = jday.getString("cartype");
                        initKM = jday.getString("intialkm");
                        RateCard.this.N_Initial[i] = "For First " + initKM + " KM - " + jday.getString("intailrate") + " / KM";
                        RateCard.this.N_Rest[i] = "For Rest KM - " + jday.getString("standardrate") + " / KM";
                    }
                    RateCard.this.displayTabs();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "GetRatesRequest");
    }
}


/*
package in.org.cyberspace.taxi55;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RateCard extends AppCompatActivity {

    String[] N_CAB;
    String[] D_CAB;
    String[] N_Initial;
    String[] D_Initial;
    String[] D_Rest;
    String[] N_Rest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_card);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fetchRates();


    }

    public  void displayTabs()
    {
        Log.i("Frag CABS",D_CAB.length+"");
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Day").setIcon(R.drawable.day));
        tabLayout.addTab(tabLayout.newTab().setText("Night").setIcon(R.drawable.night));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount(),N_CAB,D_CAB,N_Initial,D_Initial,N_Rest,D_Rest);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void fetchRates()
    {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("transfertype","Point to Point Transfer");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final ProgressDialog progressDialog = new ProgressDialog(RateCard.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL+"load_card_rate", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Response",response.toString());
                progressDialog.dismiss();
                try {
                    JSONArray day=response.getJSONArray("day");
                    JSONArray nigth=response.getJSONArray("night");
                    D_CAB=new String[day.length()];
                    D_Initial=new String[day.length()];
                    D_Rest=new String[day.length()];

                    N_CAB=new String[nigth.length()];
                    N_Initial=new String[nigth.length()];
                    N_Rest=new String[nigth.length()];


                    for(int i=0;i<day.length();i++)
                    {
                        JSONObject jday=day.getJSONObject(i);
                        D_CAB[i]=jday.getString("cartype");
                        String initKM=jday.getString("intialkm");
                        String initRate=jday.getString("intailrate");

                        String init_str="For First "+initKM+" KM - "+initRate+" / KM";
                        D_Initial[i]=init_str;
                        D_Rest[i]="For Rest KM - "+jday.getString("standardrate")+" / KM";
                    }


                    for(int i=0;i<nigth.length();i++)
                    {
                        JSONObject jday=nigth.getJSONObject(i);
                        N_CAB[i]=jday.getString("cartype");
                        String initKM=jday.getString("intialkm");
                        String initRate=jday.getString("intailrate");

                        String init_str="For First "+initKM+" KM - "+initRate+" / KM";
                        N_Initial[i]=init_str;
                        N_Rest[i]="For Rest KM - "+jday.getString("standardrate")+" / KM";
                    }

                    displayTabs();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            progressDialog.dismiss();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest,"GetRatesRequest");
    }
}
*/
