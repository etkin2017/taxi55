/*
package in.org.cyberspace.taxi55;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DriverTrackingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_tracking);
    }
}
*/


package in.org.cyberspace.taxi55;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import in.org.cyberspace.taxi55.db.NotiDbHelper;
import in.org.cyberspace.taxi55.services.GPSTracker;

public class DriverTrackingActivity extends AppCompatActivity implements OnMapReadyCallback {
    String driver_id, driver_nm1, user_name, phone, email, booking_id;
    GoogleMap mMap;
    ImageView back_arrow;
    GPSTracker gps;

    Marker m2;
    Marker marker1;
    TextView ph_number, driver_nm;
    String bookinID;
    CircleImageView call_action, chat_action;

    String fcm, rid, sid, uname;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_tracking);
        back_arrow = (ImageView) findViewById(R.id.back_arrow);
        call_action = (CircleImageView) findViewById(R.id.call_action);
        chat_action = (CircleImageView) findViewById(R.id.chat_action);
        bookinID = getIntent().getStringExtra("booking_id");
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        this.driver_id = getIntent().getStringExtra("driver_id");
        booking_id = getIntent().getStringExtra("booking_id");
        email = getIntent().getStringExtra("email");
        phone = getIntent().getStringExtra("phone");
        user_name = getIntent().getStringExtra("user_name");
        driver_nm1 = getIntent().getStringExtra("driver_nm1");


        //for chat
        /* mapAct.putExtra(NotiDbHelper.sid, mobile);
        mapAct.putExtra(NotiDbHelper.rid, phoneNm);
        mapAct.putExtra("fcm", fcm);
        mapAct.putExtra("uname",uname);*/


        fcm = getIntent().getStringExtra("fcm");
        uname = getIntent().getStringExtra("uname");
        rid = getIntent().getStringExtra(NotiDbHelper.rid);
        sid = getIntent().getStringExtra(NotiDbHelper.sid);


        driver_nm = (TextView) findViewById(R.id.driver_nm);
        ph_number = (TextView) findViewById(R.id.ph_number);
        gps = new GPSTracker(DriverTrackingActivity.this);


        try {
            driver_nm.setText(user_name);
            ph_number.setText(phone);

        } catch (Exception ew) {
            ew.printStackTrace();
        }

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        call_action.setColorFilter(getResources().getColor(R.color.sky_blue));
        call_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL);

                intent.setData(Uri.parse("tel:" + phone));
                if (ActivityCompat.checkSelfPermission(DriverTrackingActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);
            }
        });


        chat_action.setColorFilter(getResources().getColor(R.color.sky_blue));

        chat_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String mobile = new PrefManager(DriverTrackingActivity.this).getUserMobile();
                Intent intent = new Intent(DriverTrackingActivity.this, NewChatActivity.class);
                intent.putExtra(NotiDbHelper.sid, sid);
                intent.putExtra(NotiDbHelper.rid, rid);
                intent.putExtra("fcm", fcm);
                intent.putExtra("uname", uname);
                startActivity(intent);

            }
        });

    }


    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        go_to_map(this.driver_id);


        if (gps.canGetLocation()) {
            double myLat = gps.getLatitude();
            double myLng = gps.getLongitude();
            m2 = mMap.addMarker(new MarkerOptions().position(new LatLng(myLat, myLng)).title("Me")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.start)));
            m2.showInfoWindow();
        }
    }

    public void go_to_map(final String driver_id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("driver_id", driver_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "fetch_driver_lat_long", jsonObject, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                Log.e("PassengerResponse", response.toString());
                try {
                    JSONObject jsonObject1 = response.getJSONArray("data").getJSONObject(0);
                    marker1 = mMap.addMarker(new MarkerOptions().title("Driver")
                            .position(new LatLng(Double.parseDouble(jsonObject1.getString("latitude")), Double.parseDouble(jsonObject1.getString("longitude"))))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.carm)));

                    LatLng ll = new LatLng(Double.parseDouble(jsonObject1.getString("latitude")), Double.parseDouble(jsonObject1.getString("longitude")));

                    mMap.moveCamera(CameraUpdateFactory.newLatLng(ll));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));
                    marker1.showInfoWindow();

                    //  Toast.makeText(DriverTrackingActivity.this, "lat lng", Toast.LENGTH_SHORT).show();

                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                //sweetAlertDialog.dismiss();
                                e.printStackTrace();
                            }


                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    marker1.remove();
                                    DriverTrackingActivity.this.go_to_map(driver_id);
                                    // Do some stuff
                                   /* sweetAlertDialog.dismiss();
                                    mydecoderview.startCamera();
                                    mydecoderview.setQRDecodingEnabled(true);*/
                                }
                            });
                        }
                    };
                    thread.start(); //start the thread


                } catch (JSONException e) {
                    marker1.showInfoWindow();
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                // marker1.showInfoWindow();
                error.printStackTrace();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }

    private int calculateZoomLevel() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        double equatorLength = 40075004; // in meters
        double widthInPixels = width;
        double metersPerPixel = equatorLength / 256;
        int zoomLevel = 1;
        while ((metersPerPixel * widthInPixels) > 3000) {
            metersPerPixel /= 2;
            ++zoomLevel;
        }
        Log.i("ADNAN", "zoom level = " + zoomLevel);
        return zoomLevel;
    }
}
