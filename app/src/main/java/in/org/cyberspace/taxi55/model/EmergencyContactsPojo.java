package in.org.cyberspace.taxi55.model;

/**
 * Created by Ashvini on 3/2/2017.
 */

public class EmergencyContactsPojo {


    public Integer id;

    public Integer passid;
    public String contactnumber;
    public String contactname;


    public EmergencyContactsPojo(Integer id, Integer passid, String contactnumber, String contactname) {
        this.id = id;
        this.passid = passid;
        this.contactnumber = contactnumber;
        this.contactname = contactname;
    }

    public EmergencyContactsPojo() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPassid() {
        return passid;
    }

    public void setPassid(Integer passid) {
        this.passid = passid;
    }

    public String getContactnumber() {
        return contactnumber;
    }

    public void setContactnumber(String contactnumber) {
        this.contactnumber = contactnumber;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

}
