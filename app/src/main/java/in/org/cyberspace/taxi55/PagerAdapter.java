package in.org.cyberspace.taxi55;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import in.org.cyberspace.taxi55.application.AppUtils.Constants;
import in.org.cyberspace.taxi55.application.AppUtils.LocationConstants;

public class PagerAdapter extends FragmentStatePagerAdapter {
    String[] D_CAB;
    String[] D_Initial;
    String[] D_Rest;
    String[] N_CAB;
    String[] N_Initial;
    String[] N_Rest;
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs, String[] n_cab, String[] d_cab, String[] n_Initial, String[] d_Initial, String[] n_Rest, String[] d_Rest) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.D_CAB = d_cab;
        this.D_Initial = d_Initial;
        this.D_Rest = d_Rest;
        this.N_CAB = n_cab;
        this.N_Initial = n_Initial;
        this.N_Rest = n_Rest;
    }

    public Fragment getItem(int position) {
        switch (position) {
            case Constants.SUCCESS_RESULT /*0*/:
                Bundle bundle = new Bundle();
                bundle.putStringArray("CAB", this.D_CAB);
                bundle.putStringArray("INIT", this.D_Initial);
                bundle.putStringArray("REST", this.D_Rest);
                Fragment tab1 = new TabDay();
                tab1.setArguments(bundle);
                return tab1;
            case LocationConstants.FAILURE_RESULT /*1*/:
                Bundle bundle2 = new Bundle();
                bundle2.putStringArray("CAB", this.N_CAB);
                bundle2.putStringArray("INIT", this.N_Initial);
                bundle2.putStringArray("REST", this.N_Rest);
                TabNight tab2 = new TabNight();
                tab2.setArguments(bundle2);
                return tab2;
            default:
                return null;
        }
    }

    public int getCount() {
        return this.mNumOfTabs;
    }
}


/*
package in.org.cyberspace.taxi55;

*/
/**
 * Created by Wasim on 17/02/2017.
 *//*


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    String[] N_CAB;
    String[] D_CAB;
    String[] N_Initial;
    String[] D_Initial;
    String[] D_Rest;
    String[] N_Rest;
    public PagerAdapter(FragmentManager fm, int NumOfTabs,String[] n_cab,String[] d_cab,String[] n_Initial,String[] d_Initial,String[] n_Rest,String[] d_Rest) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.D_CAB=d_cab;
        this.D_Initial=d_Initial;
        this.D_Rest=d_Rest;


        this.N_CAB=n_cab;
        this.N_Initial=n_Initial;
        this.N_Rest=n_Rest;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Bundle bundle= new Bundle();
                bundle.putStringArray("CAB",D_CAB);
                bundle.putStringArray("INIT",D_Initial);
                bundle.putStringArray("REST",D_Rest);
                TabDay tab1 = new TabDay();

                tab1.setArguments(bundle);
                return tab1;
            case 1:
                Bundle bundle2= new Bundle();
                bundle2.putStringArray("CAB",N_CAB);
                bundle2.putStringArray("INIT",N_Initial);
                bundle2.putStringArray("REST",N_Rest);
                TabNight tab2 = new TabNight();
                tab2.setArguments(bundle2);
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}*/
