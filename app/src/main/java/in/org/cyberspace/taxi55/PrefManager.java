package in.org.cyberspace.taxi55;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PrefManager {
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String IS_USER_LOGGED_IN = "IsUserLoggedIn";
    private static final String PREF_NAME = "TAXI55";
    private static final String USER_Id = "user_id";
    private static final String USER_NAME = "UserName";
    private static final String emailId = "email_id";
    private static final String userMobile = "user_mobile";
    int PRIVATE_MODE = 0;
    Context _context;
    Editor editor;
    SharedPreferences pref;

    public PrefManager(Context context) {
        this._context = context;
        this.pref = this._context.getSharedPreferences(PREF_NAME, this.PRIVATE_MODE);
        this.editor = this.pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        this.editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        this.editor.commit();
    }

    public void setIsUserLoggedIn(boolean isUserLoggedIn) {
        this.editor.putBoolean(IS_USER_LOGGED_IN, isUserLoggedIn);
        this.editor.commit();
    }

    public void setUserName(String username) {
        this.editor.putString(USER_NAME, username);
        this.editor.commit();
    }

    public String getUSER_Id() {
        return this.pref.getString(USER_Id, BuildConfig.FLAVOR);
    }

    public void setUSER_Id(String user_id) {
        this.editor.putString(USER_Id, user_id);
        this.editor.commit();
    }

    public void setUserMobile(String use) {
        this.editor.putString(userMobile, use);
        this.editor.commit();
    }

    public void setEmailId(String user_id) {
        this.editor.putString(emailId, user_id);
        this.editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return this.pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public boolean isUserLoggedIn() {
        return this.pref.getBoolean(IS_USER_LOGGED_IN, false);
    }

    public String getUsername() {
        return this.pref.getString(USER_NAME, BuildConfig.FLAVOR);
    }

    public String getUserMobile() {
        return this.pref.getString(userMobile, BuildConfig.FLAVOR);
    }

    public String getEmailId() {
        return this.pref.getString(emailId, BuildConfig.FLAVOR);
    }
}


/*
package in.org.cyberspace.taxi55;

*/
/**
 * Created by Wasim on 13/01/2017.
 *//*


import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;


    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "TAXI55";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String IS_USER_LOGGED_IN = "IsUserLoggedIn";
    private static final String USER_NAME = "UserName";
    private static final String USER_Id = "user_id";

    private static final String userMobile = "user_mobile";
    private static final String emailId = "email_id";


    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public void setIsUserLoggedIn(boolean isUserLoggedIn) {
        editor.putBoolean(IS_USER_LOGGED_IN, isUserLoggedIn);
        editor.commit();
    }

    public void setUserName(String username) {
        editor.putString(USER_NAME, username);
        editor.commit();
    }

    public String getUSER_Id() {
        return pref.getString(USER_Id, "");
    }


    public void setUSER_Id(String user_id) {
        editor.putString(USER_Id, user_id);
        editor.commit();

    }

    public void setUserMobile(String use) {
        editor.putString(userMobile, use);
        editor.commit();

    }

    public void setEmailId(String user_id) {
        editor.putString(emailId, user_id);
        editor.commit();

    }


    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_USER_LOGGED_IN, false);
    }

    public String getUsername() {
        return pref.getString(USER_NAME, "");
    }


    public String getUserMobile() {

        String mb= pref.getString(userMobile,"");
        return mb;
        // return userMobile;
    }

    public String getEmailId() {
        return pref.getString(emailId, "");
        //return emailId;
    }

}

*/
