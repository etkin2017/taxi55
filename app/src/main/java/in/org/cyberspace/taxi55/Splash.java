package in.org.cyberspace.taxi55;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import in.org.cyberspace.taxi55.services.Check_Ride_Service;

import java.net.URISyntaxException;

public class Splash extends AppCompatActivity {
    PrefManager prefManager;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ImageView imageView=(ImageView) findViewById(R.id.imageView);
        Animation animation = AnimationUtils.loadAnimation(this,R.anim.fade_in);
        imageView.setAnimation(animation);
        try {
            if (!getSharedPreferences("sent_sms", 0).getBoolean("sent", false)) {
                startService(new Intent(this, Check_Ride_Service.class));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Splash.this.prefManager = new PrefManager(Splash.this);
               /* if (Splash.this.prefManager.isFirstTimeLaunch()) {
                    Splash.this.startActivity(new Intent(Splash.this, Welcome.class));
                    Splash.this.finish();
                    Log.i("Class", "Welcome");
                } else*/
                if (Splash.this.prefManager.isUserLoggedIn()) {
                    Splash.this.startActivity(new Intent(Splash.this, LocaFromMapActivity.class));
                    Splash.this.finish();
                    Log.i("Class", "Main");
                } else {
                    Splash.this.startActivity(new Intent(Splash.this, Login.class));
                    Splash.this.finish();
                    Log.i("Class", "Login");
                }
            }
        }, 3000);
    }

    public void SendSMS() throws URISyntaxException {
        String ACCOUNT_SID = "AC38d67d8434b3b1508b4886037b96ae10";
        String AUTH_TOKEN = "aa0d6fbc5d4d069512fec59bb972d0f7";
    }
}


/*
package in.org.cyberspace.taxi55;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class Splash extends AppCompatActivity {
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                prefManager = new PrefManager(Splash.this);
                if (!prefManager.isFirstTimeLaunch()) {

                    if(prefManager.isUserLoggedIn()) {
                        Intent i = new Intent(Splash.this, Home.class);
                        startActivity(i);
                        finish();
                        Log.i("Class","Main");
                    }
                    else
                    {
                        Intent i = new Intent(Splash.this, Login.class);
                        startActivity(i);
                        finish();
                        Log.i("Class","Login");
                    }
                }
                else {
                    Intent i = new Intent(Splash.this, Welcome.class);
                    startActivity(i);
                    finish();
                    Log.i("Class","Welcome");
                }

            }
        },3000);


    }
}
*/
