package in.org.cyberspace.taxi55.fragment;


import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.org.cyberspace.taxi55.R;
import in.org.cyberspace.taxi55.application.AppUtils;
import in.org.cyberspace.taxi55.listener.GeocoderHelper;
import in.org.cyberspace.taxi55.services.FetchAddressIntentServiceForLocMap;
import in.org.cyberspace.taxi55.services.GPSTracker;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocFromMapFragment extends Fragment implements GoogleMap.OnCameraChangeListener, LocationListener, com.google.android.gms.maps.OnMapReadyCallback {

    View rootView;
    private static final AndroidHttpClient ANDROID_HTTP_CLIENT = AndroidHttpClient.newInstance(GeocoderHelper.class.getName());

    String cityName = "";
    private boolean running = false;
    AddressResultReceiver mResultReceiver;
    protected String mAddressOutput;
    protected String mAreaOutput;
    protected String mCityOutput;
    protected String mStateOutput;
    private LatLng mCenterLatLong;

    TextView main_road, full_address;
    Button confirm;
    private GoogleMap mMap;
    double mLatitude = 0.0;
    double mLongitude = 0.0;
    GPSTracker gps;
    String dropLng, dropLat;

    String f_add;
    String check_drop_pick;

    Bundle bundle_pickup;
    Bundle bundle_drop;

    String from = "";

    Bundle pickup_bundle_from_ride;

    public void setPickupBundleRide(Bundle pickup_bundle_from_ride) {
        this.pickup_bundle_from_ride = pickup_bundle_from_ride;
    }


    public void setPickUpLocationData(Bundle bundle_pickup) {
        this.bundle_pickup = bundle_pickup;
    }

    public void setDropLocationData(Bundle bundle_drop) {
        this.bundle_drop = bundle_drop;
    }


    public LocFromMapFragment() {
        // Required empty public constructor
    }


    public void init(View view) {
        main_road = (TextView) view.findViewById(R.id.main_road);
        main_road.setVisibility(View.GONE);
        full_address = (TextView) view.findViewById(R.id.full_address);
        confirm = (Button) view.findViewById(R.id.confirm);
        gps = new GPSTracker(getActivity());
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        try {

            SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                    .findFragmentById(R.id.mapView);
          /*  Fragment fragment = (getFragmentManager()
                    .findFragmentById(R.id.map2));*/
            FragmentTransaction ft = getActivity().getSupportFragmentManager()
                    .beginTransaction();
            ft.remove(mapFragment);
            ft.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_loc_from_map, container, false);

        check_drop_pick = getArguments().getString("drop_pick");

        from = getArguments().getString("from");

        init(rootView);


        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);


        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLatitude == 0.0 || mLongitude == 0.0) {
                    Toast.makeText(getActivity(), "select extact position", Toast.LENGTH_LONG).show();
                } else {
                    //  getActivity().getSupportFragmentManager().popBackStack();
                  /*  try {
                        List<Fragment> fragments = getActivity().getSupportFragmentManager().getFragments();
                        if (fragments != null) {
                            for (Fragment fragment : fragments) {

                                    getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
/*
                    try {
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                            fm.popBackStack();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/

                    if (gps.canGetLocation()) {

                        mLatitude = gps.getLatitude();
                        mLongitude = gps.getLongitude();

                    }

                    if (from.equals("1")) {


                        if (check_drop_pick.equals("1")) {
                            DashBoardFragment dashBoardFragment = new DashBoardFragment();
                            dashBoardFragment.setPickLocation(full_address.getText().toString());
                            dashBoardFragment.setmCenterLatLong(new LatLng(mLatitude, mLongitude));
                            dashBoardFragment.forPickLoc(dropLat, dropLng);

                            Bundle pickupBundle = new Bundle();
                            pickupBundle.putString("pickup_txt", full_address.getText().toString());
                            pickupBundle.putString("pickup_lat", dropLat);
                            pickupBundle.putString("pickup_lng", dropLng);
                            dashBoardFragment.setPickupLocationData(pickupBundle);


                            dashBoardFragment.setDropLocationData(bundle_drop);
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.activity_check_ace_cabs, dashBoardFragment)
                                    .addToBackStack("from_loc_dashboard_fragment1")
                                    .commit();
                        } else if (check_drop_pick.equals("2")) {


                            DashBoardFragment dashBoardFragment = new DashBoardFragment();
                            dashBoardFragment.setDropLocation(full_address.getText().toString());
                            dashBoardFragment.setmCenterLatLong(new LatLng(mLatitude, mLongitude));
                            dashBoardFragment.forFavplace(dropLat, dropLng);

                            Bundle dropBundle = new Bundle();
                            dropBundle.putString("drop_txt", full_address.getText().toString());
                            dropBundle.putString("drop_lat", dropLat);
                            dropBundle.putString("drop_lng", dropLng);

                            dashBoardFragment.setDropLocationData(dropBundle);

                            dashBoardFragment.setPickupLocationData(bundle_pickup);
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.activity_check_ace_cabs, dashBoardFragment)
                                    .addToBackStack("from_loc_dashboard_fragment")
                                    .commit();

                        }
                    } else {


                      /*  RouteFragment routeFragment = new RouteFragment();
                        Bundle dropBundle = new Bundle();
                        dropBundle.putString("drop_txt",full_address.getText().toString());
                        dropBundle.putString("drop_lat",dropLat);
                        dropBundle.putString("drop_lng", dropLng);
                        routeFragment.setBundleFromRoute(dropBundle);
                        routeFragment.setPickUpBundleFromRoute(pickup_bundle_from_ride);
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.activity_check_ace_cabs, routeFragment)
                                .addToBackStack("route_fragment4")
                                .commit();*/

                    }
                }
            }
        });

        return rootView;
    }

    protected void startIntentService(Location mLocation) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent intent = new Intent(getActivity(), FetchAddressIntentServiceForLocMap.class);

        // Pass the result receiver as an extra to the service.
        intent.putExtra(AppUtils.LocationConstants.RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        intent.putExtra(AppUtils.LocationConstants.LOCATION_DATA_EXTRA, mLocation);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        getActivity().startService(intent);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {


        dropLat = cameraPosition.target.latitude + "";
        dropLng = cameraPosition.target.longitude + "";

        Location mloc = new Location("");
        mloc.setLatitude(cameraPosition.target.latitude);
        mloc.setLongitude(cameraPosition.target.longitude);
      /*  mResultReceiver = new AddressResultReceiver(new Handler());
        startIntentService(mloc);*/

        cityName="";
        fetchCityName(getActivity(),mloc);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //    updateMarkOnMap();
        if (gps.canGetLocation()) {

            mLatitude = gps.getLatitude();
            mLongitude = gps.getLongitude();

        }
        mMap.setOnCameraChangeListener(null);
        updateMarkOnMap();

        dropLat = mLatitude + "";
        dropLng = mLongitude + "";


        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                dropLat = cameraPosition.target.latitude + "";
                dropLng = cameraPosition.target.longitude + "";

                Location mloc = new Location("");
                mloc.setLatitude(cameraPosition.target.latitude);
                mloc.setLongitude(cameraPosition.target.longitude);
              /*  mResultReceiver = new AddressResultReceiver(new Handler());
                startIntentService(mloc);*/
                cityName="";
                fetchCityName(getActivity(),mloc);

            }
        });
    }


    public void updateMarkOnMap() {

        if (gps.canGetLocation()) {

            mLatitude = gps.getLatitude();
            mLongitude = gps.getLongitude();

        }
        final LatLng loc1 = new LatLng(mLatitude, mLongitude);
        //    Marker m = mMap.addMarker(new MarkerOptions().position(loc1).title(note));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(loc1));
        //   mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));


        mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()), 10, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {

                mMap.moveCamera(CameraUpdateFactory.newLatLng(loc1));
                moveListenerforMap();


            }

            @Override
            public void onCancel() {

            }
        });

        Location mloc = new Location("");
        mloc.setLatitude(mLatitude);
        mloc.setLongitude(mLongitude);

       /* mResultReceiver = new AddressResultReceiver(new Handler());
        startIntentService(mloc);*/
        cityName="";
        fetchCityName(getActivity(),mloc);


    }

    private void moveListenerforMap() {
        mMap.setOnCameraChangeListener(this);
    }

    private int calculateZoomLevel() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        double equatorLength = 40075004; // in meters
        double widthInPixels = width;
        double metersPerPixel = equatorLength / 256;
        int zoomLevel = 1;
        while ((metersPerPixel * widthInPixels) > 3000) {
            metersPerPixel /= 2;
            ++zoomLevel;
        }
        Log.i("ADNAN", "zoom level = " + zoomLevel);
        return zoomLevel;
    }


    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string or an error message sent from the intent service.
            mAddressOutput = resultData.getString(AppUtils.LocationConstants.RESULT_DATA_KEY);

            String a = mAddressOutput.replace("\n", ",");
            Log.e("ADDRESSForLocMap", a);
            f_add = a;
            mAreaOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_AREA);

            mCityOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_CITY);
            mStateOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_STREET);

            if (mAreaOutput != null) {
                main_road.setText(mAreaOutput);
            } else {

            }
          /*  if (mCityOutput != null) {
                full_address.setText(mCityOutput);
            } else {

            }*/

            full_address.setText(a + "");


            if (resultCode == AppUtils.LocationConstants.SUCCESS_RESULT) {
                //  showToast(getString(R.string.address_found));


            }


        }

    }


    public void fetchCityName(final Context contex, final Location location) {
        if (running)
            return;
        new AsyncTask<Void, Void, String>() {
            protected void onPreExecute() {
                running = true;
            }

            ;

            @Override
            protected String doInBackground(Void... params) {


                if (Geocoder.isPresent()) {
                    try {
                        Geocoder geocoder = new Geocoder(contex, Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                       /* if (addresses.size() > 0) {
                            cityName = addresses.get(0).getLocality();
                        }*/


                        Address address = addresses.get(0);
                        ArrayList<String> addressFragments = new ArrayList<String>();

                        for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                            addressFragments.add(address.getAddressLine(i));

                        }

                        String add1 = TextUtils.join(System.getProperty("line.separator"), addressFragments);
                        cityName = add1.replace("\n", ",");


                    } catch (Exception ignored) {
                        // after a while, Geocoder start to trhow "Service not availalbe" exception. really weird since it was working before (same device, same Android version etc..
                    }
                }

                if (cityName.equals("")) // i.e., Geocoder succeed
                {
                    cityName = fetchCityNameUsingGoogleMap();
                } else // i.e., Geocoder failed
                {
                    Log.e("CityName", cityName.toString() + "  city");
                }
                return cityName;
            }

            // Geocoder failed :-(
            // Our B Plan : Google Map
            private String fetchCityNameUsingGoogleMap() {
                String googleMapUrl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + location.getLatitude() + ","
                        + location.getLongitude() + "&sensor=false&language=en";

                try {
                    JSONObject googleMapResponse = new JSONObject(ANDROID_HTTP_CLIENT.execute(new HttpGet(googleMapUrl),
                            new BasicResponseHandler()));

                    // many nested loops.. not great -> use expression instead
                    // loop among all results
                    JSONArray results = (JSONArray) googleMapResponse.get("results");
                    for (int i = 0; i < results.length(); i++) {
                        // loop among all addresses within this result
                        JSONObject result = results.getJSONObject(i);

                        cityName = result.getString("formatted_address").toString();

                        if (cityName != null) {
                            return cityName;
                        }

                      /*  if (result.has("address_components")) {
                            JSONArray addressComponents = result.getJSONArray("address_components");
                            // loop among all address component to find a 'locality' or 'sublocality'
                            for (int j = 0; j < addressComponents.length(); j++) {
                                JSONObject addressComponent = addressComponents.getJSONObject(j);
                                if (result.has("types")) {
                                    JSONArray types = addressComponent.getJSONArray("types");

                                    // search for locality and sublocality
                                    String cityName = null;

                                    for (int k = 0; k < types.length(); k++) {
                                        if ("locality".equals(types.getString(k)) && cityName == null) {
                                            if (addressComponent.has("long_name")) {
                                                cityName = addressComponent.getString("long_name");
                                            } else if (addressComponent.has("short_name")) {
                                                cityName = addressComponent.getString("short_name");
                                            }
                                        }
                                        if ("sublocality".equals(types.getString(k))) {
                                            if (addressComponent.has("long_name")) {
                                                cityName = addressComponent.getString("long_name");
                                            } else if (addressComponent.has("short_name")) {
                                                cityName = addressComponent.getString("short_name");
                                            }
                                        }
                                    }
                                    if (cityName != null) {
                                        return cityName;
                                    }
                                }
                            }
                        }*/
                    }
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(String cityName) {
                running = false;
                if (cityName != null) {
                    // Do something with cityName
                    Log.i("GeocoderHelper", cityName);


                    if (mAreaOutput != null) {
                        main_road.setText(mAreaOutput);
                    } else {

                    }
          /*  if (mCityOutput != null) {
                full_address.setText(mCityOutput);
            } else {

            }*/

                    full_address.setText(cityName + "");

                    mMap.clear();

                }
            }

            ;
        }.execute();
    }


}
