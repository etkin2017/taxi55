package in.org.cyberspace.taxi55;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.model.LatLng;

import in.org.cyberspace.taxi55.db.NotiDbHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SelectCab extends AppCompatActivity {
    String CAB_NAME = BuildConfig.FLAVOR;
    float DIST = 0.0f;
    LatLng DROP = null;
    String DURATION = BuildConfig.FLAVOR;
    LatLng PICK = null;
    String T_FARE = BuildConfig.FLAVOR;
    String TravelDate;
    String TravelDateTime;
    String TravelTime;
    Button btnDatePicker;
    Button btnTimePicker;
    int dist;
    String dropAdd = BuildConfig.FLAVOR;
    private int mDay;
    private int mHour;
    private int mMinute;
    private int mMonth;
    private int mYear;
    String picAdd = BuildConfig.FLAVOR;
    TextView txtDate;
    TextView txtTime;
    ImageView time, date;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_cab);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.TravelDateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(new Date());
        this.TravelDate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        this.TravelTime = new SimpleDateFormat("HH:mm").format(new Date());
        Bundle bundle = getIntent().getExtras();
        this.picAdd = bundle.getString("PICK_ADD");
        this.dropAdd = bundle.getString("DROP_ADD");
        this.PICK = (LatLng) bundle.get("PICK_LOC");
        this.DROP = (LatLng) bundle.get("DROP_LOC");
        TextView tvPickAdd = (TextView) findViewById(R.id.tvPicAdd);
        TextView tvDropAdd = (TextView) findViewById(R.id.tvDropAdd);
        TextView tvDist = (TextView) findViewById(R.id.tvDistance);
        TextView tvDuration = (TextView) findViewById(R.id.tvDuration);
        this.txtDate = (TextView) findViewById(R.id.tvDate);
        this.txtTime = (TextView) findViewById(R.id.tvTime);

        time = (ImageView) findViewById(R.id.time);
        date = (ImageView) findViewById(R.id.date);


        time.setColorFilter(getResources().getColor(R.color.colorAccent));

        date.setColorFilter(getResources().getColor(R.color.colorAccent));
        //  this.txtDate.setText("Travel Date : " + this.TravelDate);
        //  this.txtTime.setText("Travel Time : " + this.TravelTime);
        tvPickAdd.setText(this.picAdd);
        tvDropAdd.setText(this.dropAdd);
        tvDist.setText("Total Distance : " + bundle.getString("DIST"));
        tvDuration.setText("Travel Duration : " + bundle.getString("DURATION"));
        String dist = bundle.getString("DIST");
        if (dist.endsWith(" km")) {
            dist = dist.substring(0, dist.length() - 3);
        } else if (dist.endsWith(" m")) {
            dist = dist.substring(0, dist.length() - 2);
        }
        this.DIST = Float.parseFloat(dist);
        this.DURATION = bundle.getString("DURATION");

        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDate(v);
            }
        });
        txtTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeTime(v);
            }
        });

    }

    public void changeTime(View v) {
        Calendar c = Calendar.getInstance();
        this.mHour = c.get(Calendar.HOUR_OF_DAY);
        this.mMinute = c.get(Calendar.MINUTE);
        new TimePickerDialog(this, new OnTimeSetListener() {
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                if (minute <= 9) {
                    SelectCab.this.txtTime.setText("Travel Time : " + hourOfDay + ":0" + minute);
                    SelectCab.this.TravelTime = hourOfDay + ":0" + minute;
                } else {
                    SelectCab.this.txtTime.setText("Travel Time : " + hourOfDay + ":" + minute);
                    SelectCab.this.TravelTime = hourOfDay + ":" + minute;
                }
                SelectCab.this.TravelDateTime = SelectCab.this.TravelDate + " " + SelectCab.this.TravelTime;
            }
        }, this.mHour, this.mMinute, false).show();
    }

    public void changeDate(View v) {
        Calendar c = Calendar.getInstance();
        this.mYear = c.get(Calendar.YEAR);
        this.mMonth = c.get(Calendar.MONTH);
        this.mDay = c.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(this, new OnDateSetListener() {


            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                SelectCab.this.txtDate.setText("Travel Date : " + dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                SelectCab.this.TravelDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                SelectCab.this.TravelDateTime = SelectCab.this.TravelDate + " " + SelectCab.this.TravelTime;
            }
        }, this.mYear, this.mMonth, this.mDay).show();
    }

    public void fetchCabs(String strTime) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("book_date", this.TravelDateTime);
            jsonObject.put("transfertype", "Point to Point Transfer");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("data", jsonObject.toString());
        final ProgressDialog pdlg = new ProgressDialog(this);
        pdlg.setMessage("Please Wait...");
        pdlg.show();
        pdlg.setCancelable(false);
        String REQUEST_TAG = getPackageName() + ".volleyJsonArrayRequest";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "fetch_cab_details", jsonObject, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                Log.i("Response", response.toString());
                pdlg.dismiss();
                try {
                    if (response.getString("status").equals("success")) {
                        JSONArray jsonArray = response.getJSONArray("cabs");
                        final String[] CABS = new String[jsonArray.length()];
                        final String[] FARE = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject cab = jsonArray.getJSONObject(i);
                            CABS[i] = cab.getString("cartype");
                            int fare = cab.getInt("standardrate");
                            int initKM = cab.getInt("intialkm");
                            FARE[i] = "Fare : " + String.valueOf(Math.round(((float) fare) * (SelectCab.this.DIST - ((float) initKM))) + (initKM * cab.getInt("intailrate")));
                        }
                        final Dialog dlg = new Dialog(SelectCab.this);
                        dlg.setTitle("Select Taxi");
                        dlg.setContentView(R.layout.taxi_list);
                        ListView list = (ListView) dlg.findViewById(R.id.listView1);
                        list.setAdapter(new CabListAdapter(SelectCab.this, CABS, FARE));
                        list.setOnItemClickListener(new OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                SelectCab.this.CAB_NAME = CABS[i];
                                SelectCab.this.T_FARE = FARE[i];
                                dlg.dismiss();
                                HashMap<String, String> bookingData = new HashMap();
                                bookingData.put("token", new PrefManager(SelectCab.this).getUsername());
                                bookingData.put("book_date", SelectCab.this.TravelDateTime);
                                bookingData.put("drop_area", SelectCab.this.dropAdd);
                                bookingData.put("pickup_area", SelectCab.this.picAdd);
                                bookingData.put("taxi_type", CABS[i]);
                                bookingData.put("amount", FARE[i]);
                                bookingData.put("km", String.valueOf(SelectCab.this.DIST));
                                bookingData.put("duration", SelectCab.this.DURATION);
                                bookingData.put("pickup_lat", SelectCab.this.PICK.latitude + BuildConfig.FLAVOR);
                                bookingData.put("pickup_lng", SelectCab.this.PICK.longitude + BuildConfig.FLAVOR);
                                bookingData.put("drop_lat", SelectCab.this.DROP.latitude + BuildConfig.FLAVOR);
                                bookingData.put("drop_lng", SelectCab.this.DROP.longitude + BuildConfig.FLAVOR);
                                bookingData.put("travel_time", SelectCab.this.TravelTime);
                                bookingData.put("travel_date", SelectCab.this.TravelDate);
                                bookingData.put("from", SelectCab.this.getIntent().getStringExtra("from"));
                                Intent intent = new Intent(SelectCab.this, ConfirmBooking.class);
                                intent.putExtra("DATA", bookingData);
                                SelectCab.this.startActivity(intent);
                                SelectCab.this.finish();
                            }
                        });
                        dlg.show();
                        return;
                    }
                    Toast.makeText(SelectCab.this, response.getString(NotiDbHelper.msg), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.i("Response", error.toString());
                pdlg.dismiss();
                Toast.makeText(SelectCab.this, "Please Try Again", Toast.LENGTH_SHORT).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, REQUEST_TAG);
    }

    public void select_taxi(View v) {

        if (TextUtils.isEmpty(txtDate.getText().toString())) {
            showAlertDialog("Please Select Date");
        } else if (TextUtils.isEmpty(txtTime.getText().toString())) {
            showAlertDialog("Please Select Time");
        } else {
            fetchCabs(this.TravelDateTime);
        }
    }

    public void showAlertDialog(String mesg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(mesg);
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.dismiss();
                        // Toast.makeText(MainActivity.this,"You clicked yes button",Toast.LENGTH_LONG).show();
                    }
                });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}


