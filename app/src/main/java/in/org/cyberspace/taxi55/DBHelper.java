package in.org.cyberspace.taxi55;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import in.org.cyberspace.taxi55.model.ContactNumber;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {
    private static final String CONTACT_LIST = "contact_list";
    static final String DB_NAME = "TAXI55_DB";
    final String USER_TABLE = "USER_DATA";
    Cursor cursor;
    Context mcontext;
    SQLiteDatabase sqLiteDatabase;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.mcontext = context;
    }

    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("Create table USER_DATA (username text primary key, password text,email text,mobile text,token text);");
        sqLiteDatabase.execSQL("CREATE TABLE contact_list(NAME TEXT , NUMBER TEXT)");
    }

    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("Drop Table if exist USER_DATA");
        onCreate(sqLiteDatabase);
    }

    public void saveUserData(String username, String password, String email, String mobile, String token) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Delete from USER_DATA");
        ContentValues values = new ContentValues();
        values.put("username", username);
        values.put("email", email);
        values.put("password", password);
        values.put("mobile", mobile);
        values.put("token", token);
        db.insert("USER_DATA", null, values);
    }

    public Cursor getUserData() {
        return getReadableDatabase().rawQuery("Select * from USER_DATA", null);
    }

    public HashMap<String, String> get_userData() {
        HashMap<String, String> nmap = new HashMap();
        Cursor c = getReadableDatabase().rawQuery("Select * from USER_DATA", null);
        try {
            if (c.moveToFirst()) {
                nmap.put("username", c.getString(0));
                nmap.put("name", c.getString(1));
                nmap.put("email", c.getString(3));
                nmap.put("mobile", c.getString(4));
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }
        return nmap;
    }

    public void addMultipleContact(List<ContactNumber> contactNumberList) {
        try {
            SQLiteDatabase db = getWritableDatabase();
            for (int i = 0; i < contactNumberList.size(); i++) {
                ContentValues values;
                if (check_empid(((ContactNumber) contactNumberList.get(i)).getNumber())) {
                    try {
                        values = new ContentValues();
                        values.put("NUMBER", ((ContactNumber) contactNumberList.get(i)).getNumber());
                        values.put("NAME", ((ContactNumber) contactNumberList.get(i)).getName());
                        db.update(CONTACT_LIST, values, " NUMBER = '" + ((ContactNumber) contactNumberList.get(i)).getNumber() + "'", null);
                    } catch (Exception ee1) {
                        ee1.printStackTrace();
                    }
                } else {
                    try {
                        values = new ContentValues();
                        values.put("NUMBER", ((ContactNumber) contactNumberList.get(i)).getNumber());
                        values.put("NAME", ((ContactNumber) contactNumberList.get(i)).getName());
                        db.insert(CONTACT_LIST, null, values);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            db.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean check_empid(String id) {
        this.sqLiteDatabase = getReadableDatabase();
        this.cursor = this.sqLiteDatabase.rawQuery("SELECT * FROM contact_list where NUMBER='" + id + "'", null);
        if (this.cursor == null || !this.cursor.moveToFirst()) {
            return false;
        }
        return true;
    }

    public List<ContactNumber> allContacts() {
        List<ContactNumber> recent_placeList = new ArrayList();
        this.sqLiteDatabase = getReadableDatabase();
        this.cursor = this.sqLiteDatabase.rawQuery("SELECT * FROM contact_list", null);
        try {
            if (this.cursor != null && this.cursor.moveToFirst()) {
                do {
                    recent_placeList.add(new ContactNumber(this.cursor.getString(0), this.cursor.getString(1)));
                } while (this.cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return recent_placeList;
    }
}


