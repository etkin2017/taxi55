package in.org.cyberspace.taxi55.model;

public class User {
    String contact;
    String name;

    public User(String contact, String name) {
        this.contact = contact;
        this.name = name;
    }

    public String getContact() {
        return this.contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
/*
package in.org.cyberspace.taxi55.model;

*/
/**
 * Created by Ashvini on 3/8/2017.
 *//*


public class User {

    String contact,name;

    public User(String contact, String name) {
        this.contact = contact;
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
*/
