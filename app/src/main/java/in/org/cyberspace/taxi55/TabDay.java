package in.org.cyberspace.taxi55;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Wasim on 17/02/2017.
 */

public class TabDay extends Fragment {

    String[] N_CAB;
    String[] D_CAB;
    String[] N_Initial;
    String[] D_Initial;
    String[] D_Rest;
    String[] N_Rest;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_day, container, false);

        D_CAB=getArguments().getStringArray("CAB");
        D_Initial=getArguments().getStringArray("INIT");
        D_Rest=getArguments().getStringArray("REST");

        Log.i("Frag CABS",D_CAB.length+"");

        ListView listView = (ListView) v.findViewById(R.id.listView1);
        RateListAdapter adapter = new RateListAdapter(getActivity(),D_CAB,D_Initial,D_Rest);

        listView.setAdapter(adapter);

        return v;



    }


    public void fetchRates()
    {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("transfertype","Point to Point Transfer");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL+"load_card_rate", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Response",response.toString());
                progressDialog.dismiss();
                try {
                    JSONArray day=response.getJSONArray("day");
                    JSONArray nigth=response.getJSONArray("night");
                    D_CAB=new String[day.length()];
                    D_Initial=new String[day.length()];
                    D_Rest=new String[day.length()];

                    N_CAB=new String[nigth.length()];
                    N_Initial=new String[nigth.length()];
                    N_Rest=new String[nigth.length()];


                    for(int i=0;i<day.length();i++)
                    {
                        JSONObject jday=day.getJSONObject(i);
                        D_CAB[i]=jday.getString("cartype");
                        String initKM=jday.getString("intialkm");
                        String initRate=jday.getString("intailrate");

                        String init_str="First "+initKM+" KM For "+initRate;
                        D_Initial[i]=init_str;
                        D_Rest[i]="Rest For "+jday.getString("standardrate")+" / KM";
                    }


                    for(int i=0;i<nigth.length();i++)
                    {
                        JSONObject jday=nigth.getJSONObject(i);
                        N_CAB[i]=jday.getString("cartype");
                        String initKM=jday.getString("intialkm");
                        String initRate=jday.getString("intailrate");

                        String init_str="First "+initKM+" KM For "+initRate;
                        N_Initial[i]=init_str;
                        N_Rest[i]="Rest For "+jday.getString("standardrate")+" / KM";
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        });
    }
}
