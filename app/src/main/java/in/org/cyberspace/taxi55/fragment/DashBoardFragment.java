package in.org.cyberspace.taxi55.fragment;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;


import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import in.org.cyberspace.taxi55.R;
import in.org.cyberspace.taxi55.SelectCab;
import in.org.cyberspace.taxi55.application.AppUtils;
import in.org.cyberspace.taxi55.application.DAO;
import in.org.cyberspace.taxi55.listener.GeocoderHelper;
import in.org.cyberspace.taxi55.listener.MapStateListener;
import in.org.cyberspace.taxi55.model.DirectParser;
import in.org.cyberspace.taxi55.services.ConnectivityReceiver;
import in.org.cyberspace.taxi55.services.FetchAddressIntentService;
import in.org.cyberspace.taxi55.services.GPSTracker;
import in.org.cyberspace.taxi55.views.TouchableMapFragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashBoardFragment extends Fragment implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult>,
        com.google.android.gms.location.LocationListener,
        com.google.android.gms.maps.OnMapReadyCallback,
        GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraChangeListener,
        ConnectivityReceiver.ConnectivityReceiverListener {
    LatLng PICK = null, DROP = null;
    View rootView;
//    SupportMapFragment mapFragment;

    String cityName = "";


    private static final AndroidHttpClient ANDROID_HTTP_CLIENT = AndroidHttpClient.newInstance(GeocoderHelper.class.getName());

    private boolean running = false;

    TouchableMapFragment mapFragment;
    GPSTracker gps;
    ProgressDialog pd;
    private AddressResultReceiver mResultReceiver;
    Location location;

    protected String mAddressOutput;
    protected String mAreaOutput;
    protected String mCityOutput;
    protected String mStateOutput;
    private LatLng mCenterLatLong;

    private GoogleMap mMap;
    double mLatitude = 0;
    double mLongitude = 0;


    double mLatitude1 = 0;
    double mLongitude1 = 0;

    LocationManager locationManager;
    String locationProvider;

    String locname = "";
    TextView map_toolbar;


    GoogleMap.OnCameraChangeListener cont;

    AppBarLayout appBarLayout;

    String drop_lat1 = "", drop_lang2 = "";
    String pick_lng1 = "", pick_lat1 = "";


    //Pick-up and Drop Layout

    TextView pick_up_txt, drop_txt;

    ImageView drop_search, pick_up_search;

    //loc lat n lng;

    ImageView next;
    LatLng pickup_lat_lng, drop_lat_lng;


    TextView save_pick_up_txt, save_pick_up_lat, save_pick_up_lng, save_drop_txt, save_drop_lat, save_drop_lng, time_of_taxi;

    String pickAddress = "", dropAddress = "", DIST = "", DURATION = "";
    String lat = "", lng = "";

    String dropLocation = "", pickLocation = "";
    ImageView pointer;
    Bundle pickupLocationDataBundle, dropupLocationDataBundle;

    Bundle savedInstanceState;

    private GoogleApiClient mGoogleApiClient;
    private final String TAG = "DashBoardFragment";
    private final long INTERVAL = 1000 * 10;//10000
    private final long FASTEST_INTERVAL = 1000 * 5;
    private final long DISPLACEMENT = 1000 * 10;//10000
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;

    private boolean release = false;


    private void forPickUpLov() {
        if (pickLocation.equals("")) {

            if (location != null) {
                onLocationChanged(location);
            }
            updateMarkOnMap();
        } else {

            //updateCamera();

            updateCameraByPickUpLocation(Double.parseDouble(save_pick_up_lat.getText().toString()), Double.parseDouble(save_pick_up_lng.getText().toString()));

        }


    }

    public void setPickBundleData() {
        try {

            if (pickupLocationDataBundle.getString("pickup_txt").equals("") && pickupLocationDataBundle.getString("pickup_lat").equals("") && pickupLocationDataBundle.getString("pickup_lng").equals("")) {

            } else {
                pick_up_txt.setText(pickupLocationDataBundle.getString("pickup_txt"));
                save_pick_up_txt.setText(pickupLocationDataBundle.getString("pickup_txt"));
                save_pick_up_lat.setText(pickupLocationDataBundle.getString("pickup_lat"));
                save_pick_up_lng.setText(pickupLocationDataBundle.getString("pickup_lng"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDropBundleData() {
        try {

            if (dropupLocationDataBundle.getString("drop_txt").equals("") && dropupLocationDataBundle.getString("drop_lat").equals("") && dropupLocationDataBundle.getString("drop_lng").equals("")) {
                mResultReceiver = new AddressResultReceiver(new Handler());
            } else {
                drop_txt.setText(dropupLocationDataBundle.getString("drop_txt"));
                save_drop_txt.setText(dropupLocationDataBundle.getString("drop_txt"));
                save_drop_lat.setText(dropupLocationDataBundle.getString("drop_lat"));
                save_drop_lng.setText(dropupLocationDataBundle.getString("drop_lng"));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public DashBoardFragment() {

    }

    public void forFavplace(String drop_lat11, String drop_lang22) {
        this.drop_lat1 = drop_lat11;
        this.drop_lang2 = drop_lang22;
    }


    public void forPickLoc(String pick_lng1, String pick_lat1) {
        this.pick_lng1 = pick_lng1;
        this.pick_lat1 = pick_lat1;
    }


    public void setPickLocation(String pickLocation) {
        this.pickLocation = pickLocation;
    }

    public void setDropLocation(String dropLocation) {
        this.dropLocation = dropLocation;
    }


    public void setPickupLocationData(Bundle pickupLocationDataBundle) {

        this.pickupLocationDataBundle = pickupLocationDataBundle;
        try {
            Log.e("pick_up_data", pickupLocationDataBundle.getString("pickup_txt") + ":" + pickupLocationDataBundle.getString("pickup_lat") + ":" + pickupLocationDataBundle.getString("pickup_lng"));
        } catch (Exception eee) {
            eee.printStackTrace();
        }
    }

    public void setDropLocationData(Bundle dropupLocationDataBundle) {
        this.dropupLocationDataBundle = dropupLocationDataBundle;

        try {
            Log.e("drop_data", dropupLocationDataBundle.getString("drop_txt") + ":" + dropupLocationDataBundle.getString("drop_lat") + ":" + dropupLocationDataBundle.getString("drop_lng"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init(View view) {


        pd = new ProgressDialog(getActivity());
        pd.setMessage("Plz wait while fetching details....");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        gps = new GPSTracker(getActivity());

        pointer = (ImageView) view.findViewById(R.id.pointer);
        map_toolbar = (TextView) view.findViewById(R.id.map_toolbar);


        pick_up_txt = (TextView) view.findViewById(R.id.pick_up_txt);
        drop_txt = (TextView) view.findViewById(R.id.drop_txt);


        save_pick_up_txt = (TextView) view.findViewById(R.id.save_pick_up_text);
        save_pick_up_lat = (TextView) view.findViewById(R.id.save_pick_up_lat);
        save_pick_up_lng = (TextView) view.findViewById(R.id.save_pick_up_lng);
        save_drop_txt = (TextView) view.findViewById(R.id.save_drop_text);
        save_drop_lat = (TextView) view.findViewById(R.id.save_drop_lat);
        save_drop_lng = (TextView) view.findViewById(R.id.save_drop_lng);


        drop_search = (ImageView) view.findViewById(R.id.drop_search);
        pick_up_search = (ImageView) view.findViewById(R.id.pick_up_search);

        pick_up_search.setColorFilter(getResources().getColor(R.color.sky_blue));

        drop_search.setColorFilter(getResources().getColor(R.color.sky_blue));


        try {

            if (pickupLocationDataBundle == null) {
                mResultReceiver = new AddressResultReceiver(new Handler());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {


            if (dropLocation.equals("")) {

            } else {
                drop_txt.setText(dropLocation + "");
                save_drop_txt.setText(dropLocation + "");
                save_drop_lat.setText(drop_lat1 + "");
                save_drop_lng.setText(drop_lang2 + "");

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        try {


            if (pickLocation.equals("")) {

            } else {
                pick_up_txt.setText(pickLocation + "");
                save_pick_up_txt.setText(pickLocation + "");
                save_pick_up_lat.setText(pick_lat1 + "");
                save_pick_up_lng.setText(pick_lng1 + "");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (gps.canGetLocation()) {

            mLatitude = gps.getLatitude();
            mLongitude = gps.getLongitude();

        }


        this.locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);


        //define the location manager criteria
        Criteria criteria = new Criteria();

        this.locationProvider = locationManager.getBestProvider(criteria, false);

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        location = locationManager.getLastKnownLocation(locationProvider);


    }


    @Override
    public void onPause() {
        super.onPause();
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        this.locationManager.removeUpdates(this);

        //  onDestroyView();

    }

    @Override
    public void onStop() {
        super.onStop();
        // killOldMap();


        if (savedInstanceState != null) {
            onDestroyView();
        } else {
            //  Toast.makeText(getActivity(), "Stoped", Toast.LENGTH_LONG).show();
        }

    }


    public void noCamChangeListener() {
        mMap.setOnCameraChangeListener(null);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);

        mMap.setOnCameraMoveStartedListener(null);
        mMap.setOnCameraMoveListener(null);
        noCamChangeListener();


        new MapStateListener(googleMap, mapFragment, getActivity()) {
            @Override
            public void onMapTouched() {
                release = false;

            }

            @Override
            public void onMapReleased() {

                if (dropupLocationDataBundle != null && pickupLocationDataBundle != null) {
                    StopCameraListener();
                    mMap.setOnCameraMoveStartedListener(null);
                    mMap.setOnCameraMoveListener(null);
                    noCamChangeListener();
                } else {
                    moveListenerforMap();
                    release = true;
                }

            }

            @Override
            public void onMapUnsettled() {
                release = false;

            }

            @Override
            public void onMapSettled() {
                release = true;

            }
        };


        if (dropupLocationDataBundle != null && pickupLocationDataBundle != null) {


            setDropBundleData();
            setPickBundleData();
            // updateCamera();
            release = false;

            StopCameraListener();

            updateCameraByPickUpLocation(Double.parseDouble(save_pick_up_lat.getText().toString()), Double.parseDouble(save_pick_up_lng.getText().toString()));
        } else if (pickupLocationDataBundle != null && dropupLocationDataBundle == null) {
            forPickUpLov();
        } else {
            updateMarkOnMap();

        }

        if (!TextUtils.isEmpty(pick_up_txt.getText().toString()) && !TextUtils.isEmpty(drop_txt.getText().toString())) {

            final double src_lat = Double.parseDouble(save_pick_up_lat.getText().toString());
            final double src_lng = Double.parseDouble(save_pick_up_lng.getText().toString());
            final double dest_lat = Double.parseDouble(save_drop_lat.getText().toString());
            final double dest_lng = Double.parseDouble(save_drop_lng.getText().toString());

            pickup_lat_lng = new LatLng(src_lat, src_lng);

            drop_lat_lng = new LatLng(dest_lat, dest_lng);

            new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] params) {
                    //  return null;
                    try {
                        getDistanceByDao(src_lat, src_lng, dest_lat, dest_lng);

                    } catch (Exception ee) {
                        ee.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                    try {


                        String url = getDirectionsUrl(pickup_lat_lng, drop_lat_lng);
                        DownloadTask downloadTask = new DownloadTask();
// Start downloading json data from Google Directions API
                        downloadTask.execute(url);
                    } catch (Exception ee) {
                        ee.printStackTrace();
                    }

                }
            }.execute();


        }


    }


    public void setmCenterLatLong(LatLng lnglat) {
        mLatitude1 = lnglat.latitude;
        mLongitude1 = lnglat.longitude;

    }


    public void updateCameraByPickUpLocation(final double lat11, final double lng11) {


        final LatLng loc2 = new LatLng(lat11, lng11);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(loc2));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()), 10, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                mMap.moveCamera(CameraUpdateFactory.newLatLng(loc2));
                noCamChangeListener();
                mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()), 1, new GoogleMap.CancelableCallback() {
                    @Override
                    public void onFinish() {
                        //   moveListenerforMap();

                    }

                    @Override
                    public void onCancel() {

                    }
                });

            }

            @Override
            public void onCancel() {

            }
        });


    }

    public void getDistanceByDao(final double src_latt, final double dest_lngg, final double latt, final double lngg) {


        new AsyncTask() {
            JSONObject json = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Object doInBackground(Object[] params) {


                try {
                    String url1 = "http://maps.google.com/maps/api/directions/json?origin=" + src_latt + "," + dest_lngg + "&destination=" + latt + "," + lngg + "&sensor=false&units=metric";

                    Log.e("print_url", url1);
                    json = DAO.getResult(url1);

                } catch (Exception ee) {
                    ee.printStackTrace();
                }

                return json;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                  /*  for (int j = 0; j < json.length; j++) {
                        try {*/

                    //   JSONObject jObject = new JSONObject(json[j]);
                    JSONArray jRoutes = json.getJSONArray("routes");
                    JSONArray jLegs = ((JSONObject) jRoutes.get(0)).getJSONArray("legs");

                    JSONObject jDistance = ((JSONObject) jLegs.get(0)).getJSONObject("distance");


                    String jDist = jDistance.getString("text");

                    DIST = jDist;
                           /* if(jDist.contains("hours")){
                                jDist.replace("hours","hr");
                            }
*/

                    /** Getting duration from the json data */
                    JSONObject jDuration = ((JSONObject) jLegs.get(0)).getJSONObject("duration");

                    DURATION = jDuration.getString("text");

                    if (DURATION.contains("hours")) {
                        DURATION.replace("hours", "hr");
                    }


                } catch (Exception e1) {
                    e1.printStackTrace();
                }


                   /* }


                } catch (Exception ee) {
                    ee.printStackTrace();
                }*/
                try {
                  /*  for (int k = 0; k < lis_bdp.size(); k++) {
                        Log.e("Drivers", "name : " + lis_bdp.get(k).getTaxi_service_name() +
                                " distance : " + lis_bdp.get(k).getRate() +
                                " time : " + lis_bdp.get(k).getTime());
                    }*/


                } catch (Exception e1) {
                    e1.printStackTrace();
                }


            }
        }.execute();

    }

    //as
    public HashMap<String, String> getDistance(double lat1, double lon1, double lat2, double lon2) {
        final HashMap<String, String> dist_n_dura = new HashMap<>();
        System.out.println(lat1 + " " + lon1 + " " + lat2 + " " + lon2);
        final String url1 = "http://maps.google.com/maps/api/directions/json?origin=" + lat1 + "," + lon1 + "&destination=" + lat2 + "," + lon2 + "&sensor=false&units=metric";


        // return dist_n_dura;

        new AsyncTask() {
            HttpResponse response = null;
            String data = "";
            JSONArray jRoutes = null;
            JSONArray jLegs = null;
            InputStream iStream = null;
            HttpURLConnection urlConnection = null;

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    try {
                        URL url = new URL(url1);
                        // Creating an http connection to communicate with url
                        urlConnection = (HttpURLConnection) url.openConnection();
                        // Connecting to url
                        urlConnection.connect();
                        // Reading data from url
                        iStream = urlConnection.getInputStream();
                        BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
                        StringBuffer sb = new StringBuffer();
                        String line = "";
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                        }
                        data = sb.toString();
                        br.close();

                    } catch (Exception e) {
                        Log.d("Excep downloading url", e.toString());
                    } finally {
                        iStream.close();
                        urlConnection.disconnect();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return data;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {

                    JSONObject jObject = new JSONObject(data);
                    jRoutes = jObject.getJSONArray("routes");
                    jLegs = ((JSONObject) jRoutes.get(0)).getJSONArray("legs");

                    String jDistance = ((JSONObject) jLegs.get(0)).getJSONObject("distance") + "";
                    dist_n_dura.put("dist", jDistance);

                    /** Getting duration from the json data */
                    String jDuration = ((JSONObject) jLegs.get(0)).getJSONObject("duration") + "";
                    dist_n_dura.put("dura", jDuration);


                } catch (Exception ee) {
                    ee.printStackTrace();
                }


            }
        }.execute();

        return dist_n_dura;
    }


    public void updateMarkOnMap() {

        gps = new GPSTracker(getActivity());

        if (gps.canGetLocation()) {

            mLatitude = gps.getLatitude();
            mLongitude = gps.getLongitude();

        }
        final LatLng loc1 = new LatLng(mLatitude, mLongitude);
        //    Marker m = mMap.addMarker(new MarkerOptions().position(loc1).title(note));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(loc1));
        // mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));

        mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()), 10, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {

                mMap.moveCamera(CameraUpdateFactory.newLatLng(loc1));
                //  moveListenerforMap();
             /*   AddressGeocodingTask ad = new AddressGeocodingTask(getActivity());
                ad.execute(loc1);*/

                Location aloc = new Location("");
                aloc.setLatitude(loc1.latitude);
                aloc.setLongitude(loc1.longitude);
                fetchCityName(getActivity(), aloc);


            }

            @Override
            public void onCancel() {

            }
        });
        lat = mLatitude + "";
        lng = mLongitude + "";


    }

    public void moveListener() {
        mMap.setOnCameraMoveStartedListener(this);
        mMap.setOnCameraMoveListener(this);

    }


    public void StopCameraListener() {
        onCameraChange(null);
        mMap.setOnCameraMoveStartedListener(null);
        mMap.setOnCameraMoveListener(null);

    }

    public void moveListenerforMap() {
        mMap.setOnCameraChangeListener(this);
    }


    protected void startIntentService(Location mLocation) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        try {
            Intent intent = new Intent(getActivity(), FetchAddressIntentService.class);

            // Pass the result receiver as an extra to the service.
            intent.putExtra(AppUtils.LocationConstants.RECEIVER, mResultReceiver);

            // Pass the location data as an extra to the service.
            intent.putExtra(AppUtils.LocationConstants.LOCATION_DATA_EXTRA, mLocation);

            // Start the service. If the service isn't already running, it is instantiated and started
            // (creating a process for it if needed); if it is running then it remains running. The
            // service kills itself automatically once all intents are processed.
            getActivity().startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private int calculateZoomLevel() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        double equatorLength = 40075004; // in meters
        double widthInPixels = width;
        double metersPerPixel = equatorLength / 256;
        int zoomLevel = 1;
        while ((metersPerPixel * widthInPixels) > 3000) {
            metersPerPixel /= 2;
            ++zoomLevel;
        }
        Log.i("ADNAN", "zoom level = " + zoomLevel);
        return zoomLevel;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i("called", "onStatusChanged");
    }

    @Override
    public void onProviderEnabled(String provider) {

        Log.i("called", "onProviderEnabled");

    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.i("called", "onProviderDisabled");
    }


    @Override
    public void onCameraMove() {
        //  Toast.makeText(getActivity(), "The camera is moving.", Toast.LENGTH_SHORT).show();
        //initializeLocationManager();


        Location location1 = new Location("");
        double lt1 = mMap.getCameraPosition().target.latitude;
        double ln1 = mMap.getCameraPosition().target.longitude;

        location1.setLatitude(lt1);
        location1.setLongitude(ln1);

        //initialize the location
        if (location1 != null) {
            onLocationChanged(location1);
        }

    }

    @Override
    public void onCameraMoveStarted(int reason) {

      /*  if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            Toast.makeText(getActivity(), "The user gestured on the map.", Toast.LENGTH_SHORT).show();
        } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_API_ANIMATION) {
            Toast.makeText(getActivity(), "The user tapped something on the map.", Toast.LENGTH_SHORT).show();
        } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_DEVELOPER_ANIMATION) {
            Toast.makeText(getActivity(), "The app moved the camera.", Toast.LENGTH_SHORT).show();
        }*/

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        moveListener();
        checkLocationSettings();

        try {
          /*  AddressGeocodingTask ad = new AddressGeocodingTask(getActivity());
            ad.execute(new LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude));*/

            Location locat = new Location("");
            locat.setLatitude(cameraPosition.target.latitude);
            locat.setLongitude(cameraPosition.target.longitude);

            cityName ="";
            fetchCityName(getActivity(), locat);

        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    public boolean chckConnect() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }


    public void showSnack(boolean isConnected) {

        String message;
        int color;
        if (isConnected) {
            // message = "Good! Connected to Internet";
            color = Color.WHITE;
        } else {
            message = "Sorry! Not connected to internet, Can't reach the Taxi network";
            color = Color.WHITE;
            Snackbar snackbar = Snackbar
                    .make(drop_search, message, Snackbar.LENGTH_LONG);

            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }


    }

    /*@Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {

    }*/


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.d("Onstart", "onStart fired ..............");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult result) {
        final Status status = result.getStatus();
        //                final LocationSettingsStates state = result.getStatus().ge
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // All location settings are satisfied. The client can initialize location
                startLocationUpdates();
                // Toast.makeText(getActivity(), "SUCCESS", Toast.LENGTH_SHORT).show();

                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                // Location settings are not satisfied. But could be fixed by showing the user
                // a dialog.
                //Toast.makeText(getApplicationContext(), "RESOLUTION_REQUIRED", Toast.LENGTH_SHORT).show();

                try {
                    status.startResolutionForResult(getActivity(), 100);

                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are not satisfied. However, we have no way to fix the
                // settings so we won't show the dialog.
                //   Toast.makeText(getApplicationContext(), "SETTINGS_CHANGE_UNAVAILABLE", Toast.LENGTH_SHORT).show();

                break;
        }
    }


    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        Log.d(TAG, "Location update stopped .......................");
    }

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }


    protected void startLocationUpdates() {
       /* if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }*/

        if (ActivityCompat.checkSelfPermission((Activity) getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) getActivity(), new String[]{
                    android.Manifest.permission.ACCESS_FINE_LOCATION
            }, 10);
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            // pendingResult.

            Log.d(TAG, "Location update started ..............: ");
        }
    }

    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
            Log.d(TAG, "Location update resumed .....................");
        }
    }


    class AddressResultReceiver extends ResultReceiver {
        Handler handler;

        public AddressResultReceiver(Handler handler) {
            super(handler);
            this.handler = handler;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            mAddressOutput = resultData.getString(AppUtils.LocationConstants.RESULT_DATA_KEY);
            String a = mAddressOutput.replace("\n", ",");
            locname = a;
            Log.e("ADDRESS", a);
            mAreaOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_AREA);
            mCityOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_CITY);
            mStateOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_STREET);
            String lat = resultData.getString(AppUtils.LocationConstants.LATITUDE);
            String lng = resultData.getString(AppUtils.LocationConstants.LONGITUDE);
            try {
                Log.e("newlat", lat);
                Log.e("newlng", lng);
            } catch (Exception e) {
                e.printStackTrace();
            }

            save_pick_up_txt.setText(a + "");
            save_pick_up_lng.setText(lng + "");
            save_pick_up_lat.setText(lat + "");
            pick_up_txt.setText(a);

            if (resultCode == AppUtils.LocationConstants.SUCCESS_RESULT) {
            }


        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        try {

            TouchableMapFragment mapFragment = (TouchableMapFragment) this.getChildFragmentManager()
                    .findFragmentById(R.id.map2);
            FragmentTransaction ft = getActivity().getSupportFragmentManager()
                    .beginTransaction();
            ft.remove(mapFragment);
            ft.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {


        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Excep downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectParser parser = new DirectParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {


            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            if (result.size() < 1) {
                Toast.makeText(getActivity(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {    // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duration = (String) point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.RED);
            }

            //   tvDistanceDuration.setText("Distance:"+distance + ", Duration:"+duration);
            Log.e("print_distance", "Distance:" + distance + ", Duration:" + duration);

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);

            MarkerOptions options = new MarkerOptions();
            options.position(pickup_lat_lng);
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
            mMap.addMarker(options);
            StopCameraListener();
            options = new MarkerOptions();
            options.position(drop_lat_lng);
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            mMap.addMarker(options);

        }


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //    setRetainInstance(true);
        rootView = inflater.inflate(R.layout.dashbord_with_service_fragment, container, false);

        this.savedInstanceState = savedInstanceState;
        init(rootView);

        mapFragment = (TouchableMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map2);
        mapFragment.getMapAsync(this);

        next = (ImageView) rootView.findViewById(R.id.next);


        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        buildLocationSettingsRequest();
        checkLocationSettings();


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(pick_up_txt.getText().toString())) {
                    pick_up_txt.setError("Please select Pick up location");

                } else if (TextUtils.isEmpty(drop_txt.getText().toString())) {
                    drop_txt.setError("Please select Drop location");
                } else {

                    Intent i = new Intent(getActivity(), SelectCab.class);
                    i.putExtra("PICK_ADD", pick_up_txt.getText().toString());
                    i.putExtra("DROP_ADD", drop_txt.getText().toString());

                    PICK = new LatLng(Double.parseDouble(save_pick_up_lat.getText().toString()),
                            Double.parseDouble(save_pick_up_lng.getText().toString()));

                    DROP = new LatLng(Double.parseDouble(save_drop_lat.getText().toString()),
                            Double.parseDouble(save_drop_lng.getText().toString()));

                    i.putExtra("PICK_LOC", PICK);
                    i.putExtra("DROP_LOC", DROP);

                    Location start = new Location("loc1");
                    Location end = new Location("loc2");

                    start.setLatitude(PICK.latitude);
                    start.setLongitude(PICK.longitude);

                    end.setLatitude(DROP.latitude);
                    end.setLongitude(DROP.longitude);


                    i.putExtra("DIST", DIST);
                    i.putExtra("DURATION", DURATION);
                    i.putExtra("from", "1");
                    startActivity(i);

                    getActivity().finish();

                }

            }
        });


        drop_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocFromMapFragment dropLocationFragment1 = new LocFromMapFragment();

                Bundle b = new Bundle();
                b.putString("from", "1");
                b.putString("drop_pick", "2");
                dropLocationFragment1.setArguments(b);

                Bundle b2 = new Bundle();
                b2.putString("pickup_txt", save_pick_up_txt.getText().toString());
                b2.putString("pickup_lat", save_pick_up_lat.getText().toString());
                b2.putString("pickup_lng", save_pick_up_lng.getText().toString());


                dropLocationFragment1.setPickUpLocationData(b2);
                FragmentManager ridefragmentManagerg = getActivity().getSupportFragmentManager();
                ridefragmentManagerg.beginTransaction()
                        .replace(R.id.activity_check_ace_cabs, dropLocationFragment1)
                        //    .addToBackStack("drop_location_fragment1")
                        .commit();
                map_toolbar.setVisibility(View.GONE);
            }
        });
        drop_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LocFromMapFragment dropLocationFragment = new LocFromMapFragment();

                Bundle b = new Bundle();
                b.putString("from", "1");
                b.putString("drop_pick", "2");
                dropLocationFragment.setArguments(b);

                Bundle b2 = new Bundle();
                b2.putString("pickup_txt", save_pick_up_txt.getText().toString());
                b2.putString("pickup_lat", save_pick_up_lat.getText().toString());
                b2.putString("pickup_lng", save_pick_up_lng.getText().toString());


                dropLocationFragment.setPickUpLocationData(b2);

                FragmentManager ridefragmentManagerg = getActivity().getSupportFragmentManager();
                ridefragmentManagerg.beginTransaction()
                        .replace(R.id.activity_check_ace_cabs, dropLocationFragment)
                        //  .addToBackStack("drop_location_fragment2")
                        .commit();

                map_toolbar.setVisibility(View.GONE);
            }
        });

        pick_up_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocFromMapFragment dropLocationFragment = new LocFromMapFragment();

                Bundle b = new Bundle();
                b.putString("from", "1");
                b.putString("drop_pick", "1");
                dropLocationFragment.setArguments(b);

                Bundle b2 = new Bundle();
                b2.putString("drop_txt", save_drop_txt.getText().toString());
                b2.putString("drop_lat", save_drop_lat.getText().toString());
                b2.putString("drop_lng", save_drop_lng.getText().toString());
                dropLocationFragment.setDropLocationData(b2);

                FragmentManager ridefragmentManagerg = getActivity().getSupportFragmentManager();
                ridefragmentManagerg.beginTransaction()
                        .replace(R.id.activity_check_ace_cabs, dropLocationFragment)
                        //  .addToBackStack("drop_location_fragment1")
                        .commit();

                map_toolbar.setVisibility(View.GONE);
            }
        });


        return rootView;
    }


    public void setProgressMsg(String title, String message) {
        pd.setTitle(title);
        pd.setMessage(message);
    }


    class AddressGeocodingTask extends AsyncTask<LatLng, Void, String> {
        Context mContext;
        double latitude, longitude;

        public AddressGeocodingTask(Context context) {
            super();
            mContext = context;
        }

        // Finding address using reverse geocoding
        @Override
        protected String doInBackground(LatLng... params) {
            Geocoder geocoder = new Geocoder(mContext);
            latitude = params[0].latitude;
            longitude = params[0].longitude;

            List<Address> addresses = null;
            String addressText = "";

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (addresses != null && addresses.size() > 0) {
               /* Address address = addresses.get(0);

                addressText = String.format("%s, %s, %s",
                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                        address.getLocality(),
                        address.getCountryName());*/


                Address address = addresses.get(0);
                ArrayList<String> addressFragments = new ArrayList<String>();

                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressFragments.add(address.getAddressLine(i));

                }

                String add1 = TextUtils.join(System.getProperty("line.separator"), addressFragments);
                addressText = add1.replace("\n", ",");
            }

            return addressText;
        }

        @Override
        protected void onPostExecute(String addressText) {
            // Setting the title for the marker.
            // This will be displayed on taping the marker

            save_pick_up_txt.setText(addressText + "");
            save_pick_up_lng.setText(longitude + "");
            save_pick_up_lat.setText(latitude + "");
            pick_up_txt.setText(addressText);

            mMap.clear();
            addressText = addressText.replace("null,", "");
            //    txtView.setText(addressText);


        }
    }


    public void fetchCityName(final Context contex, final Location location) {
        if (running)
            return;
        new AsyncTask<Void, Void, String>() {
            protected void onPreExecute() {
                running = true;
            }

            ;

            @Override
            protected String doInBackground(Void... params) {


                if (Geocoder.isPresent()) {
                    try {
                        Geocoder geocoder = new Geocoder(contex, Locale.getDefault());
                        List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                       /* if (addresses.size() > 0) {
                            cityName = addresses.get(0).getLocality();
                        }*/


                        Address address = addresses.get(0);
                        ArrayList<String> addressFragments = new ArrayList<String>();

                        for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                            addressFragments.add(address.getAddressLine(i));

                        }

                        String add1 = TextUtils.join(System.getProperty("line.separator"), addressFragments);
                        cityName = add1.replace("\n", ",");


                    } catch (Exception ignored) {
                        // after a while, Geocoder start to trhow "Service not availalbe" exception. really weird since it was working before (same device, same Android version etc..
                    }
                }

                if (cityName.equals("")) // i.e., Geocoder succeed
                {
                    cityName = fetchCityNameUsingGoogleMap();
                } else // i.e., Geocoder failed
                {
                    Log.e("CityName", cityName.toString() + "  city");
                }
                return cityName;
            }

            // Geocoder failed :-(
            // Our B Plan : Google Map
            private String fetchCityNameUsingGoogleMap() {
                String googleMapUrl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + location.getLatitude() + ","
                        + location.getLongitude() + "&sensor=false&language=en";

                try {
                    JSONObject googleMapResponse = new JSONObject(ANDROID_HTTP_CLIENT.execute(new HttpGet(googleMapUrl),
                            new BasicResponseHandler()));

                    // many nested loops.. not great -> use expression instead
                    // loop among all results
                    JSONArray results = (JSONArray) googleMapResponse.get("results");
                    for (int i = 0; i < results.length(); i++) {
                        // loop among all addresses within this result
                        JSONObject result = results.getJSONObject(i);

                        cityName = result.getString("formatted_address").toString();

                        if (cityName != null) {
                            return cityName;
                        }

                      /*  if (result.has("address_components")) {
                            JSONArray addressComponents = result.getJSONArray("address_components");
                            // loop among all address component to find a 'locality' or 'sublocality'
                            for (int j = 0; j < addressComponents.length(); j++) {
                                JSONObject addressComponent = addressComponents.getJSONObject(j);
                                if (result.has("types")) {
                                    JSONArray types = addressComponent.getJSONArray("types");

                                    // search for locality and sublocality
                                    String cityName = null;

                                    for (int k = 0; k < types.length(); k++) {
                                        if ("locality".equals(types.getString(k)) && cityName == null) {
                                            if (addressComponent.has("long_name")) {
                                                cityName = addressComponent.getString("long_name");
                                            } else if (addressComponent.has("short_name")) {
                                                cityName = addressComponent.getString("short_name");
                                            }
                                        }
                                        if ("sublocality".equals(types.getString(k))) {
                                            if (addressComponent.has("long_name")) {
                                                cityName = addressComponent.getString("long_name");
                                            } else if (addressComponent.has("short_name")) {
                                                cityName = addressComponent.getString("short_name");
                                            }
                                        }
                                    }
                                    if (cityName != null) {
                                        return cityName;
                                    }
                                }
                            }
                        }*/
                    }
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(String cityName) {
                running = false;
                if (cityName != null) {
                    // Do something with cityName
                    Log.i("GeocoderHelper", cityName);


                    save_pick_up_txt.setText(cityName + "");
                    save_pick_up_lng.setText(location.getLongitude() + "");
                    save_pick_up_lat.setText(location.getLatitude() + "");
                    pick_up_txt.setText(cityName + "");

                    mMap.clear();

                }
            }

            ;
        }.execute();
    }


}