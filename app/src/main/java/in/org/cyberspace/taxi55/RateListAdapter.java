package in.org.cyberspace.taxi55;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class RateListAdapter extends BaseAdapter {
    String[] CAB;
    String[] INITIAL;
    String[] REST;
    Activity mContex;

    public RateListAdapter(Activity context, String[] cab, String[] initial, String[] rest) {
        this.mContex = context;
        this.CAB = cab;
        this.INITIAL = initial;
        this.REST = rest;
        Log.i("Adapter CABS", this.CAB.length + BuildConfig.FLAVOR);
        Log.i("Adapter INI", this.INITIAL.length + BuildConfig.FLAVOR);
        Log.i("Adapter REST", this.REST.length + BuildConfig.FLAVOR);
    }

    public int getCount() {
        return this.CAB.length;
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View row = this.mContex.getLayoutInflater().inflate(R.layout.rate_list_item, viewGroup, false);
        TextView tvInitial = (TextView) row.findViewById(R.id.textView7);
        TextView tvRest = (TextView) row.findViewById(R.id.textView8);
        ((TextView) row.findViewById(R.id.textView6)).setText(this.CAB[i]);
        tvInitial.setText(this.INITIAL[i]);
        tvRest.setText(this.REST[i]);
        return row;
    }
}


/*
package in.org.cyberspace.taxi55;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

*/
/**
 * Created by Wasim on 13/02/2017.
 *//*


public class RateListAdapter extends BaseAdapter {

    String[] CAB,INITIAL,REST;
    Activity mContex;
    public RateListAdapter(Activity context, String[] cab, String[] initial, String[] rest)
    {
        this.mContex=context;
        this.CAB=cab;
        this.INITIAL=initial;
        this.REST=rest;

        Log.i("Adapter CABS",CAB.length+"");
        Log.i("Adapter INI",INITIAL.length+"");
        Log.i("Adapter REST",REST.length+"");
    }
    @Override
    public int getCount() {
        return CAB.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = mContex.getLayoutInflater();
        View row=layoutInflater.inflate(R.layout.rate_list_item,viewGroup,false);
        TextView tvCab,tvInitial,tvRest;
        tvCab=(TextView)row.findViewById(R.id.textView6);
        tvInitial=(TextView)row.findViewById(R.id.textView7);
        tvRest=(TextView)row.findViewById(R.id.textView8);

        tvCab.setText(CAB[i]);
        tvInitial.setText(INITIAL[i]);
        tvRest.setText(REST[i]);

        return row;
    }
}
*/
