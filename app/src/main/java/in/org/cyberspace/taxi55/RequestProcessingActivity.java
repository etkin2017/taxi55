package in.org.cyberspace.taxi55;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import in.org.cyberspace.taxi55.services.CheckDriverAssignedService;

public class RequestProcessingActivity extends AppCompatActivity {

    String booking_id;
    Button cancel_trip;
    ImageView back_arrow;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_processing);
        cancel_trip = (Button) findViewById(R.id.cancel_trip);
        booking_id = getIntent().getStringExtra("booking_id");
        back_arrow = (ImageView) findViewById(R.id.back_arrow);
        dialog = new ProgressDialog(RequestProcessingActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait...");


        Intent i = new Intent(RequestProcessingActivity.this, CheckDriverAssignedService.class);
        i.putExtra("booking_id", booking_id);
        startService(i);


        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                    startActivity(new Intent(RequestProcessingActivity.this, LocaFromMapActivity.class));
                } catch (Exception ex1) {
                    ex1.printStackTrace();
                }
            }
        });
        cancel_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dlg = new AlertDialog.Builder(RequestProcessingActivity.this);
                dlg.setTitle(R.string.app_name);
                dlg.setMessage("Are You Sure You Want To Cancel the Trip ?");
                dlg.setIcon(R.drawable.icon);
                dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //   finish();
                        dialogInterface.dismiss();
                        cancel_booking(booking_id);

                    }
                });


                dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                });

                dlg.create().show();
            }
        });
    }


    public void cancel_booking(String booking_id) {
        dialog.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("booking_id", booking_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "cancelled_booking", jsonObject, new Response.Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                Log.e("PassengerResponse", response.toString());
                try {
                    String status = response.getString("status");
                    int data = response.getInt("data");
                    if (status.equals("success") && data == 1) {
                        finish();
                        Toast.makeText(RequestProcessingActivity.this.getApplicationContext(), "Booking Cancelled", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(RequestProcessingActivity.this, MyRides.class));
                    } else {
                        //   Toast.makeText(RequestProcessingActivity.this.getApplicationContext(), "Failed To Cancel", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.printStackTrace();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }
}
