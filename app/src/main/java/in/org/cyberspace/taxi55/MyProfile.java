package in.org.cyberspace.taxi55;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MyProfile extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        showProfile();
    }

    public void showProfile() {
        TextView tvMobile;
        TextView tvEmail;
        Cursor c = new DBHelper(this).getUserData();
        if (c.getCount() >= 1) {
            c.moveToFirst();
            do {
                String username = c.getString(0);
                String email = c.getString(2);
                String mobile = c.getString(3);
                tvMobile = (TextView) findViewById(R.id.tvMobile);
                tvEmail = (TextView) findViewById(R.id.tvEmail);
                ((TextView) findViewById(R.id.tvUsername)).setText(username);
                tvMobile.setText(mobile);
                tvEmail.setText(email);
            } while (c.moveToNext());
        }
        PrefManager prefManager = new PrefManager(this);
        String unamr = prefManager.getUsername();
        String emaild = prefManager.getEmailId();
        String mobile_no = prefManager.getUserMobile();
        tvMobile = (TextView) findViewById(R.id.tvMobile);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        ((TextView) findViewById(R.id.tvUsername)).setText(unamr);
        tvMobile.setText(mobile_no);
        tvEmail.setText(emaild);
    }
}


/*
package in.org.cyberspace.taxi55;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MyProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        showProfile();
    }

    public void showProfile()
    {
        DBHelper db = new DBHelper(this);
        Cursor c = db.getUserData();
        if(c.getCount()>=1)
        {
            c.moveToFirst();
            do {
                String username=c.getString(0);
                String email=c.getString(2);
                String mobile=c.getString(3);

                TextView tvUser=(TextView)findViewById(R.id.tvUsername);
                TextView tvMobile=(TextView)findViewById(R.id.tvMobile);
                TextView tvEmail=(TextView)findViewById(R.id.tvEmail);

                tvUser.setText(username);
                tvMobile.setText(mobile);
                tvEmail.setText(email);
            }while (c.moveToNext());

        }


        PrefManager prefManager = new PrefManager(MyProfile.this);

        String unamr = prefManager.getUsername();

        String emaild =prefManager.getEmailId();

        String mobile_no =prefManager.getUserMobile();


        TextView tvUser=(TextView)findViewById(R.id.tvUsername);
        TextView tvMobile=(TextView)findViewById(R.id.tvMobile);
        TextView tvEmail=(TextView)findViewById(R.id.tvEmail);

        tvUser.setText(unamr);
        tvMobile.setText(mobile_no);
        tvEmail.setText(emaild);
    }
}
*/
