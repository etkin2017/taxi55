package in.org.cyberspace.taxi55;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import in.org.cyberspace.taxi55.db.NotiDbHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void login(View v) {
        EditText txtUser = (EditText) findViewById(R.id.input_username);
        EditText txtPass = (EditText) findViewById(R.id.input_password);
        final String username = txtUser.getText().toString().trim();
        String pass = txtPass.getText().toString().trim();
        if (username.equals(BuildConfig.FLAVOR)) {
            txtUser.setError("Enter Username !");
            txtUser.requestFocus();
        } else if (pass.equals(BuildConfig.FLAVOR)) {
            txtPass.setError("Enter Password !");
            txtPass.requestFocus();
        } else {
            final ProgressDialog pdlg = new ProgressDialog(this);
            pdlg.setMessage("Please Wait...");
            pdlg.setCancelable(false);
            pdlg.show();
            String REQUEST_TAG = getPackageName() + ".login_request";
            JSONObject jsonObject = new JSONObject();
            try {
                String refreshedToken = getSharedPreferences("fcm_token", 0).getString("fcm_token", BuildConfig.FLAVOR);
                jsonObject.put("secret_key", ConnectionConfiguration.SECRET_KEY);
                jsonObject.put("Email", username);
                jsonObject.put("Password", pass);
                jsonObject.put("fcm", refreshedToken);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(ConnectionConfiguration.URL + Event.LOGIN, jsonObject, new Listener<JSONArray>() {
                public void onResponse(JSONArray response) {
                    Log.e("Response", response.toString());

                    try {
                        JSONObject jsonObject1 = response.getJSONObject(0);
                        if (jsonObject1.getString("status").equals("success")) {
                            PrefManager prefManager = new PrefManager(Login.this);
                            prefManager.setUserName(username);
                            prefManager.setIsUserLoggedIn(true);
                            prefManager.setUSER_Id(jsonObject1.getString(NotiDbHelper.mid));
                            prefManager.setUserMobile(jsonObject1.getString("mobile"));
                            prefManager.setEmailId(jsonObject1.getString("email"));
                            Login.this.startActivity(new Intent(Login.this, LocaFromMapActivity.class));
                            Login.this.finish();
                            return;
                        }
                        Toast.makeText(Login.this, jsonObject1.getString(NotiDbHelper.msg), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        Toast.makeText(Login.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    pdlg.dismiss();
                }
            }, new ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    pdlg.dismiss();
                    error.printStackTrace();
                    Log.e("Response", error.toString());
                }
            });
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
            AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonArrayRequest, REQUEST_TAG);
        }
    }

    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") != 0) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION", "android.permission.READ_CONTACTS","android.permission.CALL_PHONE"}, R.styleable.AppCompatTheme_ratingBarStyleSmall);
        }
    }

    public void signup(View v) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.signup_layout);
        dialog.setTitle("Signup...");
        ((Button) dialog.findViewById(R.id.singup_dialog_btn)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
                EditText txtUser = (EditText) dialog.findViewById(R.id.input_username);
                EditText txtEmail = (EditText) dialog.findViewById(R.id.input_email);
                EditText txtMobile = (EditText) dialog.findViewById(R.id.input_mobile);
                EditText txtPass = (EditText) dialog.findViewById(R.id.input_password);
                EditText txtCPass = (EditText) dialog.findViewById(R.id.input_c_passworde);
                final String username = txtUser.getText().toString().trim();
                final String mobile = txtMobile.getText().toString().trim();
                final String email = txtEmail.getText().toString().trim();
                final String pass = txtPass.getText().toString().trim();
                String cPass = txtCPass.getText().toString().trim();
                if (username.equals(BuildConfig.FLAVOR)) {
                    txtUser.setError("Enter Username !");
                    txtUser.requestFocus();
                } else if (email.equals(BuildConfig.FLAVOR)) {
                    txtEmail.setError("Enter Email !");
                    txtEmail.requestFocus();
                } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    txtEmail.setError("Enter Valid Email !");
                    txtEmail.requestFocus();
                } else if (mobile.equals(BuildConfig.FLAVOR)) {
                    txtMobile.setError("Enter Mobile Number !");
                    txtMobile.requestFocus();
                } else if (pass.equals(BuildConfig.FLAVOR)) {
                    txtPass.setError("Enter Password !");
                    txtPass.requestFocus();
                } else if (cPass.equals(BuildConfig.FLAVOR)) {
                    txtCPass.setError("Confirmn Password !");
                    txtCPass.requestFocus();
                } else if (pass.equals(cPass)) {
                    final ProgressDialog pdlg = new ProgressDialog(Login.this);
                    pdlg.setMessage("Please Wait...");
                    pdlg.show();
                    pdlg.setCancelable(false);
                    String REQUEST_TAG = Login.this.getPackageName() + ".volleyJsonArrayRequest";
                    JSONObject jsonObject = new JSONObject();
                    try {
                        String refreshedToken = Login.this.getSharedPreferences("fcm_token", 0).getString("fcm_token", BuildConfig.FLAVOR);
                        Log.e("refreshedToken", refreshedToken);
                        jsonObject.put("secret_key", ConnectionConfiguration.SECRET_KEY);
                        jsonObject.put("Email", email);
                        jsonObject.put("Password", pass);
                        jsonObject.put("User_name", username);
                        jsonObject.put("Mobile", mobile);
                        jsonObject.put("fcm", refreshedToken);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + Event.SIGN_UP, jsonObject, new Listener<JSONObject>() {
                        @RequiresApi(api = 19)
                        public void onResponse(JSONObject response) {
                            Log.e("Response", response.toString());

                            try {
                                if (response.getString("status").equals("success")) {
                                    new DBHelper(Login.this).saveUserData(username, pass, email, mobile, response.getString("token"));
                                    PrefManager prefManager = new PrefManager(Login.this);
                                    prefManager.setIsUserLoggedIn(true);
                                    prefManager.setUserName(username);
                                    prefManager.setUSER_Id(response.getString("user_id"));
                                    prefManager.setUserMobile(mobile);
                                    prefManager.setEmailId(email);
                                    Login.this.startActivity(new Intent(Login.this, LocaFromMapActivity.class));
                                    Login.this.finish();

                                    return;
                                }
                                Toast.makeText(Login.this, response.getJSONArray("error_list").getJSONObject(0).getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                            pdlg.dismiss();
                        }
                    }, new ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            Log.e("Response", error.toString());
                            pdlg.dismiss();
                            Toast.makeText(Login.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                        }
                    });
                    jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
                    AppSingleton.getInstance(Login.this.getApplicationContext()).addToRequestQueue(jsonObjectRequest, REQUEST_TAG);
                } else {
                    txtCPass.setError("Confirmn Password Did Not Match !");
                    txtCPass.requestFocus();
                }
            }
        });
        dialog.show();
        dialog.getWindow().setLayout(-1, -2);
    }
}


/*
package in.org.cyberspace.taxi55;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }


    public void login(View v) {

        EditText txtUser = (EditText) findViewById(R.id.input_username);
        EditText txtPass = (EditText) findViewById(R.id.input_password);


        final String username = txtUser.getText().toString().trim();
        final String pass = txtPass.getText().toString().trim();


        if (username.equals("")) {
            txtUser.setError("Enter Username !");
            txtUser.requestFocus();
            return;
        }

        if (pass.equals("")) {
            txtPass.setError("Enter Password !");
            txtPass.requestFocus();
            return;
        }

        final ProgressDialog pdlg = new ProgressDialog(Login.this);
        pdlg.setMessage("Please Wait...");
        pdlg.setCancelable(false);
        pdlg.show();
        String REQUEST_TAG = getPackageName() + ".login_request";
        JSONObject jsonObject = new JSONObject();
        try {

            SharedPreferences sh = getSharedPreferences("fcm_token", MODE_PRIVATE);
            String refreshedToken = sh.getString("fcm_token", "");
            jsonObject.put("secret_key", ConnectionConfiguration.SECRET_KEY);
            jsonObject.put("Email", username);
            jsonObject.put("Password", pass);
            jsonObject.put("fcm", refreshedToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(ConnectionConfiguration.URL + "login", jsonObject, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e("Response", response.toString());
                pdlg.dismiss();
                try {
                    JSONObject jsonObject1 = response.getJSONObject(0);
                    if (jsonObject1.getString("status").equals("success")) {
                        PrefManager prefManager = new PrefManager(Login.this);
                        prefManager.setUserName(username);
                        prefManager.setIsUserLoggedIn(true);
                        prefManager.setUSER_Id(jsonObject1.getString("id"));
                        prefManager.setUserMobile(jsonObject1.getString("mobile"));
                        prefManager.setEmailId(jsonObject1.getString("email"));
                        startActivity(new Intent(Login.this, Home.class));
                        finish();
                    } else {
                        Toast.makeText(Login.this, jsonObject1.getString("message"), Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (JSONException e) {
                    Toast.makeText(Login.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pdlg.dismiss();
                        error.printStackTrace();
                        Log.e("Response", error.toString());
                    }
                });
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonArrayRequest, REQUEST_TAG);


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ActivityCompat.checkSelfPermission(Login.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Login.this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION,  android.Manifest.permission.READ_CONTACTS},
                    110);

        }
    }

    public void signup(View v) {
        final Dialog dialog = new Dialog(Login.this);
        dialog.setContentView(R.layout.signup_layout);

        dialog.setTitle("Signup...");

        // set the custom dialog components - text, image and button

        Button dialogButton = (Button) dialog.findViewById(R.id.singup_dialog_btn);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //startActivity(new Intent(Login.this,Home.class));
                //finish();
                EditText txtUser = (EditText) dialog.findViewById(R.id.input_username);
                EditText txtEmail = (EditText) dialog.findViewById(R.id.input_email);
                EditText txtMobile = (EditText) dialog.findViewById(R.id.input_mobile);
                EditText txtPass = (EditText) dialog.findViewById(R.id.input_password);
                EditText txtCPass = (EditText) dialog.findViewById(R.id.input_c_passworde);


                final String username = txtUser.getText().toString().trim();
                final String mobile = txtMobile.getText().toString().trim();
                final String email = txtEmail.getText().toString().trim();
                final String pass = txtPass.getText().toString().trim();
                String cPass = txtCPass.getText().toString().trim();


                if (username.equals("")) {
                    txtUser.setError("Enter Username !");
                    txtUser.requestFocus();
                    return;
                }

                if (email.equals("")) {
                    txtEmail.setError("Enter Email !");
                    txtEmail.requestFocus();
                    return;
                }

                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    txtEmail.setError("Enter Valid Email !");
                    txtEmail.requestFocus();
                    return;
                }

                if (mobile.equals("")) {
                    txtMobile.setError("Enter Mobile Number !");
                    txtMobile.requestFocus();
                    return;
                }

                if (pass.equals("")) {
                    txtPass.setError("Enter Password !");
                    txtPass.requestFocus();
                    return;
                }

                if (cPass.equals("")) {
                    txtCPass.setError("Confirmn Password !");
                    txtCPass.requestFocus();
                    return;
                }

                if (!pass.equals(cPass)) {
                    txtCPass.setError("Confirmn Password Did Not Match !");
                    txtCPass.requestFocus();
                    return;
                }
                final ProgressDialog pdlg = new ProgressDialog(Login.this);
                pdlg.setMessage("Please Wait...");
                pdlg.show();
                pdlg.setCancelable(false);
                String REQUEST_TAG = getPackageName() + ".volleyJsonArrayRequest";
                JSONObject jsonObject = new JSONObject();
                try {
                    SharedPreferences sh = getSharedPreferences("fcm_token", MODE_PRIVATE);
                    String refreshedToken = sh.getString("fcm_token", "");
                    Log.e("refreshedToken", refreshedToken);
                    jsonObject.put("secret_key", ConnectionConfiguration.SECRET_KEY);
                    jsonObject.put("Email", email);
                    jsonObject.put("Password", pass);
                    jsonObject.put("User_name", username);
                    jsonObject.put("Mobile", mobile);
                    jsonObject.put("fcm", refreshedToken);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "sign_up", jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Response", response.toString());
                        pdlg.dismiss();
                        try {
                            if (response.getString("status").equals("success")) {
                                DBHelper db = new DBHelper(Login.this);
                                db.saveUserData(username, pass, email, mobile, response.getString("token"));
                                PrefManager prefManager = new PrefManager(Login.this);
                                prefManager.setIsUserLoggedIn(true);
                                prefManager.setUserName(username);
                                prefManager.setUSER_Id(response.getString("user_id"));
                                startActivity(new Intent(Login.this, Home.class));
                                finish();
                            } else {
                                Toast.makeText(Login.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                Log.e("Response", error.toString());
                                pdlg.dismiss();
                                Toast.makeText(Login.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                            }
                        });
                jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                        5000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, REQUEST_TAG);
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}
*/
