package in.org.cyberspace.taxi55;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import in.org.cyberspace.taxi55.db.NotiDbHelper;

public class RideAdapter extends BaseAdapter {
    String[] Amt;
    String[] Dates;
    String[] Fcm;
    String[] ID;
    String[] NAMES;
    String[] Phone;
    String[] Status;
    String[] Times;
    String[] ASSIGNED_FOR;
    Activity mContex;

    public RideAdapter(Activity context, String[] id, String[] dates, String[] times, String[] amt, String[] status, String[] fcm, String[] phone, String[] NAMES, String[] ASSIGNED_FOR) {
        this.mContex = context;
        this.ID = id;
        this.Dates = dates;
        this.Times = times;
        this.Amt = amt;
        this.Status = status;
        this.Fcm = fcm;
        this.Phone = phone;
        this.NAMES = NAMES;
        this.ASSIGNED_FOR = ASSIGNED_FOR;
    }

    public RideAdapter(Activity context, String[] id, String[] dates, String[] times, String[] amt, String[] status) {
        this.mContex = context;
        this.ID = id;
        this.Dates = dates;
        this.Times = times;
        this.Amt = amt;
        this.Status = status;
    }

    public int getCount() {
        return this.ID.length;
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View row = this.mContex.getLayoutInflater().inflate(R.layout.booking_list, viewGroup, false);
        TextView tvDate = (TextView) row.findViewById(R.id.bookDate);
        TextView tvTime = (TextView) row.findViewById(R.id.bookTime);
        TextView tvAmt = (TextView) row.findViewById(R.id.bookAmt);
        final ImageView menu_my_ride = (ImageView) row.findViewById(R.id.menu_my_ride);
        final TextView tvStatus = (TextView) row.findViewById(R.id.bookStatus);
        LinearLayout bok_list = (LinearLayout) row.findViewById(R.id.bok_list);
        final TextView name = (TextView) row.findViewById(R.id.name);
        final TextView fcm = (TextView) row.findViewById(R.id.fcm);
        final TextView phone = (TextView) row.findViewById(R.id.phone);
        final TextView assigned_for = (TextView) row.findViewById(R.id.assigned_for);
        final TextView bookID = ((TextView) row.findViewById(R.id.bookID));
        bookID.setText(this.ID[i]);
        tvDate.setText(this.Dates[i]);
        tvTime.setText(this.Times[i]);
        tvAmt.setText(this.Amt[i]);
        tvStatus.setText(this.Status[i]);
        menu_my_ride.setColorFilter(mContex.getResources().getColor(R.color.sky_blue));
        try {
            fcm.setText(this.Fcm[i]);
            phone.setText(this.Phone[i]);
            name.setText(this.NAMES[i]);
            assigned_for.setText(this.ASSIGNED_FOR[i]);
        } catch (Exception ee) {
            ee.printStackTrace();
        }
        if (this.Status[i].contains("Complete")) {
            tvStatus.setTextColor(Color.RED);
            menu_my_ride.setVisibility(View.GONE);
        } else {

            menu_my_ride.setVisibility(View.VISIBLE);
        }


        if (this.Status[i].contains("Processing")) {
            tvStatus.setTextColor(Color.GREEN);
        }

        if (this.Status[i].contains("Booking")) {
            tvStatus.setTextColor(Color.YELLOW);
        }
        if (this.Status[i].contains("Cancelled")) {
            tvStatus.setTextColor(mContex.getResources().getColor(R.color.colorAccent));
        }

        menu_my_ride.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popup = new PopupMenu(mContex, menu_my_ride);
                popup.getMenuInflater().inflate(R.menu.chat_n_track, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id1 = item.getItemId();
                        String status = tvStatus.getText().toString().toLowerCase();
                        if (id1 == R.id.new_chat) {
                            if (!TextUtils.isEmpty(fcm.getText().toString()) && status.contains("processing")) {
                                String mobile = new PrefManager(RideAdapter.this.mContex).getUserMobile();
                                Intent intent = new Intent(RideAdapter.this.mContex, NewChatActivity.class);
                                intent.putExtra(NotiDbHelper.sid, mobile);
                                intent.putExtra(NotiDbHelper.rid, phone.getText().toString());
                                intent.putExtra("fcm", fcm.getText().toString());
                                intent.putExtra("uname", name.getText().toString());
                                RideAdapter.this.mContex.startActivity(intent);
                            }
                        }

                        if (id1 == R.id.track_driver) {
                            String ass = assigned_for.getText().toString().replace("@", " ");
                            if (!TextUtils.isEmpty(ass) && status.contains("processing")) {
                               /* Intent mapAct = new Intent(mContex, DriverTrackingActivity.class);
                                mapAct.putExtra("driver_id", ass);
                                mContex.startActivity(mapAct);*/

                                go_to_map(bookID.getText().toString(), fcm.getText().toString(), name.getText().toString(), phone.getText().toString());
                            } else if (!status.contains("cancelled")) {

                                Intent intent1 = new Intent(mContex, RequestProcessingActivity.class);
                                intent1.putExtra("booking_id", bookID.getText().toString().replace("Booking ID :", "").trim());
                                mContex.startActivity(intent1);

                            }
                        }

                        return true;
                    }
                });

                popup.show();
            }
        });

        bok_list.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String status = tvStatus.getText().toString().toLowerCase();
                if (!TextUtils.isEmpty(fcm.getText().toString()) && status.contains("processing")) {
                    String mobile = new PrefManager(RideAdapter.this.mContex).getUserMobile();
                    Intent intent = new Intent(RideAdapter.this.mContex, NewChatActivity.class);
                    intent.putExtra(NotiDbHelper.sid, mobile);
                    intent.putExtra(NotiDbHelper.rid, phone.getText().toString());
                    intent.putExtra("fcm", fcm.getText().toString());
                    intent.putExtra("uname", name.getText().toString());
                    RideAdapter.this.mContex.startActivity(intent);
                }
            }
        });
        return row;
    }


    public void go_to_map(final String booking_id, final String fcm, final String uname, final String phoneNm) {
        final ProgressDialog dialog = new ProgressDialog(mContex);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait...");
        dialog.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("booking_id", booking_id.replace("Booking ID :", "").trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "check_booking_assigned_for", jsonObject, new Response.Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                Log.e("PassengerResponse", response.toString());
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    Intent mapAct = new Intent(mContex, DriverTrackingActivity.class);
                    String driver_id = jsonArray.getJSONObject(0).getString("assigned_for");

                    if (driver_id.equals("")) {
                        Toast.makeText(mContex, "Driver Not Assigned To this booking", Toast.LENGTH_LONG).show();
                        return;
                    } else {
                        String mobile = new PrefManager(RideAdapter.this.mContex).getUserMobile();

                        mapAct.putExtra("driver_id", driver_id);
                        mapAct.putExtra("driver_nm", jsonArray.getJSONObject(0).getString("name"));
                        mapAct.putExtra("user_name", jsonArray.getJSONObject(0).getString("user_name"));
                        mapAct.putExtra("phone", jsonArray.getJSONObject(0).getString("phone"));
                        mapAct.putExtra("email", jsonArray.getJSONObject(0).getString("email"));
                        mapAct.putExtra("booking_id", booking_id);
                        mapAct.putExtra(NotiDbHelper.sid, mobile);
                        mapAct.putExtra(NotiDbHelper.rid, phoneNm);
                        mapAct.putExtra("fcm", fcm);
                        mapAct.putExtra("uname", uname);
                        mContex.startActivity(mapAct);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Intent intent1 = new Intent(mContex, RequestProcessingActivity.class);
                    intent1.putExtra("booking_id", booking_id);
                    mContex.startActivity(intent1);


                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.printStackTrace();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(mContex).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }


}


/*
package in.org.cyberspace.taxi55;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.sql.Time;
import java.util.HashMap;

*/
/**
 * Created by Wasim on 13/02/2017.
 *//*


public class RideAdapter extends BaseAdapter {

    String[] ID, Dates, Times, Amt, Status, Fcm, Phone, NAMES;
    Activity mContex;

    public RideAdapter(Activity context, String[] id, String[] dates, String[] times, String[] amt, String[] status, String[] fcm, String[] phone, String[] NAMES) {
        this.mContex = context;
        this.ID = id;
        this.Dates = dates;
        this.Times = times;
        this.Amt = amt;
        this.Status = status;
        this.Fcm = fcm;
        this.Phone = phone;
        this.NAMES = NAMES;
    }


    public RideAdapter(Activity context, String[] id, String[] dates, String[] times, String[] amt, String[] status) {
        this.mContex = context;
        this.ID = id;
        this.Dates = dates;
        this.Times = times;
        this.Amt = amt;
        this.Status = status;

    }

    @Override
    public int getCount() {
        return ID.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = mContex.getLayoutInflater();
        View row = layoutInflater.inflate(R.layout.booking_list, viewGroup, false);
        final TextView tvID, tvDate, tvTime, tvAmt, tvStatus, fcm, phone, name;
        LinearLayout bok_list;

        tvID = (TextView) row.findViewById(R.id.bookID);
        tvDate = (TextView) row.findViewById(R.id.bookDate);
        tvTime = (TextView) row.findViewById(R.id.bookTime);
        tvAmt = (TextView) row.findViewById(R.id.bookAmt);
        tvStatus = (TextView) row.findViewById(R.id.bookStatus);
        bok_list = (LinearLayout) row.findViewById(R.id.bok_list);
        name = (TextView) row.findViewById(R.id.name);

        fcm = (TextView) row.findViewById(R.id.fcm);
        phone = (TextView) row.findViewById(R.id.phone);


        tvID.setText(ID[i]);
        tvDate.setText(Dates[i]);
        tvTime.setText(Times[i]);
        tvAmt.setText(Amt[i]);
        tvStatus.setText(Status[i]);

        try {
            fcm.setText(Fcm[i]);
            phone.setText(Phone[i]);
            name.setText(NAMES[i]);
        } catch (Exception ee) {
            ee.printStackTrace();
        }


        bok_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String status = tvStatus.getText().toString().toLowerCase();

                if (!TextUtils.isEmpty(fcm.getText().toString())) {
                    if (status.contains("processing")) {
                        PrefManager prefManager = new PrefManager(mContex);
                        String mobile = prefManager.getUserMobile();

                        Intent intent = new Intent(mContex, NewChatActivity.class);
                        intent.putExtra("sid", mobile);
                        intent.putExtra("rid", phone.getText().toString());
                        intent.putExtra("fcm", fcm.getText().toString());
                        intent.putExtra("uname", name.getText().toString());
                        mContex.startActivity(intent);
                    }
                }
            }
        });

        return row;
    }
}
*/
