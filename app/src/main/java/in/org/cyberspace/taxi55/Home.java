package in.org.cyberspace.taxi55;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds.Builder;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import in.org.cyberspace.taxi55.application.AppUtils.Constants;
import in.org.cyberspace.taxi55.application.AppUtils.LocationConstants;
import in.org.cyberspace.taxi55.services.MyService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import org.json.JSONObject;

public class Home extends AppCompatActivity implements OnItemClickListener, OnMapReadyCallback {
    private static final float BLUR_RADIUS = 25.0f;
    String DIST = BuildConfig.FLAVOR;
    LatLng DROP = null;
    String DURATION = BuildConfig.FLAVOR;
    LatLng PICK = null;
    Location currentLocation;
    String dropAddress = BuildConfig.FLAVOR;
    private GoogleMap mMap;
    ImageView map;
    ImageView map_Drop;
    ImageView map_pick;
    String[] meniListArray = new String[]{"Book Taxi", "My Rides", "Rate Card", "Profile", "Emergency Contact", "Chat", "Share", "Rate Us", "Logout", "Exit"};
    TextView menu_profile_emailid;
    TextView menu_profile_name;
    ListView menudrawer_list;
    LocationManager mlocManager;
    String pickAddress = BuildConfig.FLAVOR;

    private class DownloadTask extends AsyncTask<String, Void, String> {
        private DownloadTask() {
        }

        protected String doInBackground(String... url) {
            String data = BuildConfig.FLAVOR;
            try {
                data = Home.this.downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(new String[]{result});
        }
    }

    class LeftMenuItemAdapter extends BaseAdapter {
        ArrayList<Integer> itemsimg = new ArrayList();
        private LayoutInflater mInflater;
        private List<String> originalData = null;

        class ViewHolder {
            ImageView ic;
            TextView menu_Name;

            ViewHolder() {
            }
        }

        public LeftMenuItemAdapter(Context context, List<String> delarnames) {
            this.originalData = delarnames;
            this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
            this.itemsimg.add(Integer.valueOf(R.drawable.booking));
            this.itemsimg.add(Integer.valueOf(R.drawable.riding_car));
            this.itemsimg.add(Integer.valueOf(R.drawable.rate));
            this.itemsimg.add(Integer.valueOf(R.drawable.profile));
            this.itemsimg.add(Integer.valueOf(R.drawable.profile));
            this.itemsimg.add(Integer.valueOf(R.drawable.chatt));
            this.itemsimg.add(Integer.valueOf(R.drawable.share));
            this.itemsimg.add(Integer.valueOf(R.drawable.rate_us));
            this.itemsimg.add(Integer.valueOf(R.drawable.logout));
            this.itemsimg.add(Integer.valueOf(R.drawable.exit));
        }

        public int getCount() {
            return this.originalData.size();
        }

        public Object getItem(int position) {
            return this.originalData.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.drawer_menu_item, null);
                holder = new ViewHolder();
                holder.ic = (ImageView) convertView.findViewById(R.id.imageView);
                holder.menu_Name = (TextView) convertView.findViewById(R.id.menu_name_tv);
                convertView.setTag(holder);
                holder.menu_Name.setTag(R.id.menu_name_tv, Integer.valueOf(position));
                holder.ic.setTag(R.id.imageView, Integer.valueOf(position));
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.menu_Name.setText((CharSequence) this.originalData.get(position));
            holder.ic.setImageResource(((Integer) this.itemsimg.get(position)).intValue());
            holder.ic.setColorFilter(-1);
            return convertView;
        }
    }

    public class MyLocationListener implements LocationListener {
        public void onLocationChanged(Location loc) {
            loc.getLatitude();
            loc.getLongitude();
            String Text = "My current location is: Latitud = " + loc.getLatitude() + "Longitud = " + loc.getLongitude();
            Home.this.currentLocation = loc;
            Home.this.drawRoaute();
        }

        public void onProviderDisabled(String provider) {
            Toast.makeText(Home.this.getApplicationContext(), "GPS Disabled", 0).show();
        }

        public void onProviderEnabled(String provider) {
            Toast.makeText(Home.this.getApplicationContext(), "GPS Enabled", 0).show();
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        private ParserTask() {
        }

        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            List<List<HashMap<String, String>>> routes = null;
            try {
                routes = new DirectionsJSONParser().parse(new JSONObject(jsonData[0]));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = BuildConfig.FLAVOR;
            String duration = BuildConfig.FLAVOR;
            if (result.size() < 1) {
                Toast.makeText(Home.this.getBaseContext(), "No Points", 0).show();
                return;
            }
            for (int i = 0; i < result.size(); i++) {
                ArrayList<LatLng> points = new ArrayList();
                lineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = (List) result.get(i);
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = (HashMap) path.get(j);
                    if (j == 0) {
                        distance = (String) point.get("distance");
                    } else if (j == 1) {
                        duration = (String) point.get("duration");
                    } else {
                        points.add(new LatLng(Double.parseDouble((String) point.get("lat")), Double.parseDouble((String) point.get("lng"))));
                    }
                }
                lineOptions.addAll(points);
                lineOptions.width(5.0f);
                lineOptions.color(Color.parseColor("#399ed7"));
            }
            Home.this.DIST = distance;
            Home.this.DURATION = duration;
            Home.this.mMap.addPolyline(lineOptions);
        }
    }

    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") != 0) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION", "android.permission.READ_CONTACTS"}, R.styleable.AppCompatTheme_ratingBarStyleSmall);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        switch (position) {
            case Constants.SUCCESS_RESULT /*0*/:
                finish();
                startActivity(new Intent(this, LocaFromMapActivity.class));
                return;
            case LocationConstants.FAILURE_RESULT /*1*/:
                startActivity(new Intent(this, MyRides.class));
                drawer.closeDrawer(8388611);
                return;
            case Constants.USE_ADDRESS_LOCATION /*2*/:
                startActivity(new Intent(this, RateCard.class));
                drawer.closeDrawer(8388611);
                return;
            case R.styleable.View_paddingEnd /*3*/:
                drawer.closeDrawer(8388611);
                startActivity(new Intent(this, MyProfile.class));
                return;
            case R.styleable.View_theme /*4*/:
                drawer.closeDrawer(8388611);
                startActivity(new Intent(this, Emergency_Contact.class));
                return;
            case R.styleable.Toolbar_contentInsetStart /*5*/:
                drawer.closeDrawer(8388611);
                startActivity(new Intent(this, ChatActivity.class));
                return;
            case R.styleable.Toolbar_contentInsetEnd /*6*/:
                drawer.closeDrawer(8388611);
                Intent sharingIntent = new Intent("android.intent.action.SEND");
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra("android.intent.extra.TEXT", "Hi , I am using Taxi-55 Android App, this is very useful and easy application for booking taxi from anywhere. Please download it from following link  http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName());
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
                return;
            case R.styleable.Toolbar_contentInsetLeft /*7*/:
                drawer.closeDrawer(8388611);
                Intent goToMarket = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + getApplicationContext().getPackageName()));
                goToMarket.addFlags(1208483840);
                try {
                    startActivity(goToMarket);
                    return;
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                    return;
                }
            case R.styleable.Toolbar_contentInsetRight /*8*/:
                drawer.closeDrawer(8388611);
                logout();
                return;
            case R.styleable.Toolbar_contentInsetStartWithNavigation /*9*/:
                drawer.closeDrawer(8388611);
                exit();
                return;
            default:
                return;
        }
    }

    public Bitmap blur(Bitmap image) {
        if (image == null) {
            return null;
        }
        Bitmap outputBitmap = Bitmap.createBitmap(image);
        RenderScript renderScript = RenderScript.create(this);
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }

    public Location getLocation() {
        LocationManager mLocationManager = (LocationManager) getApplicationContext().getSystemService(Param.LOCATION);
        Location bestLocation = null;
        for (String provider : mLocationManager.getProviders(true)) {
            if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0 || ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l != null && (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy())) {
                    bestLocation = l;
                }
            } else if (!ActivityCompat.shouldShowRequestPermissionRationale(this, "android.permission.ACCESS_FINE_LOCATION")) {
                ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 2);
            }
        }
        return bestLocation;
    }

    public void initPlaceSearch() {
        PlaceAutocompleteFragment autocompleteFragment_pick = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_pick);
        ((EditText) autocompleteFragment_pick.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16.0f);
        ((EditText) autocompleteFragment_pick.getView().findViewById(R.id.place_autocomplete_search_input)).setTextColor(-1);
        ((EditText) autocompleteFragment_pick.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Select Pickup Location");
        autocompleteFragment_pick.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            public void onPlaceSelected(Place place) {
                Home.this.PICK = place.getLatLng();
                Log.i("Place", "Place: " + place.getName());
                Home.this.pickAddress = place.getName() + " , " + place.getAddress();
            }

            public void onError(Status status) {
                Log.i("Error", "An error occurred: " + status);
                Toast.makeText(Home.this, "An error occurred: " + status, 1).show();
            }
        });
        PlaceAutocompleteFragment autocompleteFragment_drop = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_drop);
        ((EditText) autocompleteFragment_drop.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16.0f);
        ((EditText) autocompleteFragment_drop.getView().findViewById(R.id.place_autocomplete_search_input)).setTextColor(-1);
        ((EditText) autocompleteFragment_drop.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Select Drop Location");
        autocompleteFragment_drop.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            public void onPlaceSelected(Place place) {
                Home.this.DROP = place.getLatLng();
                Log.i("Place", "Place: " + place.getName());
                Home.this.dropAddress = place.getName() + " , " + place.getAddress();
                if (Home.this.PICK != null && Home.this.DROP != null) {
                    Home.this.drawRoaute();
                }
            }

            public void onError(Status status) {
                Log.i("Error", "An error occurred: " + status);
                Toast.makeText(Home.this, "An error occurred: " + status, 1).show();
            }
        });
    }

    public void drawRoaute() {
        try {
            this.mMap.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mMap.setMapType(1);
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.start);
        if (this.currentLocation != null) {
            this.mMap.addMarker(new MarkerOptions().position(new LatLng(this.currentLocation.getLatitude(), this.currentLocation.getLongitude())).title("Your Current Location").icon(icon)).setDraggable(false);
        }
        if (this.PICK != null && this.DROP != null) {
            String url = getDirectionsUrl(this.PICK, this.DROP);
            new DownloadTask().execute(new String[]{url});
            this.mMap.addMarker(new MarkerOptions().position(this.PICK).title("Pickup Location").icon(BitmapDescriptorFactory.defaultMarker(120.0f)).draggable(true));
            this.mMap.addMarker(new MarkerOptions().position(this.DROP).title("Drop Location").icon(BitmapDescriptorFactory.defaultMarker(30.0f)).draggable(true));
            Builder boundsBuilder = new Builder();
            boundsBuilder.include(this.PICK);
            boundsBuilder.include(this.DROP);
            this.mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 3));
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.map = (ImageView) findViewById(R.id.map);
        this.map.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Home.this.finish();
                Home.this.startActivity(new Intent(Home.this, LocaFromMapActivity.class));
            }
        });
        startService(new Intent(this, MyService.class));
        this.currentLocation = getLocation();
        this.map_Drop = (ImageView) findViewById(R.id.map_drop);
        this.map_pick = (ImageView) findViewById(R.id.map_pick);
        this.mlocManager = (LocationManager) getSystemService(Param.LOCATION);
        this.menudrawer_list = (ListView) findViewById(R.id.drawer_list);
        this.menu_profile_emailid = (TextView) findViewById(R.id.menu_profile_emailid);
        this.menu_profile_name = (TextView) findViewById(R.id.menu_profile_name);
        try {
            PrefManager prefManager = new PrefManager(getApplicationContext());
            String nm = prefManager.getUsername();
            String unm = prefManager.getEmailId();
            this.menu_profile_name.setText(nm);
            this.menu_profile_emailid.setText(unm);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        ((LinearLayout) findViewById(R.id.drawer_lin_lay)).setBackgroundDrawable(new BitmapDrawable(blur(BitmapFactory.decodeResource(getResources(), R.drawable.road))));
        this.menudrawer_list.setAdapter(new LeftMenuItemAdapter(this, Arrays.asList(this.meniListArray)));
        this.menudrawer_list.setOnItemClickListener(this);
        ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        ((FloatingActionButton) findViewById(R.id.fab)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (Home.this.pickAddress.equals(BuildConfig.FLAVOR)) {
                    Toast.makeText(Home.this, "Select Pickup Address", 0).show();
                } else if (Home.this.dropAddress.equals(BuildConfig.FLAVOR)) {
                    Toast.makeText(Home.this, "Select Drop Address", 0).show();
                } else {
                    Intent i = new Intent(Home.this, SelectCab.class);
                    i.putExtra("PICK_ADD", Home.this.pickAddress);
                    i.putExtra("DROP_ADD", Home.this.dropAddress);
                    i.putExtra("PICK_LOC", Home.this.PICK);
                    i.putExtra("DROP_LOC", Home.this.DROP);
                    Location start = new Location("loc1");
                    Location end = new Location("loc2");
                    start.setLatitude(Home.this.PICK.latitude);
                    start.setLongitude(Home.this.PICK.longitude);
                    end.setLatitude(Home.this.DROP.latitude);
                    end.setLongitude(Home.this.DROP.longitude);
                    i.putExtra("DIST", Home.this.DIST);
                    i.putExtra("DURATION", Home.this.DURATION);
                    i.putExtra("from", "2");
                    Home.this.startActivity(i);
                }
            }
        });
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(8388611)) {
            drawer.closeDrawer(8388611);
        } else {
            exit();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        this.mMap.getUiSettings().setMapToolbarEnabled(false);
        this.mMap.getUiSettings().setZoomControlsEnabled(false);
        this.mMap.setMapType(1);
        this.currentLocation = getLocation();
        LocationListener mlocListener = new MyLocationListener();
        if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0 || ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            this.mlocManager.requestLocationUpdates("gps", 0, 0.0f, mlocListener);
        } else if (!ActivityCompat.shouldShowRequestPermissionRationale(this, "android.permission.ACCESS_FINE_LOCATION")) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 2);
        }
        initPlaceSearch();
        if (!(this.currentLocation == null || this.dropAddress == null)) {
            drawRoaute();
            this.mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(this.currentLocation.getLatitude(), this.currentLocation.getLongitude()), 15.0f));
        }
        this.mMap.setOnMarkerDragListener(new OnMarkerDragListener() {
            public void onMarkerDragStart(Marker marker) {
            }

            public void onMarkerDrag(Marker marker) {
            }

            public void onMarkerDragEnd(Marker marker) {
                Location loc = new Location("gps");
                loc.setLatitude(marker.getPosition().latitude);
                loc.setLongitude(marker.getPosition().longitude);
                if (marker.getTitle().equals("Pickup Location")) {
                    Home.this.PICK = marker.getPosition();
                } else if (marker.getTitle().equals("Drop Location")) {
                    Home.this.DROP = marker.getPosition();
                }
                Home.this.drawRoaute();
            }
        });
        this.mMap.setOnMarkerClickListener(new OnMarkerClickListener() {
            public boolean onMarkerClick(Marker marker) {
                try {
                    Address obj = (Address) new Geocoder(Home.this, Locale.getDefault()).getFromLocation(marker.getPosition().latitude, marker.getPosition().longitude, 1).get(0);
                    Log.e("IGA", "Address" + (((((((obj.getAddressLine(0) + "\n" + obj.getCountryName()) + "\n" + obj.getCountryCode()) + "\n" + obj.getAdminArea()) + "\n" + obj.getPostalCode()) + "\n" + obj.getSubAdminArea()) + "\n" + obj.getLocality()) + "\n" + obj.getSubThoroughfare()));
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(Home.this, e.getMessage(), 0).show();
                }
                return false;
            }
        });
    }

    public void logout() {
        AlertDialog.Builder dlg = new AlertDialog.Builder(this);
        dlg.setTitle(R.string.app_name);
        dlg.setMessage("Are You Sure You Want To Logout ?");
        dlg.setIcon(R.drawable.icon);
        dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                PrefManager prefManager = new PrefManager(Home.this);
                prefManager.setUserName(BuildConfig.FLAVOR);
                prefManager.setUSER_Id(BuildConfig.FLAVOR);
                prefManager.setIsUserLoggedIn(false);
                Home.this.finish();
            }
        });
        dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        dlg.create().show();
    }

    public void exit() {
        AlertDialog.Builder dlg = new AlertDialog.Builder(this);
        dlg.setTitle(R.string.app_name);
        dlg.setMessage("Are You Sure You Want To Exit ?");
        dlg.setIcon(R.drawable.icon);
        dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Home.this.finish();
            }
        });
        dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        dlg.create().show();
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = BuildConfig.FLAVOR;
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) new URL(strUrl).openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String str = BuildConfig.FLAVOR;
            while (true) {
                str = br.readLine();
                if (str == null) {
                    break;
                }
                sb.append(str);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Error", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        return "https://maps.googleapis.com/maps/api/directions/" + "json" + "?" + (str_origin + "&" + ("destination=" + dest.latitude + "," + dest.longitude) + "&" + "sensor=false");
    }
}


/*
package in.org.cyberspace.taxi55;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import android.location.LocationListener;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class Home extends AppCompatActivity implements OnMapReadyCallback, AdapterView.OnItemClickListener {
    private GoogleMap mMap;
    Location currentLocation;
    LatLng PICK = null, DROP = null;
    String pickAddress = "", dropAddress = "", DIST = "", DURATION = "";
    ImageView map_pick, map_Drop, map;
    LocationManager mlocManager;

    String[] meniListArray = {"Book Taxi", "My Rides", "Rate Card", "Profile", "Emergency Contact", "Chat",
            "Share", "Rate Us", "Logout", "Exit"};
    ListView menudrawer_list;
    TextView menu_profile_emailid, menu_profile_name;

    @Override
    protected void onResume() {
        super.onResume();

        if (ActivityCompat.checkSelfPermission(Home.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Home.this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.READ_CONTACTS},
                    110);

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        switch (position) {
            case 0:
                finish();
                startActivity(new Intent(Home.this, LocaFromMapActivity.class));
                break;
            case 1:
                startActivity(new Intent(this, MyRides.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 2:
                startActivity(new Intent(this, RateCard.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 3:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, MyProfile.class));
                break;
            case 4:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, Emergency_Contact.class));
                break;
            case 5:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(Home.this, ChatActivity.class));
                break;

            case 6:
                drawer.closeDrawer(GravityCompat.START);
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "Hi , I am using Taxi-55 Android App, this is very useful and easy application for booking taxi from anywhere. Please download it from following link " + " http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName());
                startActivity(Intent.createChooser(sharingIntent, "Share using"));

                break;
            case 7:
                drawer.closeDrawer(GravityCompat.START);
                Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                }
                break;
            case 8:
                drawer.closeDrawer(GravityCompat.START);
                logout();
                break;
            case 9:
                drawer.closeDrawer(GravityCompat.START);
                exit();
                break;
        }
    }


    class LeftMenuItemAdapter extends BaseAdapter {


        private List<String> originalData = null;
        private LayoutInflater mInflater;
        ArrayList<Integer> itemsimg = new ArrayList<Integer>();

        public LeftMenuItemAdapter(Context context, List<String> delarnames) {
            this.originalData = delarnames;
            mInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemsimg.add(R.drawable.booking);
            itemsimg.add(R.drawable.riding_car);
            itemsimg.add(R.drawable.rate);
            itemsimg.add(R.drawable.profile);
            itemsimg.add(R.drawable.profile);
            itemsimg.add(R.drawable.chatt);

            itemsimg.add(R.drawable.share);
            itemsimg.add(R.drawable.rate_us);
            itemsimg.add(R.drawable.logout);
            itemsimg.add(R.drawable.exit);


        }


        @Override
        public int getCount() {
            return originalData.size();
        }

        @Override
        public Object getItem(int position) {
            return originalData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LeftMenuItemAdapter.ViewHolder holder = null;
            if (convertView == null) {
                convertView = mInflater
                        .inflate(R.layout.drawer_menu_item, null);
                holder = new LeftMenuItemAdapter.ViewHolder();

                holder.ic = (ImageView) convertView
                        .findViewById(R.id.imageView);
                holder.menu_Name = (TextView) convertView
                        .findViewById(R.id.menu_name_tv);
//
                //  holder.menu_Name.setTypeface(regular);

                convertView.setTag(holder);
                holder.menu_Name.setTag(R.id.menu_name_tv, position);
                holder.ic.setTag(R.id.imageView, position);
            } else {
                holder = (LeftMenuItemAdapter.ViewHolder) convertView.getTag();
            }
            holder.menu_Name.setText(originalData.get(position));
            holder.ic.setImageResource(itemsimg.get(position));
            holder.ic.setColorFilter(Color.WHITE);


            return convertView;
        }

        class ViewHolder {
            ImageView ic;
            TextView menu_Name;
        }
    }


    private static final float BLUR_RADIUS = 25f;

    public Bitmap blur(Bitmap image) {
        if (null == image) return null;

        Bitmap outputBitmap = Bitmap.createBitmap(image);
        final RenderScript renderScript = RenderScript.create(this);
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

//Intrinsic Gausian blur filter
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }


    public Location getLocation() {

        LocationManager mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                final int REQUEST_LOCATION = 2;

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                    // Display UI and wait for user interaction
                } else {
                    ActivityCompat.requestPermissions(
                            this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            REQUEST_LOCATION);
                }
            } else {
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    // Found best last known location: %s", l);
                    bestLocation = l;
                }
            }

        }

        return bestLocation;
    }

    public void initPlaceSearch() {
        PlaceAutocompleteFragment autocompleteFragment_pick = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_pick);
        ((EditText) autocompleteFragment_pick.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16.0f);
        ((EditText) autocompleteFragment_pick.getView().findViewById(R.id.place_autocomplete_search_input)).setTextColor(Color.WHITE);
        ((EditText) autocompleteFragment_pick.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Select Pickup Location");
        autocompleteFragment_pick.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                PICK = place.getLatLng();
                Log.i("Place", "Place: " + place.getName());
                // Toast.makeText(Home.this,place.getAddress(),Toast.LENGTH_LONG).show();
                pickAddress = place.getName() + " , " + place.getAddress();
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("Error", "An error occurred: " + status);
                Toast.makeText(Home.this, "An error occurred: " + status, Toast.LENGTH_LONG).show();

            }
        });


        PlaceAutocompleteFragment autocompleteFragment_drop = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_drop);
        ((EditText) autocompleteFragment_drop.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16.0f);
        ((EditText) autocompleteFragment_drop.getView().findViewById(R.id.place_autocomplete_search_input)).setTextColor(Color.WHITE);
        ((EditText) autocompleteFragment_drop.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Select Drop Location");
        autocompleteFragment_drop.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                DROP = place.getLatLng();
                Log.i("Place", "Place: " + place.getName());
                //Toast.makeText(Home.this,place.getAddress(),Toast.LENGTH_LONG).show();
                dropAddress = place.getName() + " , " + place.getAddress();

                if (PICK != null && DROP != null) {
                    drawRoaute();
                }
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("Error", "An error occurred: " + status);
                Toast.makeText(Home.this, "An error occurred: " + status, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void drawRoaute() {
        try {
            mMap.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.start);
        if (currentLocation != null) {
            LatLng location = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());

            mMap.addMarker(new
                    MarkerOptions().position(location).title("Your Current Location").icon(icon)).setDraggable(false);
        }
        if (PICK != null && DROP != null) {
            // Getting URL to the Google Directions API
            String url = getDirectionsUrl(PICK, DROP);

            DownloadTask downloadTask = new DownloadTask();

            // Start downloading json data from Google Directions API
            downloadTask.execute(url);

            mMap.addMarker(new MarkerOptions().position(PICK).title("Pickup Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).draggable(true));
            mMap.addMarker(new MarkerOptions().position(DROP).title("Drop Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)).draggable(true));

            LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
            boundsBuilder.include(PICK);
            boundsBuilder.include(DROP);

            LatLngBounds bounds = boundsBuilder.build();
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 3));
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
      */
/*  initPlaceSearch();*//*


        map = (ImageView) findViewById(R.id.map);

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(Home.this, LocaFromMapActivity.class));
            }
        });

        currentLocation = getLocation();
        map_Drop = (ImageView) findViewById(R.id.map_drop);
        map_pick = (ImageView) findViewById(R.id.map_pick);
        mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);


        menudrawer_list = (ListView) findViewById(R.id.drawer_list);
        menu_profile_emailid = (TextView) findViewById(R.id.menu_profile_emailid)
        ;

        menu_profile_name = (TextView) findViewById(R.id.menu_profile_name);

        try {
            PrefManager prefManager = new PrefManager(getApplicationContext());
            String nm = prefManager.getUsername();
            String unm = prefManager.getEmailId();
            menu_profile_name.setText(nm);
            menu_profile_emailid.setText(unm);


        } catch (Exception e1) {
            e1.printStackTrace();
        }

        LinearLayout imageView = (LinearLayout) findViewById(R.id.drawer_lin_lay);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.road);
        Bitmap blurredBitmap = blur(bitmap);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(blurredBitmap);
        imageView.setBackgroundDrawable(bitmapDrawable);
        menudrawer_list.setAdapter(new LeftMenuItemAdapter(this, Arrays.asList(meniListArray)));

        menudrawer_list.setOnItemClickListener(this);

       */
/* LocationListener mlocListener = new MyLocationListener();


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            final int REQUEST_LOCATION = 2;

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Display UI and wait for user interaction
            } else {
                ActivityCompat.requestPermissions(
                        this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_LOCATION);
            }
        } else {
            mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mlocListener);
        }*//*



        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();

                if (pickAddress.equals("")) {
                    Toast.makeText(Home.this, "Select Pickup Address", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (dropAddress.equals("")) {
                    Toast.makeText(Home.this, "Select Drop Address", Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent i = new Intent(Home.this, SelectCab.class);
                i.putExtra("PICK_ADD", pickAddress);
                i.putExtra("DROP_ADD", dropAddress);

                i.putExtra("PICK_LOC", PICK);
                i.putExtra("DROP_LOC", DROP);

                Location start = new Location("loc1");
                Location end = new Location("loc2");

                start.setLatitude(PICK.latitude);
                start.setLongitude(PICK.longitude);

                end.setLatitude(DROP.latitude);
                end.setLongitude(DROP.longitude);


                i.putExtra("DIST", DIST);
                i.putExtra("DURATION", DURATION);

                startActivity(i);
                // finish();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // super.onBackPressed();
            exit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

   */
/* @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_book) {

        } else if (id == R.id.nav_rides) {
            startActivity(new Intent(Home.this, MyRides.class));
        } else if (id == R.id.nav_rates) {
            startActivity(new Intent(Home.this, RateCard.class));
        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(Home.this, MyProfile.class));
        } else if (id == R.id.nav_share) {

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, "Hi , I am using Taxi-55 Android App, this is very useful and easy application for booking taxi from anywhere. Please download it from following link " + " http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName());
            startActivity(Intent.createChooser(sharingIntent, "Share using"));
        } else if (id == R.id.nav_rate_it) {

            Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
            }
        } else if (id == R.id.nav_logout) {
            logout();
        } else if (id == R.id.nav_exit) {
            exit();
        } else if (id == R.id.nav_cont_us) {

            startActivity(new Intent(Home.this, Emergency_Contact.class));
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }*//*


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        currentLocation = getLocation();
        // Add a marker in Sydney and move the camera


        LocationListener mlocListener = new MyLocationListener();


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            final int REQUEST_LOCATION = 2;

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Display UI and wait for user interaction
            } else {
                ActivityCompat.requestPermissions(
                        this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_LOCATION);
            }
        } else {
            mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mlocListener);
        }

        initPlaceSearch();
        if (currentLocation != null) {
          */
/*  BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.start);
            mMap.clear();
            LatLng location = new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());

            mMap.addMarker(new
                    MarkerOptions().position(location).title("Your Current Location").icon(icon)).setDraggable(false);

           mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location,15f));
           *//*


            drawRoaute();
            LatLng location = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 15f));
        }

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {

                Location loc = new Location(LocationManager.GPS_PROVIDER);
                loc.setLatitude(marker.getPosition().latitude);
                loc.setLongitude(marker.getPosition().longitude);

                if (marker.getTitle().equals("Pickup Location")) {
                    PICK = marker.getPosition();
                } else if (marker.getTitle().equals("Drop Location")) {
                    DROP = marker.getPosition();
                }

                drawRoaute();
                //currentLocation=loc;
            }
        });
        mMap.setOnMarkerClickListener(new OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                Geocoder geocoder = new Geocoder(Home.this, Locale.getDefault());

                try {
                    List<Address> addresses = geocoder.getFromLocation(marker.getPosition().latitude, marker.getPosition().longitude, 1);
                    Address obj = addresses.get(0);
                    String add = obj.getAddressLine(0);
                    add = add + "\n" + obj.getCountryName();
                    add = add + "\n" + obj.getCountryCode();
                    add = add + "\n" + obj.getAdminArea();
                    add = add + "\n" + obj.getPostalCode();
                    add = add + "\n" + obj.getSubAdminArea();
                    add = add + "\n" + obj.getLocality();
                    add = add + "\n" + obj.getSubThoroughfare();

                    Log.e("IGA", "Address" + add);

                    // Toast.makeText(Home.this, add, Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Toast.makeText(Home.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
    }


    public void logout() {
        AlertDialog.Builder dlg = new AlertDialog.Builder(Home.this);
        dlg.setTitle(R.string.app_name);
        dlg.setMessage("Are You Sure You Want To Logout ?");
        dlg.setIcon(R.drawable.icon);
        dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                PrefManager prefManager = new PrefManager(Home.this);
                prefManager.setUserName("");
                prefManager.setIsUserLoggedIn(false);
                finish();

            }
        });


        dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                return;
            }
        });

        dlg.create().show();
    }

    public void exit() {
        AlertDialog.Builder dlg = new AlertDialog.Builder(Home.this);
        dlg.setTitle(R.string.app_name);
        dlg.setMessage("Are You Sure You Want To Exit ?");
        dlg.setIcon(R.drawable.icon);
        dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();

            }
        });


        dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                return;
            }
        });

        dlg.create().show();
    }

    public class MyLocationListener implements LocationListener

    {

        @Override

        public void onLocationChanged(Location loc)

        {

            loc.getLatitude();

            loc.getLongitude();

            String Text = "My current location is: " +

                    "Latitud = " + loc.getLatitude() +

                    "Longitud = " + loc.getLongitude();


            //Toast.makeText( getApplicationContext(),Text,Toast.LENGTH_SHORT).show();
            currentLocation = loc;
           */
/* LatLng location = new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());
            mMap.clear();
            BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.start);
            mMap.addMarker(new
                    MarkerOptions().position(location).title("Your Current Location").icon(icon).draggable(false));
                    *//*


            drawRoaute();

        }


        @Override

        public void onProviderDisabled(String provider)

        {

            Toast.makeText(getApplicationContext(), "GPS Disabled", Toast.LENGTH_SHORT).show();

        }


        @Override

        public void onProviderEnabled(String provider) {

            Toast.makeText(getApplicationContext(), "GPS Enabled", Toast.LENGTH_SHORT).show();

        }


        @Override

        public void onStatusChanged(String provider, int status, Bundle extras)

        {


        }

    }*/
/* End of Class MyLocationListener *//*



    */
/**
     * A method to download json data from url
     *//*

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Error", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    */
/**
     * A class to parse the Google Places in JSON format
     *//*

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

      */
/*  // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                               // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    if(j==0){    // Get distance from the list
                        DIST = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Get duration from the list
                        DURATION = (String)point.get("duration");
                        continue;
                    }

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.RED);

            }

            Toast.makeText(Home.this,"Dist "+DIST +"Duration  "+DURATION,Toast.LENGTH_SHORT).show();
            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }
        *//*



        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            if (result.size() < 1) {
                Toast.makeText(getBaseContext(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {    // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duration = (String) point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.parseColor("#399ed7"));
            }

            DIST = distance;
            DURATION = duration;
            // Toast.makeText(Home.this,"Dist "+distance +"Duration  "+duration,Toast.LENGTH_SHORT).show();
            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }
    }
}
*/
