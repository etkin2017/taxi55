package in.org.cyberspace.taxi55.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class MyPreferenceManager {
    private static final String KEY_NOTIFICATIONS = "notifications";
    private static final String KEY_NOTIFICATION_BY_ID = "notifications_by_id";
    private static final String KEY_NOTIFICATION_BY_ID_GROUP = "notifications_by_group";
    private static final String KEY_USER_EMAIL = "user_email";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_USER_NAME = "user_name";
    private static final String PREF_NAME = "androidhive_gcm";
    int PRIVATE_MODE = 0;
    private String TAG = MyPreferenceManager.class.getSimpleName();
    Context _context;
    Editor editor;
    SharedPreferences pref;

    public MyPreferenceManager(Context context) {
        this._context = context;
        this.pref = this._context.getSharedPreferences(PREF_NAME, this.PRIVATE_MODE);
        this.editor = this.pref.edit();
    }

    public void addNotification(String notification) {
        String oldNotifications = getNotifications();
        if (oldNotifications != null) {
            oldNotifications = oldNotifications + "|" + notification;
        } else {
            oldNotifications = notification;
        }
        this.editor.putString(KEY_NOTIFICATIONS, oldNotifications);
        this.editor.commit();
    }

    public void removeNotimessage() {
        this.editor.remove(KEY_NOTIFICATIONS);
        this.editor.apply();
        this.editor.commit();
    }

    public void addNotificationById(String id) {
        String oldNotifications = getNotificationsById(id);
        if (oldNotifications != null) {
            oldNotifications = oldNotifications + "|" + id;
        } else {
            oldNotifications = id;
        }
        this.editor.putString(KEY_NOTIFICATION_BY_ID + id, oldNotifications);
        this.editor.commit();
    }

    public void addNotificationByGroup(String g_id) {
        String oldNotifications = getNotificationsByGroup(g_id);
        if (oldNotifications != null) {
            oldNotifications = oldNotifications + "|" + g_id;
        } else {
            oldNotifications = g_id;
        }
        this.editor.putString(KEY_NOTIFICATION_BY_ID_GROUP + g_id, oldNotifications);
        this.editor.commit();
    }

    public void removeNotificaion(String key) {
        this.editor.remove(KEY_NOTIFICATION_BY_ID + key);
        this.editor.apply();
        this.editor.commit();
    }

    public void removeGroupNotificaion(String key) {
        this.editor.remove(KEY_NOTIFICATION_BY_ID_GROUP + key);
        this.editor.apply();
        this.editor.commit();
    }

    public String getNotifications() {
        return this.pref.getString(KEY_NOTIFICATIONS, null);
    }

    public String getNotificationsById(String id) {
        return this.pref.getString(KEY_NOTIFICATION_BY_ID + id, null);
    }

    public String getNotificationsByGroup(String id) {
        return this.pref.getString(KEY_NOTIFICATION_BY_ID_GROUP + id, null);
    }

    public void clear() {
        this.editor.clear();
        this.editor.commit();
    }
}


/*
package in.org.cyberspace.taxi55.application;

import android.content.Context;
import android.content.SharedPreferences;

*/
/**
 * Created by Ashvini on 3/2/2017.
 *//*


public class MyPreferenceManager {


    private String TAG = MyPreferenceManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "androidhive_gcm";

    // All Shared Preferences Keys
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_USER_NAME = "user_name";
    private static final String KEY_USER_EMAIL = "user_email";
    private static final String KEY_NOTIFICATIONS = "notifications";
    private static final String KEY_NOTIFICATION_BY_ID = "notifications_by_id";
    private static final String KEY_NOTIFICATION_BY_ID_GROUP = "notifications_by_group";


    // Constructor
    public MyPreferenceManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }



    public void addNotification(String notification) {

        // get old notifications
        String oldNotifications = getNotifications();

        if (oldNotifications != null) {
            oldNotifications += "|" + notification;
        } else {
            oldNotifications = notification;
        }

        editor.putString(KEY_NOTIFICATIONS, oldNotifications);
        editor.commit();
    }


    public void removeNotimessage() {
        editor.remove(KEY_NOTIFICATIONS);
        editor.apply();
        editor.commit();
    }

    public void addNotificationById(String id) {

        // get old notifications
        String oldNotifications = getNotificationsById(id);

        if (oldNotifications != null) {
            oldNotifications += "|" + id;
        } else {
            oldNotifications = id;
        }

        editor.putString(KEY_NOTIFICATION_BY_ID + id, oldNotifications);
        editor.commit();
    }

    public void addNotificationByGroup(String g_id) {

        // get old notifications
        String oldNotifications = getNotificationsByGroup(g_id);

        if (oldNotifications != null) {
            oldNotifications += "|" + g_id;
        } else {
            oldNotifications = g_id;
        }

        editor.putString(KEY_NOTIFICATION_BY_ID_GROUP + g_id, oldNotifications);
        editor.commit();
    }


    public void removeNotificaion(String key) {
        editor.remove(KEY_NOTIFICATION_BY_ID + key);
        editor.apply();
        editor.commit();
    }


    public void removeGroupNotificaion(String key) {
        editor.remove(KEY_NOTIFICATION_BY_ID_GROUP + key);
        editor.apply();
        editor.commit();
    }


    public String getNotifications() {
        return pref.getString(KEY_NOTIFICATIONS, null);
    }

    public String getNotificationsById(String id) {
        return pref.getString(KEY_NOTIFICATION_BY_ID+id, null);
    }

    public String getNotificationsByGroup(String id) {
        return pref.getString(KEY_NOTIFICATION_BY_ID_GROUP+id, null);
    }

    public void clear() {
        editor.clear();
        editor.commit();
    }
}
*/
