package in.org.cyberspace.taxi55;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * Created by Wasim on 17/02/2017.
 */

public class TabNight extends Fragment {
    String[] N_CAB;
    String[] N_Initial;
    String[] N_Rest;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_day, container, false);

        N_CAB=getArguments().getStringArray("CAB");
        N_Initial=getArguments().getStringArray("INIT");
        N_Rest=getArguments().getStringArray("REST");

        Log.i("Frag CABS",N_CAB.length+"");

        ListView listView = (ListView) v.findViewById(R.id.listView1);
        RateListAdapter adapter = new RateListAdapter(getActivity(),N_CAB,N_Initial,N_Rest);

        listView.setAdapter(adapter);

        return v;

    }
}
