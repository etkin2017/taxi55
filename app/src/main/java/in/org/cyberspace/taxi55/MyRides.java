package in.org.cyberspace.taxi55;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyRides extends AppCompatActivity {

    ImageView back_arrow;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_rides);
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        back_arrow = (ImageView) findViewById(R.id.back_arrow);

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                Intent u=new Intent(MyRides.this, LocaFromMapActivity.class);
                u.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(u);
            }
        });



        getRides();
    }

    public void getRides() {
        String username = new PrefManager(this).getUsername();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("token", username);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "load_trips", jsonObject, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.e("Response", response.toString());
                try {

                    if (response.getString("status").equals("success")) {
                        JSONArray jsonArray = response.getJSONArray("all");
                        String[] ID = new String[jsonArray.length()];
                        String[] Dates = new String[jsonArray.length()];
                        String[] Times = new String[jsonArray.length()];
                        String[] Amt = new String[jsonArray.length()];
                        String[] Status = new String[jsonArray.length()];
                        String[] FCM = new String[jsonArray.length()];
                        String[] PHONE = new String[jsonArray.length()];
                        String[] NAMES = new String[jsonArray.length()];
                        String[] ASSIGNED_FOR = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            ID[i] = "Booking ID : " + jsonObject1.getString("uneaque_id");
                            Dates[i] = "Date : " + jsonObject1.getString("pickup_date");
                            Times[i] = "Time : " + jsonObject1.getString("pickup_time");
                            Amt[i] = "Amount : " + jsonObject1.getString("amount");
                            Status[i] = "Status : " + jsonObject1.getString("status");
                            try {
                                ASSIGNED_FOR[i] = jsonObject1.getString("assigned_for") + "@";

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                FCM[i] = jsonObject1.getString("fcm");
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                            try {
                                PHONE[i] = jsonObject1.getString("phone");
                            } catch (Exception e3) {
                                try {
                                    e3.printStackTrace();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    return;
                                }
                            }
                            try {
                                NAMES[i] = jsonObject1.getString("name");
                            } catch (Exception ee) {
                                ee.printStackTrace();
                            }
                        }
                        ((ListView) MyRides.this.findViewById(R.id.listView1)).setAdapter(new RideAdapter(MyRides.this, ID, Dates, Times, Amt, Status, FCM, PHONE, NAMES, ASSIGNED_FOR));
                    }
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                error.printStackTrace();
              //  Toast.makeText(MyRides.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "MyRideRequests");
    }
}

/*
package in.org.cyberspace.taxi55;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyRides extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_rides);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getRides();
    }

    public void getRides()
    {
        PrefManager prefManager = new PrefManager(this);
        String username=prefManager.getUsername();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("token",username);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final ProgressDialog progressDialog = new ProgressDialog(MyRides.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "load_trips", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.e("Response",response.toString());

                try {
                    if(response.getString("status").equals("success"))
                    {
                        JSONArray jsonArray= response.getJSONArray("all");
                        String[] ID=new String[jsonArray.length()];
                        String[] Dates=new String[jsonArray.length()];
                        String[] Times=new String[jsonArray.length()];
                        String[] Amt=new String[jsonArray.length()];
                        String[] Status=new String[jsonArray.length()];
                        String[] FCM=new String[jsonArray.length()];
                        String[] PHONE=new String[jsonArray.length()];
                        String[] NAMES = new String[jsonArray.length()];
                        for (int i=0;i<jsonArray.length();i++)
                        {
                           JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            ID[i]="Booking ID : "+jsonObject1.getString("uneaque_id");
                            Dates[i]="Date : "+jsonObject1.getString("pickup_date");
                            Times[i]="Time : "+jsonObject1.getString("pickup_time");
                            Amt[i]="Amount : "+jsonObject1.getString("amount");
                            Status[i]="Status : "+jsonObject1.getString("status");
                            try {
                                FCM[i] = jsonObject1.getString("fcm");
                            }catch (Exception e2){
                                e2.printStackTrace();
                            }
                            try {
                                PHONE[i] = jsonObject1.getString("phone");
                            }catch (Exception e3){
                                e3.printStackTrace();
                            }

                            try{

                                NAMES[i] = jsonObject1.getString("name");

                            }catch (Exception ee){
                                ee.printStackTrace();
                            }


                        }

                        ListView listView=(ListView) findViewById(R.id.listView1);
                        RideAdapter rideAdapter = new RideAdapter(MyRides.this,ID,Dates,Times,Amt,Status,FCM,PHONE,NAMES);
                        listView.setAdapter(rideAdapter);
                    }
                    else
                    {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(MyRides.this,error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest,"MyRideRequests");

    }
}
*/
