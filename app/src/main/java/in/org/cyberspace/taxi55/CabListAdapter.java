package in.org.cyberspace.taxi55;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CabListAdapter extends BaseAdapter {
    String[] CAB;
    String[] FARE;
    Activity mContex;

    public CabListAdapter(Activity context, String[] cab, String[] fare) {
        this.mContex = context;
        this.CAB = cab;
        this.FARE = fare;
    }

    public int getCount() {
        return this.CAB.length;
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View row = this.mContex.getLayoutInflater().inflate(R.layout.taxi_list_items, viewGroup, false);
        TextView tvFare = (TextView) row.findViewById(R.id.tvFair);
        ((TextView) row.findViewById(R.id.tvTaxiName)).setText(this.CAB[i]);
        tvFare.setText(this.FARE[i]);
        return row;
    }
}


/*
package in.org.cyberspace.taxi55;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

*/
/**
 * Created by Wasim on 13/02/2017.
 *//*


public class CabListAdapter extends BaseAdapter {

    String[] CAB,FARE;
    Activity mContex;
    public CabListAdapter(Activity context, String[] cab, String[] fare)
    {
        this.mContex=context;
        this.CAB=cab;
        this.FARE=fare;
    }
    @Override
    public int getCount() {
        return CAB.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = mContex.getLayoutInflater();
        View row=layoutInflater.inflate(R.layout.taxi_list_items,viewGroup,false);
        TextView tvCab,tvFare;
        tvCab=(TextView)row.findViewById(R.id.tvTaxiName);
        tvFare=(TextView)row.findViewById(R.id.tvFair);

        tvCab.setText(CAB[i]);
        tvFare.setText(FARE[i]);

        return row;
    }
}
*/
