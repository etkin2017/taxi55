package in.org.cyberspace.taxi55.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import in.org.cyberspace.taxi55.PrefManager;
import in.org.cyberspace.taxi55.model.NewMessage;
import in.org.cyberspace.taxi55.model.User;


/**
 * Created by Ashvini on 2/26/2017.
 */

public class NotiDbHelper extends SQLiteOpenHelper {

    static String DATABASE_NAME = "noti.db";


    public static final String TABLE_NAME_TWo = "noti";
    public static final String NEXT = "next_id";

    public static final String KEY_LNAME = "lname";
    public static final String KEY_ID = "id";
    public static final String TABLE_NAME = "users";
    public static final String CHAT_ROOM_ID = "chat_room_id";
    public static final String MESSAGE = "message";
    public static final String CREATED_AT = "created_at";

    public static final String messageTable = "message";
    public static final String mid = "id";
    public static final String sid = "sid";
    public static final String rid = "rid";
    public static final String msg = "message";
    public static final String stime = "stime";
    public static final String rtime = "rtime";
    public static final String mtypr = "type";
    public static final String uid = "uid";
    public static final String chk_image = "chk_image";
    public static final String rs = "rs";
    public static final String r_name = "r_name";
    Context mctx;

    public NotiDbHelper(Context context) {

        super(context, DATABASE_NAME, null, 2);
        mctx = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String MESSAGE = "CREATE TABLE " + messageTable + " (" + mid + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + sid + " TEXT, " + rid + " TEXT, " + msg + " TEXT ," + stime + " DATETIME DEFAULT CURRENT_TIMESTAMP ," + rtime + " DATETIME DEFAULT CURRENT_TIMESTAMP ," + uid + " INTEGER , " + rs + " INTEGER , " + r_name +
                " TEXT )";

        db.execSQL(MESSAGE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + MESSAGE);
        onCreate(db);
    }

    public void saveSingleMessage(NewMessage msgs) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();

        values.put(sid, msgs.getSid());
        values.put(rid, msgs.getRid());
        values.put(msg, msgs.getMsg());
        values.put(stime, msgs.getStime());
        values.put(rtime, msgs.getRtime());
        values.put(uid, msgs.getUid());
        values.put(rs, msgs.getRs());
        values.put(r_name, msgs.getR_name());
        db.insert(messageTable, null, values);
        db.close();
    }

    public ArrayList<NewMessage> getMessageByRid_Sid(String msid, String mrid) {

        ArrayList<NewMessage> mmm = new ArrayList<NewMessage>();
        try {
            String query = "select * from " + messageTable + " where " + sid + " = '" + msid + "' AND " + rid + " = '" + mrid + "'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    NewMessage message1 = new NewMessage(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), cursor.getString(8));
                    mmm.add(message1);
                    //Log.e("msg", message1.toString());

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mmm;
    }


    public ArrayList<User> getUserByChat() {
        String query = "select DISTINCT(rid) from " + messageTable + "  ORDER BY   id  DESC ";
        ArrayList<User> u1 = new ArrayList<User>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {


                PrefManager manager = new PrefManager(mctx);
                String mb_data = manager.getUserMobile();
                if (!mb_data.equals(cursor.getString(0))) {
                    User user = getUserById(cursor.getString(0));

                    if (user != null) {
                        u1.add(user);
                        //Log.e("usersnoti", user.toString());
                    }
                }


            } while (cursor.moveToNext());
        }
        return u1;

        //return null;
    }


    public User getUserById(String rid) {
        User user = null;
        String query = "select r_name from " + messageTable + " WHERE rid = '" + rid + "' AND rs = " + 1;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            //String name, String image, int type, int id, String mobile, String status,int aid
            // id + " INTEGER, " + name + " TEXT," + image + " TEXT," + mobile + " TEXT," + type + " INTEGER ,
            // " + ustatus + " TEXT ,
            // " + aid + " INTEGER)";


            user = new User(rid, cursor.getString(0));

        }
        return user;
    }

}
