package in.org.cyberspace.taxi55;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

public class BookTaxi extends AppCompatActivity {
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_taxi);
        PlaceAutocompleteFragment autocompleteFragment_pick = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_pick);
        ((EditText) autocompleteFragment_pick.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16.0f);
        ((EditText) autocompleteFragment_pick.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Select Pickup Location");
        autocompleteFragment_pick.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            public void onPlaceSelected(Place place) {
                Log.i("Place", "Place: " + place.getName());
                Toast.makeText(BookTaxi.this, place.getAddress(), 1).show();
            }

            public void onError(Status status) {
                Log.i("Error", "An error occurred: " + status);
                Toast.makeText(BookTaxi.this, "An error occurred: " + status, 1).show();
            }
        });
        PlaceAutocompleteFragment autocompleteFragment_drop = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_drop);
        ((EditText) autocompleteFragment_drop.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16.0f);
        ((EditText) autocompleteFragment_drop.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Select Drop Location");
        autocompleteFragment_drop.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            public void onPlaceSelected(Place place) {
                Log.i("Place", "Place: " + place.getName());
                Toast.makeText(BookTaxi.this, place.getAddress(), 1).show();
            }

            public void onError(Status status) {
                Log.i("Error", "An error occurred: " + status);
                Toast.makeText(BookTaxi.this, "An error occurred: " + status, 1).show();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != this.PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            return;
        }
        if (resultCode == -1) {
            Place place = PlaceAutocomplete.getPlace(this, data);
            Log.i("Place", "Place: " + place.getName());
            Toast.makeText(this, place.getAddress(), 1).show();
        } else if (resultCode == 2) {
            Log.i("Place", PlaceAutocomplete.getStatus(this, data).getStatusMessage());
        } else if (resultCode != 0) {
        }
    }
}
/*
package in.org.cyberspace.taxi55;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

public class BookTaxi extends AppCompatActivity {
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_taxi);



        PlaceAutocompleteFragment autocompleteFragment_pick = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_pick);
        ((EditText)autocompleteFragment_pick.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16.0f);
        ((EditText)autocompleteFragment_pick.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Select Pickup Location");
        autocompleteFragment_pick.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.

                Log.i("Place", "Place: " + place.getName());
                Toast.makeText(BookTaxi.this,place.getAddress(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("Error", "An error occurred: " + status);
                Toast.makeText(BookTaxi.this,"An error occurred: " + status,Toast.LENGTH_LONG).show();
            }
        });



        PlaceAutocompleteFragment autocompleteFragment_drop = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment_drop);
        ((EditText)autocompleteFragment_drop.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(16.0f);
        ((EditText)autocompleteFragment_drop.getView().findViewById(R.id.place_autocomplete_search_input)).setHint("Select Drop Location");
        autocompleteFragment_drop.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.

                Log.i("Place", "Place: " + place.getName());
                Toast.makeText(BookTaxi.this,place.getAddress(),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("Error", "An error occurred: " + status);
                Toast.makeText(BookTaxi.this,"An error occurred: " + status,Toast.LENGTH_LONG).show();
            }
        });


     */
/*   try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }

        *//*

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("Place", "Place: " + place.getName());
                Toast.makeText(BookTaxi.this,place.getAddress(),Toast.LENGTH_LONG).show();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("Place", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }
}
*/
