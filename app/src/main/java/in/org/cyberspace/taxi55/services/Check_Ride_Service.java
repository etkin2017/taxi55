package in.org.cyberspace.taxi55.services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import in.org.cyberspace.taxi55.AppSingleton;
import in.org.cyberspace.taxi55.BuildConfig;
import in.org.cyberspace.taxi55.ConnectionConfiguration;
import in.org.cyberspace.taxi55.PrefManager;
import in.org.cyberspace.taxi55.application.DAO;
import in.org.cyberspace.taxi55.db.NotiDbHelper;
import in.org.cyberspace.taxi55.model.EmergencyContactsPojo;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Check_Ride_Service extends Service {
    String[] DATE;
    List<EmergencyContactsPojo> passengerEmergencyContactsList;
    String upcoming_rides = ConnectionConfiguration.URL+ "fetch_upcoming_rides";

    @Nullable
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        this.passengerEmergencyContactsList = new ArrayList();
        try {
            if (new PrefManager(getApplicationContext()).getUSER_Id() != null) {
                fetchRideDetails();
            }
        } catch (Exception eq) {
            eq.printStackTrace();
        }
    }

    public void fetchCOntacts(String date1, String unm1, String tm1, String frm, String to, String driver_nm, String driver_no, String taxi_number) {
        String Did = new PrefManager(this).getUSER_Id();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", Did);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String str = date1;
        final String str2 = unm1;
        final String str3 = tm1;
        final String str4 = frm;
        final String str5 = to;
        final String str6 = driver_nm;
        final String str7 = driver_no;
        final String str8 = taxi_number;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL+"all_emergency_contact", jsonObject, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                Log.e("PassengerResponse", response.toString());
                try {
                    JSONArray jsonArray = response.getJSONArray("contacts");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        final JSONObject jsonobject = jsonArray.getJSONObject(i);
                        Check_Ride_Service.this.passengerEmergencyContactsList.add(new EmergencyContactsPojo(Integer.valueOf(jsonobject.getInt(NotiDbHelper.mid)), Integer.valueOf(jsonobject.getInt("user_id")), jsonobject.getString("mob_no"), jsonobject.getString("name")));
                        new AsyncTask() {
                            protected Object doInBackground(Object[] params) {
                                try {
                                    Check_Ride_Service.this.sendSms(jsonobject.getString("mob_no"), str, str2, str3, str4, str5, str6, str7, str8);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }
                        }.execute(new Object[0]);
                    }
                    Check_Ride_Service.this.stopSelf();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }

    public void sendSms(String send_no, String date1, String unm1, String tm1, String frm, String to, String driver_nm, String driver_no, String taxi_number) {
        final HashMap<String, String> data = new HashMap();
        final String str = date1;
        final String str2 = tm1;
        final String str3 = unm1;
        final String str4 = frm;
        final String str5 = to;
        final String str6 = taxi_number;
        final String str7 = driver_nm;
        final String str8 = driver_no;
        final String str9 = send_no;
        new AsyncTask() {
            String res = null;

            protected Object doInBackground(Object[] params) {
                data.put("date", str);
                data.put("time", str2);
                data.put("nm", str3);
                data.put("from", str4 + BuildConfig.FLAVOR);
                data.put("to", str5);
                data.put("taxi_no", str6);
                data.put("driver_nm", str7);
                data.put("driver_no", str8);
                data.put("send_no", str9);
                this.res = DAO.getStringFromUrl("http://shrisadgurucoaching.com/send_sms_taxi.php", data);
                return this.res;
            }

            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                Log.e("RESEUT", this.res.toString());
                Editor ed = Check_Ride_Service.this.getSharedPreferences("sent_sms", 0).edit();
                ed.putBoolean("sent", true);
                ed.commit();
                Toast.makeText(Check_Ride_Service.this.getApplicationContext(), "msg sent", Toast.LENGTH_LONG).show();
            }
        }.execute(new Object[]{null, null, null});
    }

    public void fetchRideDetails() {
        PrefManager prefManager = new PrefManager(this);
        String Did = prefManager.getUSER_Id();
        String user_nm = prefManager.getUsername();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("status", "Processing");
            jsonObject.put("user_name", user_nm);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL+"fetch_upcoming_rides", jsonObject, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                Log.e("PassengerResponse", response.toString());
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    Check_Ride_Service.this.DATE = new String[jsonArray.length()];
                    String dateInString = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
                    Log.e("Date_InString", dateInString);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonobject11 = jsonArray.getJSONObject(i);
                        Check_Ride_Service.this.DATE[i] = jsonobject11.getString("pickup_date");
                        if (Check_Ride_Service.this.DATE[i].equals(dateInString)) {
                            final String $_date = jsonobject11.getString("pickup_date");
                            final String $_pickup_area = jsonobject11.getString("pickup_area");
                            final String $_to_area = jsonobject11.getString("drop_area");
                            final String $_username = jsonobject11.getString("username");
                            final String $_pickup_time = jsonobject11.getString("pickup_time");
                            final String $_driver_name = jsonobject11.getString("name");
                            final String $_driver_no = jsonobject11.getString("phone");
                            final String $_taxi_no = jsonobject11.getString("car_no");
                            new AsyncTask() {
                                protected Object doInBackground(Object[] params) {
                                    Check_Ride_Service.this.fetchCOntacts($_date, $_username, $_pickup_time, $_pickup_area, $_to_area, $_driver_name, $_driver_no, $_taxi_no);
                                    return null;
                                }
                            }.execute(new Object[0]);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }

    public void SendSMS() throws URISyntaxException {
        String ACCOUNT_SID = "AC38d67d8434b3b1508b4886037b96ae10";
        String AUTH_TOKEN = "aa0d6fbc5d4d069512fec59bb972d0f7";
    }
}
