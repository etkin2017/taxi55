package in.org.cyberspace.taxi55.model;

public class NewMessage {
    private int id;
    private String msg;
    private int mtypr;
    private String r_name;
    private String rid;
    int rs;
    private String rtime;
    private String sid;
    private String stime;
    private int uid;

    public String getR_name() {
        return this.r_name;
    }

    public void setR_name(String r_name) {
        this.r_name = r_name;
    }

    public int getUid() {
        return this.uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String toString() {
        return "NewMessage{id=" + this.id + ", sid=" + this.sid + ", rid=" + this.rid + ", msg='" + this.msg + '\'' + ", stime='" + this.stime + '\'' + ", rtime='" + this.rtime + '\'' + ", mtypr=" + this.mtypr + ", uid=" + this.uid + ",rs=" + this.rs + ", r_name = " + this.r_name + '}';
    }

    public int getRs() {
        return this.rs;
    }

    public void setRs(int rs) {
        this.rs = rs;
    }

    public NewMessage(int id, String sid, String rid, String msg, String stime, String rtime, int uid, int rs, String r_name) {
        this.id = id;
        this.sid = sid;
        this.rid = rid;
        this.msg = msg;
        this.stime = stime;
        this.rtime = rtime;
        this.uid = uid;
        this.rs = rs;
        this.r_name = r_name;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSid() {
        return this.sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getRid() {
        return this.rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStime() {
        return this.stime;
    }

    public void setStime(String stime) {
        this.stime = stime;
    }

    public String getRtime() {
        return this.rtime;
    }

    public void setRtime(String rtime) {
        this.rtime = rtime;
    }

    public int getMtypr() {
        return this.mtypr;
    }

    public void setMtypr(int mtypr) {
        this.mtypr = mtypr;
    }
}


/*
package in.org.cyberspace.taxi55.model;

*/
/**
 * Created by Ashvini on 2/26/2017.
 *//*


public class NewMessage {

    private int id;
    private String sid;
    private String rid;
    private String msg;
    private String stime;
    private String rtime;
    private String r_name;
    private int mtypr;
    private int uid;

    int rs;


    public String getR_name() {
        return r_name;
    }

    public void setR_name(String r_name) {
        this.r_name = r_name;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "NewMessage{" +
                "id=" + id +
                ", sid=" + sid +
                ", rid=" + rid +
                ", msg='" + msg + '\'' +
                ", stime='" + stime + '\'' +
                ", rtime='" + rtime + '\'' +
                ", mtypr=" + mtypr +
                ", uid=" + uid +
                ",rs=" + rs +
                ", r_name = "+r_name+
                '}';
    }

    public int getRs() {
        return rs;
    }

    public void setRs(int rs) {
        this.rs = rs;
    }

    public NewMessage(int id, String sid, String rid, String msg, String stime, String rtime, int uid, int rs,String r_name) {
        this.id = id;
        this.sid = sid;
        this.rid = rid;
        this.msg = msg;
        this.stime = stime;
        this.rtime = rtime;

        this.uid = uid;

        this.rs = rs;
        this.r_name=r_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStime() {
        return stime;
    }

    public void setStime(String stime) {
        this.stime = stime;
    }

    public String getRtime() {
        return rtime;
    }

    public void setRtime(String rtime) {
        this.rtime = rtime;
    }

    public int getMtypr() {
        return mtypr;
    }

    public void setMtypr(int mtypr) {
        this.mtypr = mtypr;
    }

}
*/
