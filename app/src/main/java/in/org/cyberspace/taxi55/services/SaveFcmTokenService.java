package in.org.cyberspace.taxi55.services;

import android.content.SharedPreferences.Editor;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class SaveFcmTokenService extends FirebaseInstanceIdService {
    String TAG = "SaveFcmTokenService";

    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(this.TAG, "Refreshed token: " + refreshedToken);
        Editor editor = getSharedPreferences("fcm_token", 0).edit();
        editor.putString("fcm_token", refreshedToken);
        editor.commit();
    }
}


/*
package in.org.cyberspace.taxi55.services;

import android.content.SharedPreferences;
import android.util.Log;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static android.content.Context.MODE_PRIVATE;

*/
/**
 * Created by Ashvini on 2/25/2017.
 *//*


public class SaveFcmTokenService extends FirebaseInstanceIdService {

    String TAG = "SaveFcmTokenService";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Refreshed token: " + refreshedToken);
        SharedPreferences sh = getSharedPreferences("fcm_token", MODE_PRIVATE);
        SharedPreferences.Editor editor = sh.edit();
        editor.putString("fcm_token", refreshedToken);
        editor.commit();
    }
}
*/
