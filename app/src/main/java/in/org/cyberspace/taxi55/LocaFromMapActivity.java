package in.org.cyberspace.taxi55;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.readystatesoftware.viewbadger.BadgeView;

import in.org.cyberspace.taxi55.application.AppUtils.Constants;
import in.org.cyberspace.taxi55.application.AppUtils.LocationConstants;
import in.org.cyberspace.taxi55.fragment.DashBoardFragment;
import in.org.cyberspace.taxi55.services.MyService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LocaFromMapActivity extends AppCompatActivity implements OnItemClickListener {
    private static final float BLUR_RADIUS = 25.0f;
    ImageView back_arrow;
    BadgeView badge;
    String[] meniListArray = new String[]{"Book Taxi", "My Rides", "Rate Card", "Profile", "Emergency Contact", "Chat", "Share", "Rate Us", "Logout", "Exit"};
    TextView menu_profile_emailid;
    TextView menu_profile_name;
    ListView menudrawer_list;
    View target;
    Toolbar toolbar;
    private DrawerLayout drawer;

    class LeftMenuItemAdapter extends BaseAdapter {
        ArrayList<Integer> itemsimg = new ArrayList();
        private LayoutInflater mInflater;
        private List<String> originalData = null;

        class ViewHolder {
            ImageView ic;
            TextView menu_Name;

            ViewHolder() {
            }
        }

        public LeftMenuItemAdapter(Context context, List<String> delarnames) {
            this.originalData = delarnames;
            // this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
            this.mInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.itemsimg.add((R.drawable.booking));
            this.itemsimg.add((R.drawable.riding_car));
            this.itemsimg.add((R.drawable.rate));
            this.itemsimg.add((R.drawable.profile));
            this.itemsimg.add((R.drawable.profile));
            this.itemsimg.add((R.drawable.chatt));
            this.itemsimg.add((R.drawable.share));
            this.itemsimg.add((R.drawable.rate_us));
            this.itemsimg.add((R.drawable.logout));
            this.itemsimg.add((R.drawable.exit));
        }

        public int getCount() {
            return this.originalData.size();
        }

        public Object getItem(int position) {
            return this.originalData.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate(R.layout.drawer_menu_item, null);
                holder = new ViewHolder();
                holder.ic = (ImageView) convertView.findViewById(R.id.imageView);
                holder.menu_Name = (TextView) convertView.findViewById(R.id.menu_name_tv);
                convertView.setTag(holder);
                holder.menu_Name.setTag(R.id.menu_name_tv, Integer.valueOf(position));
                holder.ic.setTag(R.id.imageView, Integer.valueOf(position));
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.menu_Name.setText((CharSequence) this.originalData.get(position));
            holder.ic.setImageResource(((Integer) this.itemsimg.get(position)).intValue());
            holder.ic.setColorFilter(-1);
            return convertView;
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loca_from_map);
        this.menudrawer_list = (ListView) findViewById(R.id.drawer_list);
        this.menu_profile_emailid = (TextView) findViewById(R.id.menu_profile_emailid);
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, this.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        getSupportActionBar().setTitle("Book Taxi");
        startService(new Intent(this, MyService.class));
        this.menu_profile_name = (TextView) findViewById(R.id.menu_profile_name);
        try {
            PrefManager prefManager = new PrefManager(getApplicationContext());
            String nm = prefManager.getUsername();
            String unm = prefManager.getEmailId();
            this.menu_profile_name.setText(nm);
            this.menu_profile_emailid.setText(unm);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        ((LinearLayout) findViewById(R.id.drawer_lin_lay)).setBackgroundDrawable(new BitmapDrawable(blur(BitmapFactory.decodeResource(getResources(), R.drawable.road))));
        this.menudrawer_list.setAdapter(new LeftMenuItemAdapter(this, Arrays.asList(this.meniListArray)));
        this.menudrawer_list.setOnItemClickListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.activity_check_ace_cabs, new DashBoardFragment()).commit();

    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        switch (position) {
            case 0 /*0*/:
                finish();
                startActivity(new Intent(this, Home.class));
                return;
            case 1 /*1*/:
                startActivity(new Intent(this, MyRides.class));
                drawer.closeDrawer(GravityCompat.START);
                return;
            case 2/*2*/:
                startActivity(new Intent(this, RateCard.class));
                drawer.closeDrawer(GravityCompat.START);
                return;
            case 3/*3*/:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, MyProfile.class));
                return;
            case 4 /*4*/:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, Emergency_Contact.class));
                return;
            case 5 /*5*/:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, ChatActivity.class));
                return;
            case 6 /*6*/:
                drawer.closeDrawer(GravityCompat.START);
                Intent sharingIntent = new Intent("android.intent.action.SEND");
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra("android.intent.extra.TEXT", "Hi , I am using Taxi-55 Android App, this is very useful and easy application for booking taxi from anywhere. Please download it from following link  http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName());
                startActivity(Intent.createChooser(sharingIntent, "Share using"));
                return;
            case 7 /*7*/:
                drawer.closeDrawer(GravityCompat.START);
                Intent goToMarket = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + getApplicationContext().getPackageName()));
                goToMarket.addFlags(1208483840);
                try {
                    startActivity(goToMarket);
                    return;
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                    return;
                }
            case 8 /*8*/:
                drawer.closeDrawer(GravityCompat.START);
                logout();
                return;
            case 9 /*9*/:
                drawer.closeDrawer(GravityCompat.START);
                exit();
                return;
            default:
                return;
        }
    }

    public void logout() {
        Builder dlg = new Builder(this);
        dlg.setTitle(R.string.app_name);
        dlg.setMessage("Are You Sure You Want To Logout ?");
        dlg.setIcon(R.drawable.icon);
        dlg.setPositiveButton("Yes", new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                PrefManager prefManager = new PrefManager(LocaFromMapActivity.this);
                prefManager.setUserName(BuildConfig.FLAVOR);
                prefManager.setUSER_Id(BuildConfig.FLAVOR);
                prefManager.setIsUserLoggedIn(false);
                LocaFromMapActivity.this.finish();
            }
        });
        dlg.setNegativeButton("No", new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        dlg.create().show();
    }

    public void exit() {
        Builder dlg = new Builder(this);
        dlg.setTitle(R.string.app_name);
        dlg.setMessage("Are You Sure You Want To Exit ?");
        dlg.setIcon(R.drawable.icon);
        dlg.setPositiveButton("Yes", new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                LocaFromMapActivity.this.finish();
            }
        });
        dlg.setNegativeButton("No", new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        dlg.create().show();
    }

    public Bitmap blur(Bitmap image) {
        if (image == null) {
            return null;
        }
        Bitmap outputBitmap = Bitmap.createBitmap(image);
        RenderScript renderScript = RenderScript.create(this);
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }

    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            try {
                if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
                    getSupportFragmentManager().popBackStack();
                    dropNadd();
                    return;
                }
                super.onBackPressed();
            } catch (Exception e) {
                super.onBackPressed();
            }
        }


    }

    public void dropNadd() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
            fm.popBackStack();
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.activity_check_ace_cabs, new DashBoardFragment()).commit();
    }
}


/*
package in.org.cyberspace.taxi55;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import in.org.cyberspace.taxi55.fragment.DashBoardFragment;

public class LocaFromMapActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    String[] meniListArray = {"Book Taxi", "My Rides", "Rate Card", "Profile", "Emergency Contact","Chat",
            "Share", "Rate Us", "Logout", "Exit"};
    ListView menudrawer_list;
    TextView menu_profile_emailid, menu_profile_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loca_from_map);

        menudrawer_list = (ListView) findViewById(R.id.drawer_list);
        menu_profile_emailid = (TextView) findViewById(R.id.menu_profile_emailid)
        ;

        menu_profile_name = (TextView) findViewById(R.id.menu_profile_name);

        try {
            PrefManager prefManager = new PrefManager(getApplicationContext());
            String nm = prefManager.getUsername();
            String unm = prefManager.getEmailId();
            menu_profile_name.setText(nm);
            menu_profile_emailid.setText(unm);


        } catch (Exception e1) {
            e1.printStackTrace();
        }

        LinearLayout imageView = (LinearLayout) findViewById(R.id.drawer_lin_lay);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.road);
        Bitmap blurredBitmap = blur(bitmap);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(blurredBitmap);
        imageView.setBackgroundDrawable(bitmapDrawable);
        menudrawer_list.setAdapter(new LeftMenuItemAdapter(this, Arrays
                .asList(meniListArray)));

        menudrawer_list.setOnItemClickListener(this);

        DashBoardFragment dashBoardFragment = new DashBoardFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_check_ace_cabs, dashBoardFragment)
                .commit();


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        switch (position) {
            case 0:
                finish();
                startActivity(new Intent(LocaFromMapActivity.this,Home.class));

                break;
            case 1:
                startActivity(new Intent(LocaFromMapActivity.this, MyRides.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 2:
                startActivity(new Intent(LocaFromMapActivity.this, RateCard.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 3:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(LocaFromMapActivity.this, MyProfile.class));
                break;
            case 4:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(LocaFromMapActivity.this, Emergency_Contact.class));
                break;

            case 5:

                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(LocaFromMapActivity.this, ChatActivity.class));
                break;
            case 6:
                drawer.closeDrawer(GravityCompat.START);
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "Hi , I am using Taxi-55 Android App, this is very useful and easy application for booking taxi from anywhere. Please download it from following link " + " http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName());
                startActivity(Intent.createChooser(sharingIntent, "Share using"));

                break;
            case 7:
                drawer.closeDrawer(GravityCompat.START);
                Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));
                }
                break;
            case 8:
                drawer.closeDrawer(GravityCompat.START);
                logout();
                break;
            case 9:
                drawer.closeDrawer(GravityCompat.START);
                exit();
                break;
        }
    }

    public void logout() {
        AlertDialog.Builder dlg = new AlertDialog.Builder(LocaFromMapActivity.this);
        dlg.setTitle(R.string.app_name);
        dlg.setMessage("Are You Sure You Want To Logout ?");
        dlg.setIcon(R.drawable.icon);
        dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                PrefManager prefManager = new PrefManager(LocaFromMapActivity.this);
                prefManager.setUserName("");
                prefManager.setIsUserLoggedIn(false);
                finish();

            }
        });


        dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                return;
            }
        });

        dlg.create().show();
    }

    public void exit() {
        AlertDialog.Builder dlg = new AlertDialog.Builder(LocaFromMapActivity.this);
        dlg.setTitle(R.string.app_name);
        dlg.setMessage("Are You Sure You Want To Exit ?");
        dlg.setIcon(R.drawable.icon);
        dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();

            }
        });


        dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                return;
            }
        });

        dlg.create().show();
    }


    class LeftMenuItemAdapter extends BaseAdapter {


        private List<String> originalData = null;
        private LayoutInflater mInflater;
        ArrayList<Integer> itemsimg = new ArrayList<Integer>();

        public LeftMenuItemAdapter(Context context, List<String> delarnames) {
            this.originalData = delarnames;
            mInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemsimg.add(R.drawable.booking);
            itemsimg.add(R.drawable.riding_car);
            itemsimg.add(R.drawable.rate);
            itemsimg.add(R.drawable.profile);
            itemsimg.add(R.drawable.profile);
            itemsimg.add(R.drawable.chatt);
            itemsimg.add(R.drawable.share);
            itemsimg.add(R.drawable.rate_us);
            itemsimg.add(R.drawable.logout);
            itemsimg.add(R.drawable.exit);


        }


        @Override
        public int getCount() {
            return originalData.size();
        }

        @Override
        public Object getItem(int position) {
            return originalData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LeftMenuItemAdapter.ViewHolder holder = null;
            if (convertView == null) {
                convertView = mInflater
                        .inflate(R.layout.drawer_menu_item, null);
                holder = new LeftMenuItemAdapter.ViewHolder();

                holder.ic = (ImageView) convertView
                        .findViewById(R.id.imageView);
                holder.menu_Name = (TextView) convertView
                        .findViewById(R.id.menu_name_tv);
//
                //  holder.menu_Name.setTypeface(regular);

                convertView.setTag(holder);
                holder.menu_Name.setTag(R.id.menu_name_tv, position);
                holder.ic.setTag(R.id.imageView, position);
            } else {
                holder = (LeftMenuItemAdapter.ViewHolder) convertView.getTag();
            }
            holder.menu_Name.setText(originalData.get(position));
            holder.ic.setImageResource(itemsimg.get(position));
            holder.ic.setColorFilter(Color.WHITE);


            return convertView;
        }

        class ViewHolder {
            ImageView ic;
            TextView menu_Name;
        }
    }


    private static final float BLUR_RADIUS = 25f;

    public Bitmap blur(Bitmap image) {
        if (null == image) return null;

        Bitmap outputBitmap = Bitmap.createBitmap(image);
        final RenderScript renderScript = RenderScript.create(this);
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

//Intrinsic Gausian blur filter
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        try {
            if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
                getSupportFragmentManager().popBackStack();
                dropNadd();
            } else {
                super.onBackPressed();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dropNadd() {

        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }


        DashBoardFragment dashBoardFragment1 = new DashBoardFragment();
        // dashBoardFragment1.setmCenterLatLong(new LatLng(mLatitude, mLongitude));
        FragmentManager ridefragmentManagerg = getSupportFragmentManager();
        ridefragmentManagerg.beginTransaction()
                .replace(R.id.activity_check_ace_cabs, dashBoardFragment1)
                //.addToBackStack("ace")
                .commit();
    }
}
*/
