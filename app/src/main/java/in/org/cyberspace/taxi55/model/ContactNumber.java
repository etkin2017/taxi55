package in.org.cyberspace.taxi55.model;

/**
 * Created by Ashvini on 3/2/2017.
 */

public class ContactNumber {
    String name;
    String number;

    public ContactNumber() {
    }

    public ContactNumber(String name, String number) {
        this.name = name;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
