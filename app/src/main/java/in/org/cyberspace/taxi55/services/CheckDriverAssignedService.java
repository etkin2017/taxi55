package in.org.cyberspace.taxi55.services;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import in.org.cyberspace.taxi55.AppSingleton;
import in.org.cyberspace.taxi55.ConnectionConfiguration;
import in.org.cyberspace.taxi55.DriverTrackingActivity;
import in.org.cyberspace.taxi55.PrefManager;
import in.org.cyberspace.taxi55.RequestProcessingActivity;
import in.org.cyberspace.taxi55.RideAdapter;
import in.org.cyberspace.taxi55.Success;
import in.org.cyberspace.taxi55.db.NotiDbHelper;

public class CheckDriverAssignedService extends Service {
    public CheckDriverAssignedService() {
    }

    String booking_id;
    public static String name = "Assigned_for";

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        booking_id = intent.getStringExtra("booking_id");
        go_to_map(booking_id);

    }

    public void go_to_map(final String booking_id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("booking_id", booking_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "check_booking_assigned_for", jsonObject, new Response.Listener<JSONObject>() {
            public void onResponse(JSONObject response) {

                Log.e("PassengerResponse", response.toString());
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    Intent mapAct = new Intent(CheckDriverAssignedService.this, DriverTrackingActivity.class);
                    String driver_id = jsonArray.getJSONObject(0).getString("assigned_for");
                    if (driver_id.equals("")) {
                        //  Toast.makeText(CheckDriverAssignedService.this.getApplicationContext(), "Driver Not Assigned To this booking", Toast.LENGTH_LONG).show();
                        return;
                    } else {
                        String mobile = new PrefManager(CheckDriverAssignedService.this).getUserMobile();

                        mapAct.putExtra("driver_id", driver_id);
                        mapAct.putExtra("driver_nm", jsonArray.getJSONObject(0).getString("name"));
                        mapAct.putExtra("user_name", jsonArray.getJSONObject(0).getString("user_name"));
                        mapAct.putExtra("phone", jsonArray.getJSONObject(0).getString("phone"));
                        mapAct.putExtra("email", jsonArray.getJSONObject(0).getString("email"));
                        mapAct.putExtra("booking_id", booking_id);
                        mapAct.putExtra(NotiDbHelper.sid, mobile);
                        mapAct.putExtra(NotiDbHelper.rid, jsonArray.getJSONObject(0).getString("phone"));
                        mapAct.putExtra("fcm", jsonArray.getJSONObject(0).getString("fcm"));
                        mapAct.putExtra("uname", jsonArray.getJSONObject(0).getString("user_name"));
                        mapAct.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        CheckDriverAssignedService.this.startActivity(mapAct);

                        stopSelf();


                    }
                } catch (JSONException e) {
                    go_to_map(booking_id);
                    e.printStackTrace();


                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
                go_to_map(booking_id);
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(15000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }

}
