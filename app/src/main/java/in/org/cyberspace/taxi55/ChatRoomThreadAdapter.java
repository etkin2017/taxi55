package in.org.cyberspace.taxi55;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView.Adapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import in.org.cyberspace.taxi55.model.NewMessage;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ChatRoomThreadAdapter extends Adapter<ChatRoomThreadAdapter.ViewHolder> {
    private static String today;
    private int SELF = 100;
    Bitmap mBitmap;
    private Context mContext;
    private ArrayList<NewMessage> messageArrayList;
    private String name1;
    private String other_name;

    public class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        TextView message1 = ((TextView) this.itemView.findViewById(R.id.message));
        TextView rs = ((TextView) this.itemView.findViewById(R.id.rs));
        TextView timestamp = ((TextView) this.itemView.findViewById(R.id.timestamp));

        public ViewHolder(View view) {
            super(view);
        }
    }

    public ArrayList<NewMessage> getMessageArrayList() {
        return this.messageArrayList;
    }

    public void setMessageArrayList(ArrayList<NewMessage> messageArrayList) {
        this.messageArrayList = messageArrayList;
    }

    public ChatRoomThreadAdapter(Context mContext, ArrayList<NewMessage> messageArrayList, String other_name) {
        this.mContext = mContext;
        this.messageArrayList = messageArrayList;
        this.other_name = other_name;
        today = String.valueOf(Calendar.getInstance().get(5));
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == this.SELF) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_self, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item_other, parent, false);
        }
        return new ViewHolder(itemView);
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        NewMessage message = (NewMessage) this.messageArrayList.get(position);
        String timestamp = getTimeStamp(message.getRtime());
        holder.setIsRecyclable(false);
        holder.rs.setText(message.getRs() + BuildConfig.FLAVOR);
        if ((message.getId() + BuildConfig.FLAVOR) != null) {
            if (holder.rs.getText().toString().equals("0")) {
                timestamp = "me, " + timestamp;
            } else {
                timestamp = this.other_name + " , " + timestamp;
            }
            holder.message1.setText(Html.fromHtml(message.getMsg()));
            holder.timestamp.setText(timestamp);
        }
    }

    public int getItemViewType(int position) {
        NewMessage message = (NewMessage) this.messageArrayList.get(position);
        DBHelper dbHelper = new DBHelper(this.mContext);
        if (message.getRs() != 1) {
            return this.SELF;
        }
        return position;
    }

    public int getItemCount() {
        return this.messageArrayList.size();
    }

    public static String getTimeStamp(String dateStr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = BuildConfig.FLAVOR;
        today = today.length() < 2 ? "0" + today : today;
        try {
            Date date = format.parse(dateStr);
            timestamp = (new SimpleDateFormat("dd").format(date).equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a")).format(date).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timestamp;
    }
}
/*
package in.org.cyberspace.taxi55;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import in.org.cyberspace.taxi55.model.NewMessage;


*/
/**
 * Created by Ashvini on 3/4/2017.
 *//*


public class ChatRoomThreadAdapter extends RecyclerView.Adapter<ChatRoomThreadAdapter.ViewHolder> {
    private static String today;
    private int SELF = 100;
    Bitmap mBitmap;

    private Context mContext;
    private ArrayList<NewMessage> messageArrayList;
    private String name1, other_name;

    public ArrayList<NewMessage> getMessageArrayList() {
        return messageArrayList;
    }

    public void setMessageArrayList(ArrayList<NewMessage> messageArrayList) {
        this.messageArrayList = messageArrayList;
    }

    public ChatRoomThreadAdapter(Context mContext, ArrayList<NewMessage> messageArrayList, String other_name) {
        this.mContext = mContext;
        this.messageArrayList = messageArrayList;
        this.other_name = other_name;
        Calendar calendar = Calendar.getInstance();
        today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView;

        // view type is to identify where to render the chat message
        // left or right
        if (viewType == SELF) {
            // self message
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_self, parent, false);
        } else {
            // others message
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_item_other, parent, false);
        }


        return new ViewHolder(itemView);
        //  return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final NewMessage message = messageArrayList.get(position);
        String timestamp = getTimeStamp(message.getRtime());
        holder.setIsRecyclable(false);
        holder.rs.setText(message.getRs()+"");


        if (message.getId() + "" != null) {
          */
/*  timestamp = message.getUid() + ", " + timestamp;*//*

            {
                if ((holder).rs.getText().toString().equals("0")) {
                    timestamp = "me" + ", " + timestamp;
                } else {
                    timestamp = other_name + " , " + timestamp;
                }

                (holder).message1.setText(Html.fromHtml(message.getMsg()));
                (holder).timestamp.setText(timestamp);

            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        //   return super.getItemViewType(position);

        NewMessage message = messageArrayList.get(position);
        DBHelper dbHelper = new DBHelper(mContext);
        HashMap<String, String> mobile_data = dbHelper.get_userData();


//        So you send this message-{chatRoomId=11,
//                rid=7878785436,
//                sid=7758975971, flag=1, name=Jack, rtime=31/12/1983,
//                sound=1, title=Catch Me On, vibrate=1,
//                message=Hello Jay}


        if (message.getRs() != 1) {
            return SELF;
        }


        return position;

    }

    @Override
    public int getItemCount() {
        return messageArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView message1, timestamp, rs;


        public ViewHolder(View view) {
            super(view);
            message1 = (TextView) itemView.findViewById(R.id.message);
            timestamp = (TextView) itemView.findViewById(R.id.timestamp);


            rs = (TextView) itemView.findViewById(R.id.rs);

        }
    }

    public static String getTimeStamp(String dateStr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = "";

        today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(dateStr);
            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            String date1 = format.format(date);
            timestamp = date1.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;
    }
}
*/
