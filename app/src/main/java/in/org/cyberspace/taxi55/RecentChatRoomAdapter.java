package in.org.cyberspace.taxi55;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import in.org.cyberspace.taxi55.application.Taxi;
import in.org.cyberspace.taxi55.db.NotiDbHelper;
import in.org.cyberspace.taxi55.model.User;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RecentChatRoomAdapter extends RecyclerView.Adapter<RecentChatRoomAdapter.ViewHolder> {
    Context mCtx;
    List<User> userList;

    public class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        LinearLayout chat_container;
        TextView contact;
        TextView count;
        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name = (TextView) itemView.findViewById(R.id.chat_name);
            this.contact = (TextView) itemView.findViewById(R.id.chat_cont);
            this.count = (TextView) itemView.findViewById(R.id.count);
            this.chat_container = (LinearLayout) itemView.findViewById(R.id.chat_container);
        }
    }

    public List<User> getUserList() {
        return this.userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public RecentChatRoomAdapter(List<User> userList, Context mCtx) {
        this.userList = userList;
        this.mCtx = mCtx;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_row, parent, false));
    }

    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.name.setText(((User) this.userList.get(position)).getName());
        holder.contact.setText(((User) this.userList.get(position)).getContact());
        holder.chat_container.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String mobile = new PrefManager(RecentChatRoomAdapter.this.mCtx).getUserMobile();
                Intent intent = new Intent(RecentChatRoomAdapter.this.mCtx, NewChatActivity.class);
                intent.putExtra(NotiDbHelper.sid, mobile);
                intent.putExtra(NotiDbHelper.rid, holder.contact.getText().toString());
                intent.putExtra("fcm", "null");
                intent.putExtra("uname", holder.name.getText().toString());
                RecentChatRoomAdapter.this.mCtx.startActivity(intent);
            }
        });
        try {
            String a = BuildConfig.FLAVOR;
            int b = 0;
            List<String> messages = Arrays.asList(Taxi.getInstance().getPrefManager().getNotificationsById(holder.contact.getText().toString()).split("\\|"));

            Set<String> unique = new HashSet<String>(messages);
            for (String key :unique) {
                System.out.println(key + ": " + Collections.frequency(messages, key));
                a = key;
                b = Collections.frequency(messages, key);
            }
            if (holder.contact.getText().toString().equals(a)) {
                holder.count.setText(b + BuildConfig.FLAVOR);
                holder.count.setVisibility(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getItemCount() {
        return this.userList.size();
    }
}


/*
package in.org.cyberspace.taxi55;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import in.org.cyberspace.taxi55.application.Taxi;
import in.org.cyberspace.taxi55.model.User;

*/
/**
 * Created by Ashvini on 3/8/2017.
 *//*


public class RecentChatRoomAdapter extends RecyclerView.Adapter<RecentChatRoomAdapter.ViewHolder> {


    List<User> userList;
    Context mCtx;

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public RecentChatRoomAdapter(List<User> userList, Context mCtx) {
        this.userList = userList;
        this.mCtx = mCtx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_row, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.name.setText(userList.get(position).getName());//;
        holder.contact.setText(userList.get(position).getContact());

        holder.chat_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PrefManager prefManager = new PrefManager(mCtx);
                String mobile = prefManager.getUserMobile();
                Intent intent = new Intent(mCtx, NewChatActivity.class);
                intent.putExtra("sid", mobile);
                intent.putExtra("rid", holder.contact.getText().toString());
                intent.putExtra("fcm", "null");
                intent.putExtra("uname", holder.name.getText().toString());
                mCtx.startActivity(intent);


            }
        });


        try {

            String a = "";
            int b = 0;

            String id = holder.contact.getText().toString();

            String oldNotification = Taxi.getInstance().getPrefManager().getNotificationsById(id);
            List<String> messages = Arrays.asList(oldNotification.split("\\|"));

            Set<String> unique = new HashSet<String>(messages);
            for (String key : unique) {
                System.out.println(key + ": " + Collections.frequency(messages, key));
                a = key;
                b = Collections.frequency(messages, key);

            }

            if (holder.contact.getText().toString().equals(a)) {
                holder.count.setText(b + "");
                holder.count.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, contact, count;
        LinearLayout chat_container;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.chat_name);
            contact = (TextView) itemView.findViewById(R.id.chat_cont);
            count = (TextView) itemView.findViewById(R.id.count);
            chat_container = (LinearLayout) itemView.findViewById(R.id.chat_container);

        }
    }

}
*/
