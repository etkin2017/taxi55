package in.org.cyberspace.taxi55.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.util.Log;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.org.cyberspace.taxi55.DBHelper;
import in.org.cyberspace.taxi55.model.ContactNumber;

public class ContactNumberService extends Service {
    private ArrayList<String> mobilelist;
    HashMap<String, String> contactsname;
    String mobile;
    List<ContactNumber> ad;

    public ContactNumberService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);

        contactsname = new HashMap<String, String>();
        mobilelist = new ArrayList<String>();
        ad = new ArrayList<>();
        SharedPreferences sharedPreferences = getSharedPreferences("cont_no", MODE_PRIVATE);
        int cnt = sharedPreferences.getInt("count", 0);
        if (cnt > 0) {
            stopSelf();
            stopService(intent);
        } else {
            newContact();
        }


    }




    public void newContact() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                readPhoneContacts();
                return null;
            }


            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                SharedPreferences sh = getSharedPreferences("cont_no", MODE_PRIVATE);
                SharedPreferences.Editor editor = sh.edit();

                editor.putInt("count", ad.size());
                editor.commit();

                DBHelper dbHandler = new DBHelper(getApplicationContext());
                dbHandler.addMultipleContact(ad);
                stopSelf();
            }
        }.execute();
    }


    public void readPhoneContacts() //This Context parameter is nothing but your Activity class's Context
    {
        Context cntx = getApplicationContext();
        Cursor cursor = cntx.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        Integer contactsCount = cursor.getCount(); // get how many contacts you have in your contacts list
        if (contactsCount > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //the below cursor will give you details for multiple contacts
                    Cursor pCursor = cntx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    // continue till this cursor reaches to all phone numbers which are associated with a contact in the contact list
                    while (pCursor.moveToNext()) {
                        int phoneType = pCursor.getInt(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        //String isStarred 		= pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.STARRED));
                        String phoneNo = pCursor.getString(pCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //you will get all phone numbers according to it's type as below switch case.
                        //Logs.e will print the phone number along with the name in DDMS. you can use these details where ever you want.
                        String m = ((((phoneNo.replaceAll("[\\\\(\\\\)\\\\[\\\\]\\\\{\\\\}]", "")).replace("-", "")).replace(" ", "")).replace("*", "").replace("#", "")).trim().toString();
                        m = m.replaceAll("\\D+", "");
                        Log.e(contactName, m);
                        String numberOnly = phoneNo.replaceAll("[^0-9]", "");

                        if (m.length() > 10) {
                            m = m.substring(Math.max(m.length() - 10, 0));
                        }

                        contactsname.put(m, contactName);
                        mobilelist.add(m);

                        ad.add(new ContactNumber(contactName, m));


                    }
                    pCursor.close();
                }

            }
            cursor.close();
        /*    try {
                SharedPreferences sharedPreferences = getSharedPreferences("PHONECONTACT", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                int no_mobile = mobilelist.size();
                editor.putInt("phone_no", no_mobile);
                editor.commit();

            } catch (Exception e) {
                e.printStackTrace();
            }*/
        }
    }
}
