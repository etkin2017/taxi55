package in.org.cyberspace.taxi55;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy.Builder;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import in.org.cyberspace.taxi55.application.Config;
import in.org.cyberspace.taxi55.application.DAO;
import in.org.cyberspace.taxi55.application.Taxi;
import in.org.cyberspace.taxi55.db.NotiDbHelper;
import in.org.cyberspace.taxi55.model.NewMessage;
import in.org.cyberspace.taxi55.services.NotificationReceiver;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NewChatActivity extends AppCompatActivity {
    public static boolean activate1 = true;
    int REQUEST_CAMERA = 0;
    int REQUEST_TAKE_GALLERY_VIDEO = 100;
    int SELECT_FILE = 1;
    private String TAG = NewChatActivity.class.getSimpleName();
    Dialog a;
    ImageView back_arrow;
    String block = "0";
    MyNewBroadcast broadcast;
    private Button btnSend;
    Calendar c ;//= Calendar.getInstance();
    String cdate;;// = this.df.format(this.c.getTime());
    int chatRoomId;
    TextView chat_room_name;
    Map<String, String> data;
    SimpleDateFormat df;// = new SimpleDateFormat("yyyyMMddHHmmss");
    private Dialog dialog1;
    String fcm = BuildConfig.FLAVOR;
    Uri file_uri;
    int id;
    private EditText inputMessage;
    private ChatRoomThreadAdapter mAdapter;
    NotiDbHelper mHelper;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ArrayList<NewMessage> messageArrayList;
    private RecyclerView recyclerView;
    String rid = BuildConfig.FLAVOR;
    SharedPreferences sh;
    String sid = BuildConfig.FLAVOR;
    int type;
    String uname = BuildConfig.FLAVOR;

    public class MyNewBroadcast extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            String bd = intent.getExtras().getString("data");
            NewChatActivity.this.refresh();
            try {
                Ringtone r = RingtoneManager.getRingtone(NewChatActivity.this.getApplicationContext(), RingtoneManager.getDefaultUri(2));
                ((Vibrator) NewChatActivity.this.getSystemService("vibrator")).vibrate(1000);
                r.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (VERSION.SDK_INT > 9) {
            StrictMode.setThreadPolicy(new Builder().permitAll().build());
        }
        setContentView(R.layout.activity_new_chat);
        c = Calendar.getInstance();
        df = new SimpleDateFormat("yyyyMMddHHmmss");
        cdate = this.df.format(this.c.getTime());

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        this.chat_room_name = (TextView) findViewById(R.id.chat_room_name);
        this.back_arrow = (ImageView) findViewById(R.id.back_arrow);
        this.rid = getIntent().getStringExtra(NotiDbHelper.rid);
        this.sid = getIntent().getStringExtra(NotiDbHelper.sid);
        this.fcm = getIntent().getStringExtra("fcm");
        this.uname = getIntent().getStringExtra("uname");
        this.chat_room_name.setText(this.uname);
        if (this.fcm.equals("null")) {
            getFcm();
        }
        this.back_arrow.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                NewChatActivity.this.onBackPressed();
            }
        });
        this.data = new HashMap();
        this.mHelper = new NotiDbHelper(this);
        this.inputMessage = (EditText) findViewById(R.id.message);
        this.btnSend = (Button) findViewById(R.id.btn_send);
        this.recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        this.mAdapter = new ChatRoomThreadAdapter(this, this.mHelper.getMessageByRid_Sid(this.sid, this.rid), this.uname);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.recyclerView.setAdapter(this.mAdapter);
        this.broadcast = new MyNewBroadcast();
        try {
            if (this.mAdapter.getItemCount() > 1) {
                this.recyclerView.getLayoutManager().smoothScrollToPosition(this.recyclerView, null, this.mAdapter.getItemCount() + 1);
            }
        } catch (Exception ew) {
            ew.printStackTrace();
        }
        try {
            IntentFilter filter = new IntentFilter(NotificationReceiver.name);
            filter.addCategory("android.intent.category.DEFAULT");
            registerReceiver(this.broadcast, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        activate1 = true;
        this.btnSend.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String message = NewChatActivity.this.inputMessage.getText().toString().trim();
                if (TextUtils.isEmpty(message)) {
                    Toast.makeText(NewChatActivity.this.getApplicationContext(), "Enter a message", 0).show();
                } else {
                    NewChatActivity.this.sendMessage(message);
                }
            }
        });
    }

    protected void onResume() {
        super.onResume();
        try {
            Log.e("RDI", this.rid);
            Taxi.getInstance().getPrefManager().removeNotificaion(this.rid);
        } catch (Exception er1) {
            er1.printStackTrace();
        }
    }

    private void sendMessage(String message) {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        String str = message;
        NewMessage m = new NewMessage(0, new PrefManager(this).getUserMobile(), this.rid, str, timeStamp + BuildConfig.FLAVOR, timeStamp + BuildConfig.FLAVOR, this.chatRoomId, 0, new PrefManager(this).getUsername());
        this.mHelper.saveSingleMessage(m);
        this.inputMessage.setText(BuildConfig.FLAVOR);
        refresh();
        if (this.mAdapter.getItemCount() > 1) {
            this.recyclerView.getLayoutManager().smoothScrollToPosition(this.recyclerView, null, this.mAdapter.getItemCount() + 1);
        }
        this.mAdapter.notifyDataSetChanged();
        sendMsgToServer(m);
    }

    public void getFcm() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mobile_no", this.rid);
            jsonObject.put(NotiDbHelper.mtypr, "driver");
            Log.i("data", jsonObject.toString());
        } catch (Exception ew) {
            ew.printStackTrace();
        }
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "get_fcm_from_phone", jsonObject, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                Log.i("Response", response.toString());
                progressDialog.dismiss();
                try {
                    if (response.getString("status").equals("success")) {
                        JSONArray jsonObject2 = response.getJSONArray("key");
                        NewChatActivity.this.fcm = jsonObject2.getJSONObject(0).getString("fcm");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(NewChatActivity.this, "User is offline", 0).show();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                Log.i("Error", error.toString());
                error.printStackTrace();
                progressDialog.dismiss();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "ConfirmBookingRequest");
    }

    private void sendMsgToServer(NewMessage m) {
        final String msg1 = m.getMsg();
        final String sid1 = m.getSid() + BuildConfig.FLAVOR;
        final String rid1 = m.getRid() + BuildConfig.FLAVOR;
        final String rtime1 = m.getRtime();
        String uid1 = m.getUid() + BuildConfig.FLAVOR;
        new AsyncTask() {
            JSONObject res = null;

            protected Object doInBackground(Object[] params) {
                String un = new PrefManager(NewChatActivity.this).getUsername();
                NewChatActivity.this.data.put(NotiDbHelper.msg, msg1);
                NewChatActivity.this.data.put(NotiDbHelper.sid, sid1);
                NewChatActivity.this.data.put(NotiDbHelper.rid, rid1);
                NewChatActivity.this.data.put(NotiDbHelper.rtime, rtime1 + BuildConfig.FLAVOR);
                NewChatActivity.this.data.put("title", "Taxi55 - Driver App");
                NewChatActivity.this.data.put("chatroomid", "11");
                NewChatActivity.this.data.put("name", un);
                NewChatActivity.this.data.put("gcm_reg_id", NewChatActivity.this.fcm);
                this.res = DAO.getJsonFromUrl(Config.INSERT_MSG, NewChatActivity.this.data);
                return null;
            }

            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try {
                    Log.e("RES1", this.res.toString());
                    if (this.res.getInt("success") <= 0) {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(new Object[]{null, null, null});
    }

    private void refresh() {
        this.mAdapter.setMessageArrayList(this.mHelper.getMessageByRid_Sid(this.sid, this.rid));
        if (this.mAdapter.getItemCount() > 1) {
            this.recyclerView.getLayoutManager().smoothScrollToPosition(this.recyclerView, null, this.mAdapter.getItemCount() + 1);
        }
        this.mAdapter.notifyDataSetChanged();
    }
}

/*
package in.org.cyberspace.taxi55;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.org.cyberspace.taxi55.application.Config;
import in.org.cyberspace.taxi55.application.DAO;
import in.org.cyberspace.taxi55.application.MyPreferenceManager;
import in.org.cyberspace.taxi55.db.NotiDbHelper;
import in.org.cyberspace.taxi55.model.NewMessage;
import in.org.cyberspace.taxi55.services.NotificationReceiver;


public class NewChatActivity extends AppCompatActivity {

    private String TAG = NewChatActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private ChatRoomThreadAdapter mAdapter;
    private ArrayList<NewMessage> messageArrayList;


    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private EditText inputMessage;
    private Button btnSend;
    NotiDbHelper mHelper;
    SharedPreferences sh;
    int id;
    int chatRoomId, type;
    Map<String, String> data;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    int REQUEST_TAKE_GALLERY_VIDEO = 100;
    Uri file_uri;


    Dialog a;
    private Dialog dialog1;

    public static boolean activate1 = true;
    Calendar c = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
    String cdate = df.format(c.getTime());

    TextView chat_room_name;

    String block = "0";
    String rid = "", sid = "", fcm = "", uname = "";
    MyNewBroadcast broadcast;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        setContentView(R.layout.activity_new_chat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        chat_room_name = (TextView) findViewById(R.id.chat_room_name);

        rid = getIntent().getStringExtra("rid");
        sid = getIntent().getStringExtra("sid");
        fcm = getIntent().getStringExtra("fcm");
        uname = getIntent().getStringExtra("uname");


        chat_room_name.setText(uname);

        if(fcm.equals("null")){
            getFcm();
        }


        data = new HashMap<String, String>();
        mHelper = new NotiDbHelper(NewChatActivity.this);
        inputMessage = (EditText) findViewById(R.id.message);
        btnSend = (Button) findViewById(R.id.btn_send);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new ChatRoomThreadAdapter(this, mHelper.getMessageByRid_Sid(sid, rid), uname);
        recyclerView.setLayoutManager(new LinearLayoutManager(NewChatActivity.this));
        recyclerView.setAdapter(mAdapter);
        // broadcast = new MyNewBroadcast();

        try {
            if (mAdapter.getItemCount() > 1) {
                recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() + 1);
            }
        } catch (Exception ew) {
            ew.printStackTrace();
        }
       */
/*
        try {
            IntentFilter filter = new IntentFilter(NotificationReceiver.name);
            filter.addCategory(Intent.CATEGORY_DEFAULT);
            registerReceiver(broadcast, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }*//*

        activate1 = true;

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String message = inputMessage.getText().toString().trim();

                if (TextUtils.isEmpty(message)) {
                    Toast.makeText(getApplicationContext(), "Enter a message", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    sendMessage(message);
                }

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            MyPreferenceManager myPreferenceManager = new MyPreferenceManager(NewChatActivity.this);
            myPreferenceManager.clear();

        } catch (Exception er1) {
            er1.printStackTrace();
        }
    }

    private void sendMessage(String message) {
        // NewMessage m;
        NewMessage m;
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        PrefManager preferenceManager = new PrefManager(NewChatActivity.this);
        String mobile_data = preferenceManager.getUserMobile();
        PrefManager my = new PrefManager(NewChatActivity.this);
        String un = my.getUsername();
        m = new NewMessage(0, mobile_data, rid, message, timeStamp + "", timeStamp + "", chatRoomId, 0,un);
        //Log.e("LastMsg1", m.toString());

        mHelper.saveSingleMessage(m);
        inputMessage.setText("");
        refresh();
        // mAdapter.notifyDataSetChanged();

        //     mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());

        if (mAdapter.getItemCount() > 1) {
            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() + 1);
        }

        mAdapter.notifyDataSetChanged();
        sendMsgToServer(m);


        // refresh();


    }


    public void getFcm()
    {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("mobile_no", rid);
            jsonObject.put("type","driver");


            Log.i("data", jsonObject.toString());
        }catch (Exception ew){
            ew.printStackTrace();
        }
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "get_fcm_from_phone", jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("Response",response.toString());
                        progressDialog.dismiss();
                        try {
                            if(response.getString("status").equals("success"))
                            {
                                 fcm=response.getString("key");


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(NewChatActivity.this,"User is offline",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error",error.toString());
                error.printStackTrace();
                progressDialog.dismiss();
            }
        }
        );
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest,"ConfirmBookingRequest");
    }

    private void sendMsgToServer(NewMessage m) {

        final String msg1 = m.getMsg();
        final String sid1 = m.getSid() + "";
        final String rid1 = m.getRid() + "";
        //Log.e("RID", rid1);
        final String rtime1 = m.getRtime();
        String uid1 = m.getUid() + "";


        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected Object doInBackground(Object[] params) {

                PrefManager my = new PrefManager(NewChatActivity.this);
                String un = my.getUsername();


                data.put("message", msg1);
                data.put("sid", sid1);
                data.put("rid", rid1);
                data.put("rtime", rtime1 + "");//rtime
                data.put("title", "Taxi55 - Driver App");
                data.put("chatroomid", "11");
                data.put("name", un);
                data.put("gcm_reg_id", fcm);


                res = DAO.getJsonFromUrl(Config.INSERT_MSG, data);


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //   inputMessage.setText("");

                try {


                    Log.e("RES1", res.toString());
                    if (res.getInt("success") > 0) {
                        //       Toast.makeText(getApplicationContext(), "message sent", Toast.LENGTH_SHORT).show();

                    } else {
                        //     Toast.makeText(getApplicationContext(), "message failed", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }


    private void refresh() {

        mAdapter.setMessageArrayList(mHelper.getMessageByRid_Sid(sid, rid));


        //   mAdapter.notifyDataSetChanged();

        //    mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());
        if (mAdapter.getItemCount() > 1) {
            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() + 1);
        }
        mAdapter.notifyDataSetChanged();

        //     mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());


    }


    public class MyNewBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String bd = intent.getExtras().getString("data");
            refresh();
            //Log.e("badge_data", bd);


            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds

                v.vibrate(1000);
                r.play();
*/
/*
                JSONObject j = new JSONObject(DAO.getNotification());
                JSONArray ar = new JSONArray();
                ar.put(bd.getString("msg"));
                ar.put(bd.getString("title"));
                ar.put(bd.getString("link"));
                ar.put(true);
                j.put(bd.getString("key"), ar);
                DAO.storeNotification(j.toString());*//*



                //       Log.e("badge_data1", badge_data + "");


                // }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }
}
*/
