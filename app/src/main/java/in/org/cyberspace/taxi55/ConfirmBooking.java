package in.org.cyberspace.taxi55;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import in.org.cyberspace.taxi55.application.Config;
import in.org.cyberspace.taxi55.application.DAO;
import in.org.cyberspace.taxi55.db.NotiDbHelper;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ConfirmBooking extends AppCompatActivity {
    final String[] Payment_Methods = new String[]{"By Hand"};
    HashMap<String, String> data;
    String lat;
    String lng;
    ImageView imgsuccess;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_booking);
        this.data = (HashMap) getIntent().getExtras().get("DATA");
        ((TextView) findViewById(R.id.amtToPay)).setText("Amount  : " + ((String) this.data.get("amount")).toString().replace("Fare", BuildConfig.FLAVOR).replace(":", BuildConfig.FLAVOR));
        this.lat = ((String) this.data.get("pickup_lat")).toString();
        this.lng = ((String) this.data.get("pickup_lng")).toString();
        this.data.put("amount", ((String) this.data.get("amount")).toString().replace("Fare", BuildConfig.FLAVOR).replace(":", BuildConfig.FLAVOR));
        Spinner spnr = (Spinner) findViewById(R.id.spinner);
        imgsuccess = (ImageView) findViewById(R.id.img_success);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, this.Payment_Methods);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnr.setAdapter(arrayAdapter);
    }

    public void bookNow(View v) {
        Builder alert = new Builder(this);
        alert.setIcon(R.drawable.icon);
        alert.setTitle("Confirm Booking");
        alert.setMessage("Are You Sure To Book ?");
        alert.setPositiveButton("Yes", new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                /*if (ConfirmBooking.this.data != null) {
                    ConfirmBooking.this.book(ConfirmBooking.this.data);
                } else {
                    Toast.makeText(ConfirmBooking.this, "No Data Found", Toast.LENGTH_SHORT).show();
                }*/
                checkBookingCount();
            }
        });
        alert.setNegativeButton("No", new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        alert.show();
    }

    public void checkBookingCount() {

        JSONObject jsonObject = new JSONObject();
        PrefManager manager = new PrefManager(ConfirmBooking.this);
        try {
            jsonObject.put("user_name", manager.getUsername());
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "user_booking_count", jsonObject, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                Log.i("Response", response.toString());
                progressDialog.dismiss();
                try {

                    int count = Integer.parseInt(response.getString("status"));
                    if (count >= 3) {

                        Builder alert = new Builder(ConfirmBooking.this);
                        alert.setIcon(R.drawable.icon);
                        alert.setTitle("About Rides");
                        alert.setMessage("You have already booked three rides. You can not book another ride.");
                        alert.setPositiveButton("Ok", new OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                finish();
                                startActivity(new Intent(ConfirmBooking.this,LocaFromMapActivity.class));
                            }
                        });

                        alert.show();


                    } else {


                        if (ConfirmBooking.this.data != null) {
                            book(ConfirmBooking.this.data);
                        } else {
                            Toast.makeText(ConfirmBooking.this, "No Data Found", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ConfirmBooking.this, "Please Try Again", Toast.LENGTH_LONG).show();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                Log.i("Error", error.toString());
                error.printStackTrace();
                progressDialog.dismiss();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "ConfirmBookingRequest");


    }

    public void book(HashMap<String, String> data) {
        JSONObject jsonObject = new JSONObject(data);
        Log.i("data", jsonObject.toString());
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "book_cab", jsonObject, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                Log.i("Response", response.toString());
                progressDialog.dismiss();
                try {
                    if (response.getString("status").equals("success")) {
                        ConfirmBooking.this.sendNotiToNearestDriver(ConfirmBooking.this.lat, ConfirmBooking.this.lng, response.getString("uneaque_id"));
                      /* imgsuccess.setVisibility(View.VISIBLE);
                        Toast.makeText(ConfirmBooking.this, "Ride Book Successfull", Toast.LENGTH_LONG).show();
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                finish();
                                startActivity(new Intent(ConfirmBooking.this, MyRides.class));
                            }
                        },1000);*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ConfirmBooking.this, "Please Try Again", Toast.LENGTH_LONG).show();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                Log.i("Error", error.toString());
                error.printStackTrace();
                progressDialog.dismiss();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "ConfirmBookingRequest");
    }

    public void sendNotiToNearestDriver(String lat, String lng, final String booking_id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("lat", lat);
            jsonObject.put("long", lng);
            jsonObject.put("booking_id", booking_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("data", jsonObject.toString());
        final ProgressDialog pdlg = new ProgressDialog(this);
        pdlg.setMessage("Please Wait...");
        pdlg.show();
        pdlg.setCancelable(false);
        String REQUEST_TAG = getPackageName() + ".volleyJsonArrayRequest";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "get_driver_details", jsonObject, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                Log.i("Response", response.toString());
                pdlg.dismiss();
                try {
                    if (response.getString("status").equals("success")) {
                        JSONArray jsonArray = response.getJSONArray("driver_details");
                        String[] FCMS = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            FCMS[i] = jsonArray.getJSONObject(i).getString("fcm");
                        }
                        ConfirmBooking.this.sendNotification(booking_id, FCMS.length, FCMS);
                        return;
                    }
                    Toast.makeText(ConfirmBooking.this, response.getString(NotiDbHelper.msg), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.i("Response", error.toString());
                pdlg.dismiss();
                Toast.makeText(ConfirmBooking.this, "Please Try Again", Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, REQUEST_TAG);


    }

    public void sendNotification(final String booking_ID, final int len, final String[] fcm) {

        final ProgressDialog pdlg = new ProgressDialog(ConfirmBooking.this);
        pdlg.setMessage("Please Wait...");
        pdlg.setCancelable(false);

        new AsyncTask() {
            JSONObject[] json = new JSONObject[len];

            protected void onPreExecute() {
                super.onPreExecute();
                pdlg.show();
            }

            protected Object doInBackground(Object[] params) {
                try {

                    for (int i = 0; i < len; i++)
                        try {
                            String un = new PrefManager(ConfirmBooking.this).getUsername();
                            ConfirmBooking.this.data.put(NotiDbHelper.msg, "Hello, I want to go \nfrom : " + ((String) ConfirmBooking.this.data.get("pickup_area")) + "\nto : " + ((String) ConfirmBooking.this.data.get("drop_area")) + " \n on the date : " + ((String) ConfirmBooking.this.data.get("travel_date")).toString() + "\n at time : " + ((String) ConfirmBooking.this.data.get("travel_time")).toString() + "\nAre You Available ?");
                            ConfirmBooking.this.data.put("lat", ((String) ConfirmBooking.this.data.get("pickup_lat")).toString());
                            ConfirmBooking.this.data.put("lng", ((String) ConfirmBooking.this.data.get("pickup_lng")).toString());
                            ConfirmBooking.this.data.put("booking_id", booking_ID + BuildConfig.FLAVOR);
                            ConfirmBooking.this.data.put("date", ((String) ConfirmBooking.this.data.get("book_date")).toString());
                            ConfirmBooking.this.data.put("drop_location", ((String) ConfirmBooking.this.data.get("drop_area")).toString());
                            ConfirmBooking.this.data.put("chatroomid", "from_user");
                            ConfirmBooking.this.data.put("name", un);
                            ConfirmBooking.this.data.put("drop_lat", ((String) ConfirmBooking.this.data.get("drop_lat")).toString());
                            ConfirmBooking.this.data.put("drop_lng", ((String) ConfirmBooking.this.data.get("drop_lng")).toString());
                            ConfirmBooking.this.data.put("dist", ((String) ConfirmBooking.this.data.get("km")).toString());
                            ConfirmBooking.this.data.put("time", ((String) ConfirmBooking.this.data.get("travel_time")).toString());
                            ConfirmBooking.this.data.put("amount", ((String) ConfirmBooking.this.data.get("amount")).toString());
                            ConfirmBooking.this.data.put("gcm_reg_id", fcm[i]);
                            this.json[i] = DAO.getJsonFromUrl(Config.SEND_NOTIFICATION_TO_DRIVER, ConfirmBooking.this.data);
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                    // return this.json[0];
                    // }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return this.json;
            }

            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                pdlg.dismiss();

                int i = 0;
                if (len > 0) {
                    while (i < len) {
                        try {
                            try {
                                Log.e("Res", o.toString());
                                Toast.makeText(ConfirmBooking.this, "Notification sent", Toast.LENGTH_LONG).show();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                            if (i == len - 1) {
                                Intent in = new Intent(ConfirmBooking.this, Success.class);
                                in.putExtra("from", (String) ConfirmBooking.this.data.get("from"));
                                in.putExtra("ID", booking_ID);
                                ConfirmBooking.this.startActivity(in);
                                ConfirmBooking.this.finish();
                            }
                            i++;
                        } catch (Exception ed) {
                            ed.printStackTrace();
                            return;
                        }
                    }
                } else {
                    Intent in = new Intent(ConfirmBooking.this, Success.class);
                    in.putExtra("from", (String) ConfirmBooking.this.data.get("from"));
                    in.putExtra("ID", booking_ID);
                    ConfirmBooking.this.startActivity(in);
                    ConfirmBooking.this.finish();
                }
            }
        }.execute(new Object[]{null, null, null});
    }
}


/*
package in.org.cyberspace.taxi55;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ConfirmBooking extends AppCompatActivity {

    final String [] Payment_Methods={"By Hand"};
    HashMap<String,String> data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_booking);

        Bundle bundle =getIntent().getExtras();
        data =(HashMap<String,String> )bundle.get("DATA");

        TextView tvAmount=(TextView)findViewById(R.id.amtToPay);
        tvAmount.setText("Amount To Pay : "+data.get("amount").toString());

        Spinner spnr = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,Payment_Methods);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnr.setAdapter(arrayAdapter);
        //book(data);
    }

    public void bookNow(View v)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(ConfirmBooking.this);
        alert.setIcon(R.drawable.icon);
        alert.setTitle("Confirm Booking");
        alert.setMessage("Are You Sure To Book ?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(data!=null)
                {
                    book(data);
                }
                else
                {
                    Toast.makeText(ConfirmBooking.this,"No Data Found",Toast.LENGTH_SHORT).show();
                }
            }
        });

        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                return;
            }
        });

        alert.show();
    }

    public void book(HashMap<String,String> data)
    {

        JSONObject jsonObject = new JSONObject(data);
        Log.i("data",jsonObject.toString());

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "book_cab", jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("Response",response.toString());
                        progressDialog.dismiss();
                        try {
                            if(response.getString("status").equals("success"))
                            {
                                String BookingID=response.getString("uneaque_id");

                                Intent i = new Intent(ConfirmBooking.this,Success.class);
                                i.putExtra("ID",BookingID);
                                startActivity(i);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(ConfirmBooking.this,"Please Try Again",Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error",error.toString());
                error.printStackTrace();
                progressDialog.dismiss();
            }
        }
        );
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest,"ConfirmBookingRequest");
    }
}
*/
