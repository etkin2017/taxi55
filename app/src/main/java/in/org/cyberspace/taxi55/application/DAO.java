package in.org.cyberspace.taxi55.application;

/**
 * Created by Ashvini on 1/23/2017.
 */

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Rajeshwar on 15-02-2016.
 */
public class DAO {

    static InputStream iStream = null;
    static JSONArray jarray = null;
    static String json = "";


    public static JSONObject getJsonFromUrl(String Targeturl, Map<String, String> paramdata) {
        URL url;
        HttpURLConnection connection = null;
        JSONObject error = null;
        try {
            error = new JSONObject("{\"success\":-1}");
            String parms = "";
            Set mapset = paramdata.entrySet();
            Iterator mapitrator = mapset.iterator();
            while (mapitrator.hasNext()) {
                Map.Entry ent = (Map.Entry) mapitrator.next();
                parms += ent.getKey().toString() + "=";
                parms += URLEncoder.encode(ent.getValue().toString(), "UTF-8") + "&";
            }
            parms = parms.substring(0, parms.length() - 1);
            url = new URL(Targeturl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" + Integer.toString(parms.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            //Send request
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(parms);
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();
            is.close();
            return new JSONObject(response.toString());
            //return response.toString();


        } catch (Exception e) {
            e.printStackTrace();

        } finally {

            try {
                connection.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return error;
    }


    public static JSONArray getJsonArrayFromUrl(String Targeturl, Map<String, String> paramdata) {
        URL url;
        HttpURLConnection connection = null;
        JSONArray error = null;
        try {
            error = new JSONArray("[{\"success\":-1}]");
            String parms = "";
            Set mapset = paramdata.entrySet();
            Iterator mapitrator = mapset.iterator();
            while (mapitrator.hasNext()) {
                Map.Entry ent = (Map.Entry) mapitrator.next();
                parms += ent.getKey().toString() + "=";
                parms += URLEncoder.encode(ent.getValue().toString(), "UTF-8") + "&";
            }
            parms = parms.substring(0, parms.length() - 1);
            url = new URL(Targeturl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" + Integer.toString(parms.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            //Send request
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(parms);
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();
            is.close();
            return new JSONArray(response.toString());
            //return response.toString();


        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            connection.disconnect();
        }

        return error;
    }


    public static JSONArray getJSONFromUrl1(String url) {
        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            } else {
                Log.e("==>", "Failed to download file");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            String b1 = builder.toString();
            b1 = b1.substring(1);
            b1 = b1.substring(0, b1.length() - 1);
            jarray = new JSONArray(b1);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        } // return JSON Object return jarray;


        return jarray;
    }

    public static JSONObject getLocationInfo(double lat, double lng) {

        HttpGet httpGet = new HttpGet("http://maps.google.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&sensor=false");
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    public static JSONObject getResult(String url) {

        HttpGet httpGet = new HttpGet(url);
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static String getStringFromUrl(String Targeturl, Map<String, String> paramdata) {
        URL url;
        HttpURLConnection connection = null;
        String error = null;
        try {
            error = "success:-1";
            String parms = "";
            Set mapset = paramdata.entrySet();
            Iterator mapitrator = mapset.iterator();
            while (mapitrator.hasNext()) {
                Map.Entry ent = (Map.Entry) mapitrator.next();
                parms += ent.getKey().toString() + "=";
                parms += URLEncoder.encode(ent.getValue().toString(), "UTF-8") + "&";
            }
            parms = parms.substring(0, parms.length() - 1);
            url = new URL(Targeturl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" + Integer.toString(parms.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            //Send request
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(parms);
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();
            is.close();
            return (response.toString());
            //return response.toString();


        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            connection.disconnect();
        }

        return error;
    }


    public static String getStringFromUrlUsingDelete(String Targeturl, Map<String, String> paramdata) {
        URL url;
        HttpURLConnection connection = null;
        String error = null;
        try {
            error = "success:-1";
            String parms = "";
            Set mapset = paramdata.entrySet();
            Iterator mapitrator = mapset.iterator();
            while (mapitrator.hasNext()) {
                Map.Entry ent = (Map.Entry) mapitrator.next();
                parms += ent.getKey().toString() + "=";
                parms += URLEncoder.encode(ent.getValue().toString(), "UTF-8") + "&";
            }
            parms = parms.substring(0, parms.length() - 1);
            url = new URL(Targeturl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("DELETE");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" + Integer.toString(parms.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            //Send request
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(parms);
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();
            is.close();
            return (response.toString());
            //return response.toString();


        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            connection.disconnect();
        }

        return error;
    }


}
