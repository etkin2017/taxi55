package in.org.cyberspace.taxi55;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import in.org.cyberspace.taxi55.application.Taxi;
import in.org.cyberspace.taxi55.db.NotiDbHelper;
import in.org.cyberspace.taxi55.model.User;
import java.util.ArrayList;
import java.util.List;

public class ChatActivity extends AppCompatActivity {
    ImageView back_arrow;
    RecyclerView contrecy;
    RecentChatRoomAdapter mAdaptor;
    NotiDbHelper mHelper;
    List<User> userList;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        this.mHelper = new NotiDbHelper(Taxi.getInstance());
        this.contrecy = (RecyclerView) findViewById(R.id.contrecyc);
        this.back_arrow = (ImageView) findViewById(R.id.back_arrow);
        this.back_arrow.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ChatActivity.this.onBackPressed();
            }
        });
    }

    protected void onResume() {
        super.onResume();
        this.userList = this.mHelper.getUserByChat();
        this.mAdaptor = new RecentChatRoomAdapter(this.userList, this);
        this.contrecy.setLayoutManager(new LinearLayoutManager(this));
        this.contrecy.setAdapter(this.mAdaptor);
    }

    public void refresh() {
        ArrayList<User> ur = this.mHelper.getUserByChat();
        this.mAdaptor = new RecentChatRoomAdapter(ur, Taxi.getInstance());
        this.mAdaptor.setUserList(ur);
        this.contrecy.setAdapter(this.mAdaptor);
        this.mAdaptor.notifyDataSetChanged();
    }
}


/*
package in.org.cyberspace.taxi55;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.org.cyberspace.taxi55.application.Taxi;
import in.org.cyberspace.taxi55.db.NotiDbHelper;
import in.org.cyberspace.taxi55.model.User;

public class ChatActivity extends AppCompatActivity {

    List<User> userList;

    NotiDbHelper mHelper;
    RecyclerView contrecy;
    RecentChatRoomAdapter mAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        mHelper = new NotiDbHelper(Taxi.getInstance());
        contrecy = (RecyclerView) findViewById(R.id.contrecyc);
        userList = mHelper.getUserByChat();
        mAdaptor = new RecentChatRoomAdapter(userList, ChatActivity.this);
        contrecy.setLayoutManager(new LinearLayoutManager(ChatActivity.this));
        contrecy.setAdapter(mAdaptor);
    }

    public void refresh() {


        ArrayList<User> ur = mHelper.getUserByChat();
        mAdaptor = new RecentChatRoomAdapter(ur, Taxi.getInstance());

        mAdaptor.setUserList(ur);
        contrecy.setAdapter(mAdaptor);
        mAdaptor.notifyDataSetChanged();
    }
}
*/
