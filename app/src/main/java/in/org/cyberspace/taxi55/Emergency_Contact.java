package in.org.cyberspace.taxi55;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import in.org.cyberspace.taxi55.db.NotiDbHelper;
import in.org.cyberspace.taxi55.model.EmergencyContactsPojo;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Emergency_Contact extends AppCompatActivity {
    Button add_con_tbn;
    AppBarLayout appBarLayout;
    ImageView back_arrow;
    LinearLayout contact_container;
    RecyclerView contact_list_recycler;
    Toolbar mtoolbar;
    String passId = BuildConfig.FLAVOR;
    List<EmergencyContactsPojo> passengerEmergencyContactsList;

    private class EmergencyContactListAdapter extends RecyclerView.Adapter<EmergencyContactListAdapter.ViewHolder> {
        Context ctx;
        List<EmergencyContactsPojo> passengerEmergencyContactsList;

        class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
            TextView cont_id;
            ImageView cont_menu;
            TextView cont_nm;
            TextView cont_number;
            ImageView emer_icon;

            public ViewHolder(View itemView) {
                super(itemView);
                this.cont_id = (TextView) itemView.findViewById(R.id.cont_id);
                this.cont_number = (TextView) itemView.findViewById(R.id.cont_number);
                this.cont_nm = (TextView) itemView.findViewById(R.id.cont_nm);
                this.cont_menu = (ImageView) itemView.findViewById(R.id.cont_menu);
                this.emer_icon = (ImageView) itemView.findViewById(R.id.emer_icon);

            }
        }

        public EmergencyContactListAdapter(Context ctx, List<EmergencyContactsPojo> passengerEmergencyContactsList) {
            this.ctx = ctx;
            this.passengerEmergencyContactsList = passengerEmergencyContactsList;
        }

        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_emergency_contact, parent, false));
        }

        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.cont_nm.setText(((EmergencyContactsPojo) this.passengerEmergencyContactsList.get(position)).getContactname());
            holder.cont_id.setVisibility(View.GONE);
            holder.cont_id.setText(((EmergencyContactsPojo) this.passengerEmergencyContactsList.get(position)).getId() + BuildConfig.FLAVOR);
            holder.cont_number.setText(((EmergencyContactsPojo) this.passengerEmergencyContactsList.get(position)).getContactnumber());


            holder.emer_icon.setColorFilter(getResources().getColor(R.color.sky_blue));
            holder.cont_menu.setColorFilter(getResources().getColor(R.color.sky_blue));
            holder.cont_menu.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    Builder alertDialogBuilder = new Builder(Emergency_Contact.this);
                    alertDialogBuilder.setMessage("Do you want to delete this contact ?");
                    alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            Emergency_Contact.this.deleteContact(ConnectionConfiguration.URL + "delete_emergency_contact", holder.cont_id.getText().toString());
                        }
                    });
                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialogBuilder.create().show();
                }
            });
        }

        public int getItemCount() {
            return this.passengerEmergencyContactsList.size();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency__contact);
        this.back_arrow = (ImageView) findViewById(R.id.back_arrow);
        this.back_arrow.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Emergency_Contact.this.onBackPressed();
            }
        });
        this.contact_list_recycler = (RecyclerView) findViewById(R.id.contact_list_recycler);
        this.passengerEmergencyContactsList = new ArrayList();
        this.contact_list_recycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        this.contact_container = (LinearLayout) findViewById(R.id.contact_container);
        this.add_con_tbn = (Button) findViewById(R.id.add_con_btn);
        this.add_con_tbn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Emergency_Contact.this.startActivity(new Intent(Emergency_Contact.this, EmergencyContactActivity.class));
            }
        });
        fetchCOntacts();
    }

    public void deleteContact(String url, String id) {
        final ProgressDialog pdlg = new ProgressDialog(this);
        pdlg.setMessage("Please Wait...");
        pdlg.show();
        pdlg.setCancelable(false);
        String REQUEST_TAG = getPackageName() + ".volleyJsonArrayRequest";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(NotiDbHelper.mid, id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, jsonObject, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                Log.e("Response", response.toString());
                pdlg.dismiss();
                try {
                    if (response.getString("status").equals("success")) {
                        Emergency_Contact.this.startActivity(new Intent(Emergency_Contact.this, Emergency_Contact.class));
                        Emergency_Contact.this.finish();
                        return;
                    }
                    Toast.makeText(Emergency_Contact.this, response.getString(NotiDbHelper.msg), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("Response", error.toString());
                pdlg.dismiss();
                Toast.makeText(Emergency_Contact.this, "Please Try Again", Toast.LENGTH_SHORT).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, REQUEST_TAG);
    }

    public void fetchCOntacts() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait...");
        dialog.show();
        String Did = new PrefManager(this).getUSER_Id();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", Did);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "all_emergency_contact", jsonObject, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                Log.e("PassengerResponse", response.toString());
                try {
                    JSONArray jsonArray = response.getJSONArray("contacts");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonobject = jsonArray.getJSONObject(i);
                        Emergency_Contact.this.passengerEmergencyContactsList.add(new EmergencyContactsPojo(Integer.valueOf(jsonobject.getInt(NotiDbHelper.mid)), Integer.valueOf(jsonobject.getInt("user_id")), jsonobject.getString("mob_no"), jsonobject.getString("name")));
                    }
                    if (Emergency_Contact.this.passengerEmergencyContactsList.size() >= 5) {
                        Emergency_Contact.this.contact_container.setVisibility(View.GONE);
                    } else {
                        Emergency_Contact.this.contact_container.setVisibility(View.VISIBLE);
                    }
                    Emergency_Contact.this.contact_list_recycler.setAdapter(new EmergencyContactListAdapter(Emergency_Contact.this, Emergency_Contact.this.passengerEmergencyContactsList));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.printStackTrace();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }
}


/*
package in.org.cyberspace.taxi55;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.org.cyberspace.taxi55.model.EmergencyContactsPojo;

public class Emergency_Contact extends AppCompatActivity {

    LinearLayout contact_container;
    Button add_con_tbn;
    RecyclerView contact_list_recycler;

    List<EmergencyContactsPojo> passengerEmergencyContactsList;
    String passId = "";

    Toolbar mtoolbar;
    AppBarLayout appBarLayout;

    ImageView back_arrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency__contact);

        back_arrow =(ImageView) findViewById(R.id.back_arrow);

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        contact_list_recycler = (RecyclerView) findViewById(R.id.contact_list_recycler);
        passengerEmergencyContactsList = new ArrayList<>();
        contact_list_recycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        contact_container = (LinearLayout) findViewById(R.id.contact_container);
        add_con_tbn = (Button) findViewById(R.id.add_con_btn);

        add_con_tbn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Emergency_Contact.this, EmergencyContactActivity.class));
            }
        });

        fetchCOntacts();
    }


    public void deleteContact(final String url, final String id) {
        final ProgressDialog pdlg = new ProgressDialog(Emergency_Contact.this);
        pdlg.setMessage("Please Wait...");
        pdlg.show();
        pdlg.setCancelable(false);
        String REQUEST_TAG = getPackageName() + ".volleyJsonArrayRequest";
        JSONObject jsonObject = new JSONObject();
        try {
            //  jsonObject.put("secret_key", ConnectionConfiguration.SECRET_KEY);
            jsonObject.put("id", id);

            //  jsonObject.put("Mobile", mobile);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //String url = "http://uncletaxi.sarodestudio.photography/web_service/emergency_contact";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Response", response.toString());
                pdlg.dismiss();
                try {
                    if (response.getString("status").equals("success")) {
                        startActivity(new Intent(Emergency_Contact.this, Emergency_Contact.class));
                        finish();
                    } else {
                        Toast.makeText(Emergency_Contact.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.e("Response", error.toString());
                        pdlg.dismiss();
                        Toast.makeText(Emergency_Contact.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                    }
                });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, REQUEST_TAG);
    }


    public void fetchCOntacts() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait...");
        dialog.show();
        PrefManager prefManager = new PrefManager(Emergency_Contact.this);
        String Did = prefManager.getUSER_Id();

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", Did);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String Url = "http://uncletaxi.sarodestudio.photography/web_service/all_emergency_contact";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                Log.e("PassengerResponse", response.toString());
                try {
                    JSONArray jsonArray = response.getJSONArray("contacts");


                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonobject = jsonArray.getJSONObject(i);
                        passengerEmergencyContactsList.add(new EmergencyContactsPojo(jsonobject.getInt("id"),
                                jsonobject.getInt("user_id"), jsonobject.getString("mob_no"),
                                jsonobject.getString("name")));
                    }
                    if (passengerEmergencyContactsList.size() >= 5) {
                        contact_container.setVisibility(View.GONE);
                    } else {
                        contact_container.setVisibility(View.VISIBLE);
                    }


                    EmergencyContactListAdapter adapter = new EmergencyContactListAdapter(Emergency_Contact.this, passengerEmergencyContactsList);
                    contact_list_recycler.setAdapter(adapter);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.printStackTrace();
            }
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }


    private class EmergencyContactListAdapter extends RecyclerView.Adapter<EmergencyContactListAdapter.ViewHolder> {

        List<EmergencyContactsPojo> passengerEmergencyContactsList;
        Context ctx;

        public EmergencyContactListAdapter(Context ctx, List<EmergencyContactsPojo> passengerEmergencyContactsList) {

            this.ctx = ctx;
            this.passengerEmergencyContactsList = passengerEmergencyContactsList;

        }

        @Override
        public EmergencyContactListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_emergency_contact, parent, false);

            EmergencyContactListAdapter.ViewHolder viewHolder = new EmergencyContactListAdapter.ViewHolder(itemLayoutView);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final EmergencyContactListAdapter.ViewHolder holder, int position) {

            holder.cont_nm.setText(passengerEmergencyContactsList.get(position).getContactname());
            holder.cont_id.setVisibility(View.GONE);
            holder.cont_id.setText(passengerEmergencyContactsList.get(position).getId() + "");
            holder.cont_number.setText(passengerEmergencyContactsList.get(position).getContactnumber());

            holder.cont_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Emergency_Contact.this);
                    alertDialogBuilder.setMessage("Do you want to delete this contact ?");
                    alertDialogBuilder.setPositiveButton("yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {

                                    String delete_cont_url = "http://uncletaxi.sarodestudio.photography/web_service/delete_emergency_contact";


                                    deleteContact(delete_cont_url, holder.cont_id.getText().toString());


                                }
                            });

                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                         dialog.dismiss();
                        }
                    });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();


                }
            });


        }


        @Override
        public int getItemCount() {
            return passengerEmergencyContactsList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            TextView cont_number, cont_nm, cont_id;
            ImageView cont_menu;

            public ViewHolder(View itemView) {
                super(itemView);
                cont_id = (TextView) itemView.findViewById(R.id.cont_id);
                cont_number = (TextView) itemView.findViewById(R.id.cont_number);
                cont_nm = (TextView) itemView.findViewById(R.id.cont_nm);
                cont_menu = (ImageView) itemView.findViewById(R.id.cont_menu);

            }
        }

    }
}
*/
