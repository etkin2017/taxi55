package in.org.cyberspace.taxi55;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import in.org.cyberspace.taxi55.db.NotiDbHelper;

public class Success extends AppCompatActivity {
    String bookingID;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);
        this.bookingID = getIntent().getExtras().getString("ID");
        ((ImageView) findViewById(R.id.imageView4)).setAnimation(AnimationUtils.loadAnimation(this, R.anim.anim_move_left));
        ((ImageView) findViewById(R.id.bcap)).setAnimation(AnimationUtils.loadAnimation(this, R.anim.anim_move_left));
        ((TextView) findViewById(R.id.tvBookingNo)).setText("Your Booking No Is : " + this.bookingID);
    }

    public void back(View v) {
        // if (getIntent().getStringExtra("from").equals("1")) {
        finish();
        startActivity(new Intent(this, MyRides.class));
       /* } else {
            finish();
        }*/
    }

    public void trackDriver(View v) {
        String Url = ConnectionConfiguration.URL + "check_booking_assigned_for";
        go_to_map(this.bookingID);
    }

    public void cancelBooking(View v) {
        String u1 = ConnectionConfiguration.URL + "cancelled_booking";


        AlertDialog.Builder dlg = new AlertDialog.Builder(Success.this);
        dlg.setTitle(R.string.app_name);
        dlg.setMessage("Are You Sure You Want To Cancel the Trip ?");
        dlg.setIcon(R.drawable.icon);
        dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //    finish();
                cancel_booking(bookingID);

            }
        });


        dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                return;
            }
        });

        dlg.create().show();

        //   cancel_booking(this.bookingID);
    }

    public void go_to_map(final String booking_id) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait...");
        dialog.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("booking_id", booking_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "check_booking_assigned_for", jsonObject, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                Log.e("PassengerResponse", response.toString());
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    Intent mapAct = new Intent(Success.this, DriverTrackingActivity.class);
                    String driver_id = jsonArray.getJSONObject(0).getString("assigned_for");
                    if (driver_id.equals("")) {
                        Toast.makeText(Success.this.getApplicationContext(), "Driver Not Assigned To this booking", Toast.LENGTH_LONG).show();
                        return;
                    } else {
                        String mobile = new PrefManager(Success.this).getUserMobile();

                        mapAct.putExtra("driver_id", driver_id);
                        mapAct.putExtra("driver_nm", jsonArray.getJSONObject(0).getString("name"));
                        mapAct.putExtra("user_name", jsonArray.getJSONObject(0).getString("user_name"));
                        mapAct.putExtra("phone", jsonArray.getJSONObject(0).getString("phone"));
                        mapAct.putExtra("email", jsonArray.getJSONObject(0).getString("email"));
                        mapAct.putExtra("booking_id", booking_id);
                        mapAct.putExtra(NotiDbHelper.sid, mobile);
                        mapAct.putExtra(NotiDbHelper.rid, jsonArray.getJSONObject(0).getString("phone"));
                        mapAct.putExtra("fcm", jsonArray.getJSONObject(0).getString("fcm"));
                        mapAct.putExtra("uname", jsonArray.getJSONObject(0).getString("user_name"));
                        Success.this.startActivity(mapAct);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Intent intent1 = new Intent(Success.this, RequestProcessingActivity.class);
                    intent1.putExtra("booking_id", booking_id);
                    startActivity(intent1);


                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.printStackTrace();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }

    public void cancel_booking(String booking_id) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait...");
        dialog.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("booking_id", booking_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "cancelled_booking", jsonObject, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                Log.e("PassengerResponse", response.toString());
                try {
                    String status = response.getString("status");
                    int data = response.getInt("data");
                    if (status.equals("success") && data == 1) {
                        Toast.makeText(Success.this.getApplicationContext(), "Booking Cancelled", Toast.LENGTH_LONG).show();
                        finish();
                        startActivity(new Intent(Success.this, MyRides.class));
                    } else {
                        //  Toast.makeText(Success.this.getApplicationContext(), "Failed To Cancel", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.printStackTrace();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 0, 1.0f));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }
}

/*
package in.org.cyberspace.taxi55;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class Success extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);
        Bundle extra = getIntent().getExtras();
        String bookingID=extra.getString("ID");
        Animation anim = AnimationUtils.loadAnimation(this,R.anim.anim_move_left);

        ImageView imgSuccess= (ImageView)findViewById(R.id.imageView4);

        imgSuccess.setAnimation(anim);
        TextView tvBooking=(TextView)findViewById(R.id.tvBookingNo);
        tvBooking.setText("Your Booking No Is : "+bookingID);
    }

    public void back(View v)
    {
        finish();
    }
}
*/
